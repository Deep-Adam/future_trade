import pandas as pd
import numpy as np
import math

from tqdm import tqdm


class DATAPROCESS:
    def __init__(self, time_period):
        self.time_period = time_period

    def moving_average(self, data_input, period):
        """
        计算移动平均线
        :param data_input: 股票价格
        :return: 移动平均线的数组
        """
        # close = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        # close = np.array(close, dtype='double')
        # ma_5 = ta.SMA(close, timeperiod=5)
        #
        # print(ma_5)

        # 计算10/20/40/60/120日均线
        days = len(data_input)
        res = []
        # 10日均线计算
        if 10 in period:
            mv_10 = []
            for i in range(days):
                mv_10.append(np.average(data_input[max(0, i - 9): i+1]))
            res.append(mv_10)

        # 20日均线计算
        if 20 in period:
            mv_20 = []
            for i in range(days):
                mv_20.append(np.average(data_input[max(0, i - 19): i + 1]))
            res.append(mv_20)

        # 40日均线计算
        if 40 in period:
            mv_40 = []
            for i in range(days):
                mv_40.append(np.average(data_input[max(0, i - 39): i + 1]))
            res.append(mv_40)

        # 60日均线计算
        if 60 in period:
            mv_60 = []
            for i in range(days):
                mv_60.append(np.average(data_input[max(0, i - 59): i + 1]))
            res.append(mv_60)

        # 120日均线计算
        if 120 in period:
            mv_120 = []
            for i in range(days):
                mv_120.append(np.average(data_input[max(0, i - 119): i + 1]))
            res.append(mv_120)

        return res

    def data_process(self):
        data = pd.read_csv("Data/RB9999.XSGE/RB9999.XSGE.csv", index_col=0)

        for i in range(1, 7):
            data.iloc[:, i] = data.iloc[:, i].apply(lambda x: math.log(x) if x > 0 else 0)

        data.to_csv("RB9999_log.csv")

        price_close = np.array(data.iloc[:, 4])  # Close price

        data_mv = self.moving_average(price_close, [10, 60, 120])
        print(">>>>>> Data loaded successfully.")

        print(">>>>>> Start calculating moving average ........")
        # [mv_10, mv_20, mv_40, mv_60, mv_120] = data_mv
        [mv_10, mv_60, mv_120] = data_mv
        # print(pdd_mv)

        pdd_mv = pd.DataFrame(data_mv).T
        # pdd_mv.columns = ['mv_10', 'mv_20', 'mv_40', 'mv_60', 'mv_120']
        pdd_mv.columns = ['mv_10', 'mv_60', 'mv_120']

        data = pd.concat((data, pdd_mv), axis=1)
        print(data.columns)
        print(data)
        data.to_csv("RB9999_data.csv")

    def data_min(self, _file_path):
        _data = pd.read_csv(_file_path)
        # data_min = []
        period = self.time_period
        print(_data)

        min_data = pd.DataFrame(columns=["date", "time_min", "open", "close", "high", "low", "volume", "money"])

        with tqdm(total=int(len(_data)/self.time_period)) as pbar:
            for i in range(int(len(_data)/self.time_period) - 1):
                min_data = min_data.append([{'date': _data.iloc[i * self.time_period + self.time_period, 0],
                                             'time_min': _data.iloc[i * self.time_period + self.time_period - 1, 1],
                                             'open': _data.iloc[i * self.time_period + 1, 2],
                                             'close': _data.iloc[i * self.time_period + 1, 3],
                                             'high': max(_data[(i * self.time_period + 1): (
                                                         i * self.time_period + self.time_period + 1)].high),
                                             'low': min(_data[(i * self.time_period + 1): (
                                                         i * self.time_period + self.time_period + 1)].low),
                                             'volume': sum(_data[(i * self.time_period + 1): (
                                                         i * self.time_period + self.time_period + 1)].volume),
                                             'money': sum(_data[(i * self.time_period + 1): (
                                                         i * self.time_period + self.time_period + 1)].money)}])
                pbar.update(1)

        print(min_data)
        min_data.reset_index(drop=True, inplace=True)

        # min_data.to_csv("Data/xgboost/RB9999.XSGE_2021_30min.csv", index_label=None)
        min_data.to_csv("Data/turtle/RB9999.XSGE_2020_30min.csv", index_label=None)

    @staticmethod
    def train_test_data(input_data):
        """
        将原始数据处理成时间序列
        :param input_data: 原始数据，包含open、high、low、close、volume和均线
        :return: 处理好的时间序列片段
        """
        # 按照一定的时间周期，将过去n minutes的数据整合成时间序列
        # 取n = 60
        period = 60
        length = len(input_data)
        # for i in range(period, length):

    @staticmethod
    def data_15min():
        data = pd.read_csv("Data/RB9999.XSGE/RB9999.XSGE.csv")
        # train_test_data(data)
        data_15min = []
        period = 15
        for i in range(int(len(data) / period) - 1):
            data_15min.append([data.iloc[(i + 1) * period - 1, 1],
                               data['open'][i * period], data['close'][(i + 1) * period - 1],
                               max(data['high'][i * period:(i + 1) * period]),
                               min(data['low'][i * period:(i + 1) * period])])
        account_log = pd.DataFrame(data_15min, columns=['time', 'open', 'close', 'high', 'low'])

        account_log.to_csv("account_15min.csv", index_label=None)

    # @staticmethod
    # def day_process(_file_path, _method):
    #     """
    #     将min数据转化为日K
    #     :param _file_path: 读取文件的地址
    #     :param _method: 保存文件的地址
    #     :return:
    #     """
    #     data_min = pd.read_csv(_file_path)      # 原始的min数据
    #     time_day = data_min.date                # 从min数据中抽取日期信息
    #
    #     # # print(daily)
    #     sub_res = time_day[0]                   # 预设值
    #     daily = [sub_res]
    #
    #     for i in time_day:
    #         if i == daily[-1]:
    #             pass
    #         else:
    #             daily.append(i)
    #
    #     # print(daily)
    #     day_data = pd.DataFrame(columns=["time", "open", "high", "low", "close"])
    #
    #     with tqdm(total=len(daily)) as pbar:
    #         for day in daily:
    #             day_data = day_data.append([{'time': day,
    #                                          'open': data_min[data_min.date == day].open.iloc[0],
    #                                          'high': max(data_min[data_min.date == day].high),
    #                                          'low': min(data_min[data_min.date == day].low),
    #                                          'close': data_min[data_min.date == day].close.iloc[-1]}])
    #
    #             pbar.update(1)
    #         # print(day_data)
    #         day_data.reset_index(drop=True, inplace=True)
    #         day_data.to_csv("Data/" + _method + "/day_data_RB9999.csv")
    #     print(day_data)
    #
    # def data_2020(self, _input_data, _day_data, start_day_num, _id, _method):
    #     _index = _input_data.date
    #     _day_data.iloc[start_day_num:, :].to_csv("Data/" + _method + "/day_" + _id[:-5] + ".csv")
    #
    #     start_day_num = np.where(_index == _day_data.time[start_day_num])[0][0]     # 定位日期后，选择第一个作为开始时间
    #
    #     _input_data = _input_data.iloc[start_day_num:, :]
    #     _input_data.reset_index(drop=True, inplace=True)
    #     _input_data.to_csv("Data/" + _method + "/" + _id[:-5] + ".csv")
    #     # print(_input_data)
    #
    # def train_test_data(self, input_data):
    #     """
    #     将原始数据处理成时间序列
    #     :param input_data: 原始数据，包含open、high、low、close、volume和均线
    #     :return: 处理好的时间序列片段
    #     """
    #     # 按照一定的时间周期，将过去n minutes的数据整合成时间序列
    #     # 取n = 60
    #     period = 60
    #     length = len(input_data)
    #     # for i in range(period, length):


if __name__ == "__main__":
    data = DATAPROCESS(30)
    # data.day_process("Data/RB9999.XSGE/RB9999_20201107-20210226.XSGE.csv")
    # data.day_process("Data/r_breaker/RB9999_20201107-20210226.XSGE.csv")

    # target_id = "I9999.XDCE"
    # start = 1531

    # target_id = "J9999.XDCE"
    # start = 2160

    # ===================================
    # =        extract day date         =
    # ===================================
    # target_id = "RB9999.XSGE_2020"
    # start = 0
    # method = 'xgboost'

    # data.day_process("Data/RB9999.XSGE/" + target_id + ".csv", method)    # 处理 RB9999 数据
    # data.day_process("Data/profit/" + target_id + ".csv")                 # 处理 I9999 数据
    # data.day_process("Data/profit/" + target_id + ".csv")                 # 处理 J9999 数据

    # data_all = pd.read_csv("Data/RB9999.XSGE/" + target_id + ".csv", index_col=0)
    # # print(data_all)
    #
    # day_data = pd.read_csv("Data/RB9999.XSGE/day_data_" + target_id[:-5] + ".csv", index_col=0)
    # # print(day_data)
    #
    # data.data_2020(data_all, day_data, start, target_id, method)

    # ===================================
    # =       extract 5min date         =
    # ===================================
    data.data_min("Data/RB9999.XSGE/RB9999.XSGE_2020.csv")





