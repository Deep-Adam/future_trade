import numpy as np
import pandas as pd
import re
import os
import math
import time

from thrust import TRADING

from multiprocessing import Process


if __name__ == "__main__":
    # data = pd.read_csv("Data/try/account_15min.csv", index_col=0)
    # data = data.iloc[40212:, :]
    # # data.to_csv("15min_2016.csv", index_label=0)
    # data.reset_index(drop=True, inplace=True)
    # print(data)
    # data.to_csv("Data/thrust/15min_2018.csv", index_label=0)
    trade_algo = TRADING(slippage=0, handling_fee=5)

    counts = []
    t = time.time()
    processor_num = 3

    k1_lis = [i / 100 for i in range(10, 30, 1)]
    k2_lis = [i / 100 for i in range(10, 30, 1)]
    period_lis = [i for i in range(10, 80, 2)]

    para = trade_algo.parameter_list(k1_lis, k2_lis, period_lis)

    slip_len = int(len(para) / processor_num)  # 每个核处理的参数列表的长度

    slip_id_list = []
    for i in range(processor_num - 1):
        slip_id_list.append(para[i * slip_len:(i + 1) * slip_len])
    slip_id_list.append(para[(processor_num - 1) * slip_len:])

    for x in range(processor_num):
        process = Process(target=trade_algo.main, args=(x, slip_id_list[x], slip_len))
        counts.append(process)
        process.start()
    e = counts.__len__()
    while True:
        for th in counts:
            if not th.is_alive():
                e -= 1
        if e <= 0:
            break
    print("Multiprocess cpu", time.time() - t)

