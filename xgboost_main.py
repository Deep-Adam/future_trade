# -*-coding:utf-8-*-

import numpy as np
import pandas as pd
import time

from tqdm import tqdm
from xgboost import XGBClassifier

# from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import GradientBoostingClassifier

from maximum_drawdown import MaximumDrawdown

import joblib


class TECHNICAL:
    @staticmethod
    def moving_average(data_input, period):
        """
        计算移动平均线
        :param data_input: 股票价格
        :param period:
        :return: 移动平均线的数组
        """
        # 计算10/20/40/60/120日均线
        length = len(data_input)
        mv = []

        for t in period:
            sub_mv = []
            for i in range(length):
                sub_mv.append(round(np.average(data_input[max(0, i - t + 1): i + 1]), 2))

            mv.append(sub_mv)

        return mv

    @staticmethod
    def max_min_pro(data_input, period):
        """
        计算过去N个周期的最大最小值
        :return:
        """
        sub_max = [data_input[0]]
        sub_min = [data_input[0]]
        length = len(data_input)

        for i in range(1, length):
            sub_max.append(max(data_input[max(0, i - period + 1): i + 1]))
            sub_min.append(min(data_input[max(0, i - period + 1): i + 1]))

        return sub_max, sub_min

    @staticmethod
    def macd(input_data):
        """
        calculate MACD technical with k-line close price
        :param input_data: Close price
        :return: MACD technical
        """
        period_low = 12
        period_high = 26
        period_dif = 9

        # EMA calculate
        ema_low = [input_data[0]]
        for i in range(1, len(input_data)):
            ema_low.append(
                round(ema_low[-1] * (period_low - 1) / (period_low + 1) + input_data[i] * 2 / (period_low + 1), 2))

        ema_high = [input_data[0]]
        for i in range(1, len(input_data)):
            ema_high.append(
                round(ema_high[-1] * (period_high - 1) / (period_high + 1) + input_data[i] * 2 / (period_high + 1), 2))

        # DIF and EMA-DIF calculate, and this is MACD value called DEA or DEN
        dif = [ema_low[0] - ema_high[0]]
        ema_dif = [ema_low[0] - ema_high[0]]
        macd_value = [(dif[0] - ema_dif[0]) * 2]
        for i in range(1, len(input_data)):
            dif.append(round(ema_low[i] - ema_high[i], 2))
            ema_dif.append(
                round(ema_dif[-1] * (period_dif - 1) / (period_dif + 1) + (ema_low[i] - ema_high[i]) * 2 / (period_dif + 1), 2))
            macd_value.append(round((dif[i] - ema_dif[i]) * 2, 2))

        return dif, ema_dif, macd_value

    def data_process(self, input_data):
        """
        figure out labels and feature engineering
        :param input_data:
        :return:
        """
        # input_data = input_data.iloc[:10000, :]

        sub_res = []
        res_data = pd.DataFrame()
        input_data = input_data.reset_index(drop=True)
        input_data = input_data.iloc[:, 2:]

        # prepare for moving average
        mv = self.moving_average(input_data["close"], [10, 20, 40])
        mv_10 = pd.DataFrame(mv[0], columns=["mv10"])
        mv_20 = pd.DataFrame(mv[1], columns=["mv20"])
        mv_40 = pd.DataFrame(mv[2], columns=["mv40"])

        # >>>>>>>>>>>>> volume moving average <<<<<<<<<<<<<<
        mv = self.moving_average(input_data["volume"], [10, 20, 40])
        volume_mv_10 = pd.DataFrame(mv[0], columns=["mv10"])
        volume_mv_20 = pd.DataFrame(mv[1], columns=["mv20"])
        volume_mv_40 = pd.DataFrame(mv[2], columns=["mv40"])

        # >>>>>>>>>>>>> MACD <<<<<<<<<<<<<<
        ema_low, ema_high, macd_value = self.macd(input_data["close"])
        ema_low = pd.DataFrame(ema_low, columns=["ma_fast"])
        ema_high = pd.DataFrame(ema_high, columns=["ma_slow"])
        macd_value = pd.DataFrame(macd_value, columns=["ma_macd"])

        # >>>>>>>>>>> min-max <<<<<<<<<<<<<
        his_min, his_max = self.max_min_pro(input_data["close"], 20)
        his_min = pd.DataFrame(his_min, columns=["his_min"])
        his_max = pd.DataFrame(his_max, columns=["his_max"])

        input_data = pd.concat([input_data,
                                mv_10, mv_20, mv_40,
                                volume_mv_10, volume_mv_20, volume_mv_40,
                                ema_low, ema_high, macd_value,
                                his_min, his_max], axis=1)

        #        open  close  high   low  volume       money     mv10     mv20     mv40
        # 0      3587   3589  3592  3581   58525  2099515310  3581.00  3581.00  3581.00

        with tqdm(total=int(len(input_data) - 2), ncols=100) as pbar:
            for i in range(len(input_data) - 2):
                sub_res.extend(input_data.iloc[i, :].values)
                sub_res.extend(input_data.iloc[i + 1, :].values)
                sub_res.extend(input_data.iloc[i + 2, :].values)
                sub_res.extend([input_data.iloc[i + 1, 0] - input_data.iloc[i, 0],
                                input_data.iloc[i + 1, 1] - input_data.iloc[i, 1],
                                input_data.iloc[i + 1, 2] - input_data.iloc[i, 2],
                                input_data.iloc[i + 1, 3] - input_data.iloc[i, 3],
                                input_data.iloc[i + 1, 4] - input_data.iloc[i, 4],
                                input_data.iloc[i + 1, 9] - input_data.iloc[i, 9],
                                input_data.iloc[i + 1, 10] - input_data.iloc[i, 10],
                                input_data.iloc[i + 1, 11] - input_data.iloc[i, 11],
                                input_data.iloc[i + 1, 12] - input_data.iloc[i, 12],
                                input_data.iloc[i + 1, 13] - input_data.iloc[i, 13],

                                input_data.iloc[i + 2, 0] - input_data.iloc[i, 0],
                                input_data.iloc[i + 2, 1] - input_data.iloc[i, 1],
                                input_data.iloc[i + 2, 2] - input_data.iloc[i, 2],
                                input_data.iloc[i + 2, 3] - input_data.iloc[i, 3],
                                input_data.iloc[i + 2, 4] - input_data.iloc[i, 4],
                                input_data.iloc[i + 2, 9] - input_data.iloc[i, 9],
                                input_data.iloc[i + 2, 10] - input_data.iloc[i, 10],
                                input_data.iloc[i + 2, 11] - input_data.iloc[i, 11],
                                input_data.iloc[i + 2, 12] - input_data.iloc[i, 12],
                                input_data.iloc[i + 2, 13] - input_data.iloc[i, 13],

                                input_data.iloc[i + 2, 0] - input_data.iloc[i + 1, 0],
                                input_data.iloc[i + 2, 1] - input_data.iloc[i + 1, 1],
                                input_data.iloc[i + 2, 2] - input_data.iloc[i + 1, 2],
                                input_data.iloc[i + 2, 3] - input_data.iloc[i + 1, 3],
                                input_data.iloc[i + 2, 4] - input_data.iloc[i + 1, 4],
                                input_data.iloc[i + 2, 9] - input_data.iloc[i + 1, 9],
                                input_data.iloc[i + 2, 10] - input_data.iloc[i + 1, 10],
                                input_data.iloc[i + 2, 11] - input_data.iloc[i + 1, 11],
                                input_data.iloc[i + 2, 12] - input_data.iloc[i + 1, 12],
                                input_data.iloc[i + 2, 13] - input_data.iloc[i + 1, 13],

                                round((input_data.iloc[i, 0] + input_data.iloc[i + 1, 0] + input_data.iloc[i + 2, 0]) / 3, 2),
                                round((input_data.iloc[i, 1] + input_data.iloc[i + 1, 1] + input_data.iloc[i + 2, 1]) / 3, 2),
                                round((input_data.iloc[i, 2] + input_data.iloc[i + 1, 2] + input_data.iloc[i + 2, 2]) / 3, 2),
                                round((input_data.iloc[i, 3] + input_data.iloc[i + 1, 3] + input_data.iloc[i + 2, 3]) / 3, 2),
                                round((input_data.iloc[i, 4] + input_data.iloc[i + 1, 4] + input_data.iloc[i + 2, 4]) / 3, 2),
                                round((input_data.iloc[i, 9] + input_data.iloc[i + 1, 9] + input_data.iloc[i + 2, 9]) / 3, 2),
                                round((input_data.iloc[i, 10] + input_data.iloc[i + 1, 10] + input_data.iloc[i + 2, 10]) / 3, 2),
                                round((input_data.iloc[i, 11] + input_data.iloc[i + 1, 11] + input_data.iloc[i + 2, 11]) / 3, 2),
                                round((input_data.iloc[i, 12] + input_data.iloc[i + 1, 12] + input_data.iloc[i + 2, 12]) / 3, 2),
                                round((input_data.iloc[i, 13] + input_data.iloc[i + 1, 13] + input_data.iloc[i + 2, 13]) / 3, 2),

                                input_data.iloc[i + 2, 6], input_data.iloc[i + 2, 7], input_data.iloc[i + 2, 8],
                                input_data.iloc[i + 2, 8] - input_data.iloc[i + 2, 7], input_data.iloc[i + 2, 7] - input_data.iloc[i + 2, 6],
                                input_data.iloc[i + 2, 1] - input_data.iloc[i + 2, 6],
                                input_data.iloc[i + 2, 1] - input_data.iloc[i + 2, 7],
                                input_data.iloc[i + 2, 1] - input_data.iloc[i + 2, 8],
                                ])
                sub_res = pd.DataFrame([sub_res])
                res_data = pd.concat([res_data, sub_res], axis=0)
                sub_res = []

                pbar.update(1)

        res_data = res_data.reset_index(drop=True)
        # print(res_data)

        res_data.to_csv("Data/xgboost/xgboost_feature_fixed.csv")

    @staticmethod
    def label_cal():
        # <<<<<<<<< 计算label Y >>>>>>>>>
        res_data = pd.read_csv("Data/xgboost/xgboost_feature_fixed.csv", index_col=0)
        y_data = []
        for i in range(len(res_data) - 3):
            if res_data.iloc[i + 3, 1] > res_data.iloc[i + 2, 1]:
                y_data.append(1)
            else:
                y_data.append(0)

        np.savetxt("Data/xgboost/xgboost_y.txt", y_data, delimiter=",")

        # <<<<<<<<< 计算价格走势的动量 >>>>>>>>>
        y_data = []
        for i in range(len(res_data) - 3):
            if (res_data.iloc[i + 3, 1] > res_data.iloc[i + 2, 1]) == (res_data.iloc[i + 2, 1] > res_data.iloc[i + 1, 1]):
                y_data.append(1)
            else:
                y_data.append(0)

        np.savetxt("Data/xgboost/xgboost_momentum.txt", y_data, delimiter=",")

        return res_data

    def data_add(self):
        """
        在原有feature基础上，增加新的feature
        :return:
        """

        # input_data,
        # mv_10, mv_20, mv_40,
        # volume_mv_10, volume_mv_20, volume_mv_40,
        # ema_low, ema_high, macd_value,
        # his_min, his_max

        res_data = []
        formal_data = pd.read_csv("Data/xgboost/xgboost_feature.csv", index_col=0)
        input_data = formal_data.iloc[:, :17]
        print(input_data)
        print(res_data)
        sub_res = []

        with tqdm(total=int(len(input_data) - 2), ncols=100) as pbar:
            for i in range(12, len(input_data)):
                sub_res.extend(input_data.iloc[i, :].values)

                sub_res.extend([input_data.iloc[i - 6, 0] - input_data.iloc[i, 0],
                                input_data.iloc[i - 6, 1] - input_data.iloc[i, 1],
                                input_data.iloc[i - 6, 2] - input_data.iloc[i, 2],
                                input_data.iloc[i - 6, 3] - input_data.iloc[i, 3],
                                input_data.iloc[i - 6, 4] - input_data.iloc[i, 4],
                                input_data.iloc[i - 6, 9] - input_data.iloc[i, 9],

                                input_data.iloc[i - 12, 0] - input_data.iloc[i, 0],
                                input_data.iloc[i - 12, 1] - input_data.iloc[i, 1],
                                input_data.iloc[i - 12, 2] - input_data.iloc[i, 2],
                                input_data.iloc[i - 12, 3] - input_data.iloc[i, 3],
                                input_data.iloc[i - 12, 4] - input_data.iloc[i, 4],
                                input_data.iloc[i - 12, 9] - input_data.iloc[i, 9],
                                ])
                # print(sub_res)

                sub_res = pd.DataFrame([sub_res])
                res_data = pd.concat([res_data, sub_res], axis=0)
                sub_res = []
                # print(res_data)

                pbar.update(1)

        formal_data = formal_data.iloc[12:, :]

        res_data = pd.concat([formal_data, sub_res], axis=0)
        res_data = res_data.reset_index(drop=True)

        print(res_data)
        res_data.to_csv("Data/xgboost/xgboost_feature_add.csv")

    @staticmethod
    def data_split(input_data, y_data):
        """
        split input data into training and testing dataset
        :param input_data:
        :param y_data:
        :return:
        """
        x_data = input_data

        split_per = 0.7
        split_num = int(len(x_data) * split_per)

        x_train = x_data.iloc[:split_num, :]
        y_train = y_data[:split_num]
        x_test = x_data.iloc[split_num:, :]
        y_test = y_data[split_num:]

        return x_train, y_train, x_test, y_test


class MODEL(TECHNICAL):
    def __init__(self, xgboost_model, gbdt_model):
        self.xgboost_model = xgboost_model      # loading xgboost model
        self.gbdt_model = gbdt_model            # loading gbdt model

    def trading_signal(self, data):
        # gbdt_model = GBDTMODEL()
        # xgboost_model = XGBOOSTMODEL()

        if self.gbdt_model.predict(data) == 1 and self.xgboost_model.predict(data) == 1:
            return 1
        elif self.gbdt_model.predict(data) == 0 and self.xgboost_model.predict(data) == 0:
            return 0
        else:
            return -1


class XGBOOSTMODEL(TECHNICAL):
    @staticmethod
    def fit(X, y):
        """
        training model
        :param X:
        :param y:
        :return:
        """
        model = XGBClassifier(base_score=0.5, colsample_bylevel=0.8, colsample_bytree=1,
                              gamma=0, learning_rate=0.15, max_delta_step=0, max_depth=4,
                              min_child_weight=2, missing=1, n_estimators=20, nthread=-1,
                              objective='binary:logistic', reg_alpha=0.3, reg_lambda=0.3,
                              scale_pos_weight=1, seed=7, subsample=1, use_label_encoder=False)
        model.fit(X, y)
        print("Training accuracy: {}%".format(round(accuracy_score(y, model.predict(X)) * 100, 2)))
        # print("Training accuracy:")
        # print(model)

        # Training accuracy: 61.52%
        # Accuracy: 58.01%
        # Training accuracy: 61.47%
        # Accuracy: 58.20%

        return model

    @staticmethod
    def predict(model, X, y):
        """
        模型预测，导入训练好的模型，并给出预测结果
        :param model: 训练好的模型
        :param X: 输入数据
        :param y: 真实label，以供计算准确率
        :return:
        """
        # make predictions for X
        y_pred = model.predict(X)
        predictions = [round(value) for value in y_pred]
        # evaluate predictions
        accuracy = accuracy_score(y, predictions)

        return predictions, accuracy

    def model_train(self, x, y):
        """
        xgboost训练，返回模型
        :param x:
        :param y:
        :return:
        """
        model = self.fit(x, y)
        model.save_model("Data/xgboost/xgboost.json")

        return model

    @staticmethod
    def xgboost_load_model(model_path):
        """
        loading pretrained model
        :param model_path: model checkpoint path
        :return:
        """
        model = XGBClassifier()
        model.load_model(model_path)

        return model


class GBDTMODEL(TECHNICAL):
    @staticmethod
    def fit(X, y):
        """
        gbdt training model
        :param X: input data
        :param y: label
        :return: trained model
        """

        model = GradientBoostingClassifier(loss='deviance', learning_rate=0.05, n_estimators=20,
                                           subsample=1.0, criterion='friedman_mse', min_samples_split=8,
                                           min_samples_leaf=2, min_weight_fraction_leaf=0.,
                                           max_depth=5, min_impurity_decrease=0.,
                                           min_impurity_split=None, init=None,
                                           random_state=None, max_features=None, verbose=0,
                                           max_leaf_nodes=None, warm_start=False,
                                           validation_fraction=0.1, n_iter_no_change=None, tol=1e-4,
                                           ccp_alpha=0.0)
        model.fit(X, y)
        print("Training accuracy: {}%".format(round(accuracy_score(y, model.predict(X)) * 100, 2)))
        # Training accuracy: 64.11%
        # Accuracy: 57.76%

        return model

    @staticmethod
    def predict(model, X, y):
        """
        模型预测，导入训练好的模型，并给出预测结果
        :param model: 训练好的模型
        :param X: input prediction data
        :param y: true label, for calculate prediction accuracy
        :return: prediction result and accuracy
        """
        # make predictions for X
        y_pred = model.predict(X)
        predictions = [round(value) for value in y_pred]
        # evaluate predictions
        accuracy = accuracy_score(y, predictions)

        return predictions, accuracy

    def model_train(self, x, y):
        """
        xgboost训练，返回模型
        :param x: training data
        :param y: training lavel
        :return:
        """
        model = self.fit(x, y)

        # save and load gbdt models
        joblib.dump(model, "Data/xgboost/gbdt.model")

        return model

    @staticmethod
    def gbdt_load_model(model_path):
        """
        loading pretrained model
        :param model_path: model checkpoint path
        :return:
        """
        # model = GradientBoostingClassifier()
        model = joblib.load(model_path)

        return model


class TRADING:
    def __init__(self, slippage, handling_fee, balance):
        """
        交易函数
        :param slippage: 滑点
        :param handling_fee: 交易手续费
        :param balance: 初始账户金额
        """
        self.slippage = slippage            # spread point
        self.handling_fee = handling_fee    # trading fee
        self.balance = balance

    def main_trading(self, data_all, model):
        """
        交易主模块，完成交易信号出发、账户余额计算等功能
        :param data_all: 输入数据
        :param model: 训练好的预测模型
        """
        account_log = []
        for l in range(2, 3):
            for k in range(12, 13):
                balance = self.balance
                single_posi = int(balance / 8000)
                # single_posi = max(single_posi, int(account / 8000))
                # trading_fee = self.handling_fee + 10 * single_posi * self.slippage      # 交易成本
                account_log = []

                position = 0
                position_log = []
                order_log = []
                trading = 0
                sub_order = dict()
                limit_pro = k
                limit_loss = -l

                sub_signal = 0
                # print(data_all)
                # ======================= X ========================
                #        open  close  high   low  volume       money
                # 12283  3724   3725  3726  3723    4663   173690590
                for i in range(len(data_all) - 1):
                    # xgboost模型的预测结果
                    a = data_all.iloc[i: i + 1, :]
                    signal = model.trading_signal(data_all.iloc[i: i + 1, :])

                    '''
                    # something needs to have a look later
                    # if signal > 0:
                    #     signal = data_all.iloc[i: i + 1, 17 * 2 + 2] > data_all.iloc[i: i + 1, 17 + 2]
                    # else:
                    #     signal = 1 - data_all.iloc[i: i + 1, 17 * 2 + 2] > data_all.iloc[i: i + 1, 17 + 2]
                    # signal = signal.values[0]
                    '''

                    # if position == 0:
                    #     position = 1
                    #     sub_order.update({'longorshort': 'short', 'time': data_all.iloc[i, 1],
                    #                       'open': X.iloc[i, 1], 'high': X.iloc[i, 1], 'low': X.iloc[i, 1]})
                    # elif position == -1:
                    #     sub_order.update({'close': X.iloc[i, 3]})       # stop loss
                    #     #         order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    #     #         sub_order.update({"profit": order_profit})
                    #     position = 1
                    # else:
                    #     pass

                    # ml models output buy signal
                    if signal == 1:
                        if data_all.iloc[i + 1, 2] - data_all.iloc[i, 1] >= limit_pro:        # takeprofit
                            balance += limit_pro * 10 * single_posi
                            order_log.append({'longorshort': 'long',
                                              'time': data_all.index[i], 'holding_time': 1,
                                              'open': data_all.iloc[i, 1], 'close': data_all.iloc[i, 1] + limit_pro,
                                              'high': data_all.iloc[i + 1, 2], 'low': data_all.iloc[i + 1, 3],
                                              'profit': limit_pro})

                        elif data_all.iloc[i + 1, 2] - data_all.iloc[i, 1] <= limit_loss:     # stop loss
                            balance += limit_loss * 10 * single_posi
                            order_log.append({'longorshort': 'long',
                                              'time': data_all.index[i], 'holding_time': 1,
                                              'open': data_all.iloc[i, 1], 'close': data_all.iloc[i, 1] + limit_loss,
                                              'high': data_all.iloc[i + 1, 2], 'low': data_all.iloc[i + 1, 3],
                                              'profit': limit_loss})

                        else:                                                   # hold the order
                            balance += (data_all.iloc[i + 1, 1] - data_all.iloc[i, 1]) * 10 * single_posi
                            order_log.append({'longorshort': 'long',
                                              'time': data_all.index[i], 'holding_time': 1,
                                              'open': data_all.iloc[i, 1], 'close': data_all.iloc[i + 1, 1],
                                              'high': data_all.iloc[i + 1, 2], 'low': data_all.iloc[i + 1, 3],
                                              'profit': data_all.iloc[i + 1, 1] - data_all.iloc[i, 1]})

                    elif signal == 0:
                        if data_all.iloc[i + 1, 3] - data_all.iloc[i, 1] <= -limit_pro:       # takeprofit
                            balance += limit_pro * 10 * single_posi
                            order_log.append({'longorshort': 'short',
                                              'time': data_all.index[i], 'holding_time': 1,
                                              'open': data_all.iloc[i, 1], 'close': data_all.iloc[i, 1] - limit_pro,
                                              'high': data_all.iloc[i + 1, 3], 'low': data_all.iloc[i + 1, 2],
                                              'profit': limit_pro})

                        elif data_all.iloc[i + 1, 3] - data_all.iloc[i, 1] >= -limit_loss:    # stop loss
                            balance += limit_loss * 10 * single_posi
                            order_log.append({'longorshort': 'short',
                                              'time': data_all.index[i], 'holding_time': 1,
                                              'open': data_all.iloc[i, 1], 'close': data_all.iloc[i, 1] - limit_loss,
                                              'high': data_all.iloc[i + 1, 3], 'low': data_all.iloc[i + 1, 2],
                                              'profit': limit_loss})

                        else:                                                   # hold the order
                            balance -= (data_all.iloc[i + 1, 1] - data_all.iloc[i, 1]) * 10 * single_posi
                            order_log.append({'longorshort': 'short',
                                              'time': data_all.index[i], 'holding_time': 1,
                                              'open': data_all.iloc[i, 1], 'close': data_all.iloc[i + 1, 1],
                                              'high': data_all.iloc[i + 1, 3], 'low': data_all.iloc[i + 1, 2],
                                              'profit': -1 * (data_all.iloc[i + 1, 1] - data_all.iloc[i, 1])})

                    else:
                        balance -= (data_all.iloc[i + 1, 1] - data_all.iloc[i, 1]) * 10 * single_posi

                    if sub_signal is signal:
                        pass
                    else:
                        balance -= 10 * single_posi
                        trading += 1
                        sub_signal = signal

                    account_log.append([balance])
                print("No {:d}, {:d}      >>>>>> Balance:{:.1f}      >>>>>> Trading Num: {:d}".format(l, k, balance, trading))
                account_log = pd.DataFrame(account_log)
                account_log.to_csv("Result/xgboost/account_log.csv", header=["Balance"])

                order_log = pd.DataFrame.from_dict(order_log, orient='columns')
                order_log.to_csv("result/xgboost/order_log_xgboost.csv")

        drawdown_cal = MaximumDrawdown()
        account_log = account_log.iloc[:, 0].values
        account_log = [[i] for i in account_log]
        max_drawdown = round(drawdown_cal.calculate(account_log), 4)
        print(">>>>>> Maxmum drawdown is:", max_drawdown)

        # for k in range(1, len(data) - 1):
        #     # ===================================
        #     # =          close order            =
        #     # ===================================
        #     if position > 0:
        #         sub_order.update({'close': data.iloc[i, 3]})  # stop loss
        #         order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
        #         sub_order.update({"profit": order_profit})
        #         self.balance += order_profit
        #         order_log.append(sub_order)
        #         sub_order = {}
        #
        #     elif position < 0:
        #         trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # trading cost
        #
        #         sub_order.update({'close': data.iloc[i, 3]})  # stop loss
        #         order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
        #         sub_order.update({"profit": order_profit})
        #         self.balance += order_profit
        #         order_log.append(sub_order)
        #         sub_order = {}
        #     else:
        #         pass
        #
        #     position = 0
        #
        #     # ===================================
        #     # =           New order             =
        #     # ===================================
        #     if data.iloc[i, 3] > data.iloc[i, 2] + price_gap and data.iloc[i, 6] > 2 * data.iloc[i - 1, 6] and \
        #             data.iloc[i, 3] > data.iloc[i, 8]:
        #         # positive signal
        #         position = single_posi
        #         trading += 1
        #         sub_order.update({'longorshort': 'short', 'time': data.iloc[i, 1],
        #                           'open': data.iloc[i, 3], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})
        #
        #     elif data.iloc[i, 3] + price_gap < data.iloc[i, 2] and data.iloc[i, 6] > 2 * data.iloc[i - 1, 6] and \
        #             data.iloc[i, 3] < data.iloc[i, 8]:
        #         position = -single_posi
        #         trading += 1
        #         sub_order.update({'longorshort': 'long', 'time': data.iloc[i, 1],
        #                           'open': data.iloc[i, 3], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})
        #     else:
        #         pass
        #
        # account_log.append([self.balance])


class TRIGGER:
    def __init__(self, data_path, feature_path, label_path):
        self.data_path = data_path
        self.feature_path = feature_path
        self.label_path = label_path

    def data_processing(self, process_signal, add_signal, label_signal):
        """
        data processing
                    date         time_min  open  close  high   low  volume       money
        0       2020/1/2    2020/1/2 9:05  3587   3589  3592  3581   58525  2099515310
        1       2020/1/2    2020/1/2 9:10  3582   3578  3583  3575   43338  1551023170
        :return:
        """
        model = XGBOOSTMODEL()

        data = pd.read_csv("data/r_breaker/r_breaker_20201107-20210226.csv", index_col=0)
        if process_signal == "process":
            model.data_process(data)
        if add_signal == "add":
            model.data_add()
        if label_signal == "label":
            model.label_cal()

    def xgboost_train_inference(self, label_path):
        # ========================================
        # =     xgboost train and inference      =
        # ========================================
        model = XGBOOSTMODEL()              # loading class XGBOOSTMODEL
        data = pd.read_csv(self.feature_path, index_col=0)      # read data file
        # print(data)
        y_data = np.loadtxt(label_path, delimiter=",")

        x_train, y_train, x_test, y_test = model.data_split(data[:-3], y_data)

        xgboost_model = model.model_train(x_train, y_train)
        _, accuracy = model.predict(xgboost_model, x_test, y_test)

        print("Accuracy: %.2f%%" % (accuracy * 100.0))
        print(xgboost_model.feature_importances_)

    def gbdt_train_inference(self, label_path):
        # ========================================
        # =       GBDT train and inference       =
        # ========================================
        model = GBDTMODEL()
        data = pd.read_csv(self.feature_path, index_col=0)      # load feature data
        y_data = np.loadtxt(label_path, delimiter=",")          # load label file

        # split data into training and testing data
        x_train, y_train, x_test, y_test = model.data_split(data[:-3], y_data)

        gbdt_model = model.model_train(x_train, y_train)
        _, accuracy = model.predict(gbdt_model, x_test, y_test)

        print("Accuracy: %.2f%%" % (accuracy * 100.0))
        # print(gbdt_model.feature_importances_)

    @staticmethod
    def deal_with_signal():
        """
        save trading signal from xgboost prediction
        :return:
        """
        signal = pd.read_csv("result/xgboost/_predict_test.csv", index_col=0)
        signal = signal.iloc[:, 0].values
        signal = signal.astype(int)
        np.savetxt("result/xgboost/_signal.csv", signal, delimiter=",", newline=",")

    def main_trading(self):
        """
        trigger main_trading api
        :return:
        """
        xgboost_model = XGBOOSTMODEL()
        gbdt_model = GBDTMODEL()

        xgboost_model = xgboost_model.xgboost_load_model("data/xgboost/xgboost.json")
        gbdt_model = gbdt_model.gbdt_load_model("data/xgboost/gbdt.model")
        # print(xgboost_model)
        # print(gbdt_model)

        trading = TRADING(slippage=0, handling_fee=5, balance=20000)

        data = pd.read_csv(self.feature_path, index_col=0)
        # print(data)
        data = data.iloc[-3000:, :]
        data.reset_index(drop=True, inplace=True)

        signal_model = MODEL(xgboost_model, gbdt_model)

        trading.main_trading(data, signal_model)


def main():
    time_start = time.time()
    # day_data = pd.read_csv("Data/r_breaker/day_data_20201107-20210226.csv", index_col=0)
    # day_data.reset_index(drop=True, inplace=True)
    # data = pd.read_csv("Data/r_breaker/r_breaker_20201107-20210226.csv", index_col=0)

    data_path = "data/RB9999.XSGE_2020_5min.csv"
    feature_path = "data/xgboost/xgboost_feature_fixed.csv"
    label_path = "data/xgboost/xgboost_y.txt"

    trigger = TRIGGER(data_path, feature_path, label_path)

    # trigger.data_processing(process_signal=False, add_signal=False, label_signal="label")                   # call data processing function

    # trigger.xgboost_train_inference("Data/xgboost/xgboost_y.txt")             # training xgboost model and testing
    # trigger.xgboost_train_inference("Data/xgboost/xgboost_momentum.txt")    # training xgboost model and testing
    # trigger.gbdt_train_inference("Data/xgboost/xgboost_momentum.txt")    # training xgboost model and testing
    trigger.main_trading()

    print(">>>>>> Time used: ", round(time.time() - time_start, 2))


if __name__ == "__main__":
    main()

