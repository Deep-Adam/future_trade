# -*-coding:utf-8-*-
# 螺纹钢商品期货主连
# 使用R_Breaker算法，量化交易回测，单线程回测

import numpy as np
import pandas as pd
from tqdm import tqdm
from maximum_drawdown import MaximumDrawdown
import time
# import talib as ta


class TRADING:
    def __init__(self, slippage, handling_fee):
        self.slippage = slippage            # 滑点与交易手续费
        self.handling_fee = handling_fee    # 交易手续费

    @staticmethod
    def wait_for_process(data, i, position, sub_order, single_posi,
                         limit_pro, trading_fee, account, order_log, trading):
        if data.iloc[i - 1, 3] > data.iloc[i - 1, 2] and data.iloc[i, 6] > data.iloc[i - 1, 6]:
            # positive signal
            if position == 0:
                position = single_posi
                trading += 1
                sub_order.update({'longorshort': 'long', 'time': data.iloc[i, 1],
                                  'open': data.iloc[i, 2], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})

            elif position > 0:
                # 持有仓位为多单，判断止盈止损
                if data.iloc[i, 4] > data.iloc[i, 2] + limit_pro:
                    sub_order.update({'close': data.iloc[i, 2] + limit_pro})  # 止盈
                    order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                    order_log.append(sub_order)
                    sub_order = {}
                else:
                    sub_order.update({'close': data.iloc[i, 3]})  # 止损
                    order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                    order_log.append(sub_order)
                    sub_order = {}

            elif position < 0:
                # 平空单
                sub_order.update({'close': data.iloc[i, 3]})  # 止损
                order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                sub_order.update({"profit": order_profit})
                account += order_profit
                order_log.append(sub_order)
                sub_order = {}

                # 开多单
                if data.iloc[i, 4] > data.iloc[i, 2] + limit_pro:
                    sub_order.update({'close': data.iloc[i, 2] + limit_pro})  # 止盈
                    order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                    order_log.append(sub_order)
                    sub_order = {}
                else:
                    sub_order.update({'close': data.iloc[i, 3]})  # 止损
                    order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                    order_log.append(sub_order)
                    sub_order = {}

            else:
                position = single_posi
                trading += 1
                sub_order.update({'longorshort': 'long', 'time': data.iloc[i, 1],
                                  'open': data.iloc[i, 2], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})
        elif data.iloc[i - 1, 3] < data.iloc[i - 1, 2] and data.iloc[i, 6] > data.iloc[i - 1, 6]:
            position = -single_posi
            trading += 1
            sub_order.update({'longorshort': 'short', 'time': data.iloc[i, 1],
                              'open': data.iloc[i, 2], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})
        else:
            pass

    @staticmethod
    def time_trans(x):
        return x[:-9]

    @staticmethod
    def parameter_list(a_lis, b_lis, c_lis, d_lis, t_lis):
        para_list = []
        for a in a_lis:
            for b in b_lis:
                for c in c_lis:
                    for d in d_lis:
                        for t in t_lis:
                            para_list.append([a, b, c, d, t])

        return para_list

    @staticmethod
    def moving_average(data_input, period):
        """
        计算移动平均线
        :param data_input: 股票价格
        :param period:
        :return: 移动平均线的数组
        """
        # 计算10/20/40/60/120日均线
        length = len(data_input)
        mv = []
        sub_mv = []

        for t in period:
            sub_mv = []
            for i in range(length):
                sub_mv.append(round(np.average(data_input[max(0, i - t + 1): i + 1]), 2))

        mv.append(sub_mv)

        return mv

    @staticmethod
    def set_price_cal(high_price, low_price, close_price, a, b, c, d):
        # 计算r_breaker相关的指标
        bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
        ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价

        benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
        senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

        sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
        bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

        return bsetup, benter, bbreak, ssetup, senter, sbreak

    def main_trading(self, data_id, data_all, _len_data, order_save):
        """
        回测算法，根据参数回测算法表现
        :param data_id:
        :param data_all:
        :param _len_data:
        :param order_save: 参数表达是否为单个参数组合的回测，确认是否保留每一单交易
        :return:
        """
        # 更新a,b,c,d参数后，重新初始化交易信息
        account = 10000
        single_posi = int(account / 8000)
        account_log = []
        order_log = []
        trading = 0
        sub_order = dict()
        position = 0
        limit_pro = 15
        price_gap = 4           # 价格上涨的价差

        # calculate moving average of 20
        close_price = data_all.close
        mv_20 = self.moving_average(close_price, [60])
        mv_20 = pd.DataFrame(mv_20[0], columns=["mv20"])
        data_all = pd.concat([data_all, mv_20], axis=1)

        # 油铜比 波动率
        # 金银比 美国国债收益率
        # TIPS

        for k in range(0, _len_data):
            # single_posi = max(single_posi, int(account / 8000))
            single_posi = 1
            trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # 交易成本

            data = data_all[data_all.date == data_id[k]]
            data.reset_index(drop=True, inplace=True)

            for i in range(1, len(data) - 1):
                # ===================================
                # =          close order            =
                # ===================================
                if position > 0:
                    sub_order.update({'close': data.iloc[i, 3]})        # stop loss
                    order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                    order_log.append(sub_order)
                    sub_order = {}

                elif position < 0:
                    trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # trading cost

                    sub_order.update({'close': data.iloc[i, 3]})    # stop loss
                    order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                    order_log.append(sub_order)
                    sub_order = {}
                else:
                    pass

                position = 0

                # ===================================
                # =           New order             =
                # ===================================
                if data.iloc[i, 3] > data.iloc[i, 2] + price_gap and data.iloc[i, 6] > 2 * data.iloc[i - 1, 6] and data.iloc[i, 3] > data.iloc[i, 8]:
                    # positive signal
                    position = single_posi
                    trading += 1
                    sub_order.update({'longorshort': 'short', 'time': data.iloc[i, 1],
                                      'open': data.iloc[i, 3], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})

                elif data.iloc[i, 3] + price_gap < data.iloc[i, 2] and data.iloc[i, 6] > 2 * data.iloc[i - 1, 6] and data.iloc[i, 3] < data.iloc[i, 8]:
                    position = -single_posi
                    trading += 1
                    sub_order.update({'longorshort': 'long', 'time': data.iloc[i, 1],
                                      'open': data.iloc[i, 3], 'high': data.iloc[i, 2], 'low': data.iloc[i, 2]})
                else:
                    pass

            account_log.append([account])

        winrate = 0
        for j in order_log:
            if j["profit"] > 0:
                winrate += 1
        winrate = round(100 * winrate / len(order_log), 2)

        if order_save == 1:
            drawdown_cal = MaximumDrawdown()
            rate = round(((account_log[-1][0] - account_log[0][0]) / account_log[0][0]) * 100, 2)
            max_drawdown = drawdown_cal.calculate(account_log)
            print('rate:', rate,
                  '\ntrading:', trading,
                  '\ndraw:', round(max_drawdown * 100, 2),
                  '\nwinrate:', winrate)
            order_log = pd.DataFrame.from_dict(order_log, orient='columns')
            order_log.to_csv("Result/r_breaker/order_log_move_2.csv")

        return account_log, trading, winrate

    def main(self, pro_id, para_list, stride, order_trigger):
        year_backbone = 2021
        # 从2019年开始回测
        if year_backbone == 2019:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)     # 读入处理好的日K数据
            day_data = day_data.iloc[2433:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)             # 原始的1 min数据
            data = data.iloc[686175:, :]
            data.reset_index(drop=True, inplace=True)
        # 从20116年开始回测
        elif year_backbone == 2016:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)
            day_data = day_data.iloc[1689:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
        # 从20118年开始回测
        elif year_backbone == 2018:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)
            day_data = day_data.iloc[2190:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
        # 从2020年开始回测
        elif year_backbone == 2020:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)
            day_data = day_data.iloc[2676:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
            data = data.iloc[769290:, :]
            data.reset_index(drop=True, inplace=True)
        # 从2021年开始回测
        elif year_backbone == 2021:
            day_data = pd.read_csv("Data/r_breaker/day_data_20201107-20210226.csv", index_col=0)
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker_20201107-20210226.csv", index_col=0)
        else:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)  # 读入处理好的日K数据
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)  # 原始的1 min数据

        data_id = day_data.time  # 从日K数据中，提取日期信息

        # lock.acquire()      # 进程锁

        drawdown_cal = MaximumDrawdown()
        # 记录每次交易的min级账户情况
        save_log = pd.DataFrame()
        # 记录所有交易的利润率等
        save_all = pd.DataFrame(columns=["No", "a", "b", "c", "d", "t", "rate", 'drawdown', "trading", "winrate"])
        # 记录最后一次交易的min级交易详情，持仓等
        n = pro_id * stride
        len_data = len(day_data)

        with tqdm(total=stride, ncols=80) as pbar:
            for [a, b, c, d, period] in para_list:
                account_log, trading, winrate = self.main_trading(data_id, data, len_data, order_trigger)

                max_drawdown = round(drawdown_cal.calculate(account_log), 4)
                rate = round((account_log[-1][0] - 10000) / 100, 2)

                account_log = pd.DataFrame(account_log,
                                           columns=[str(a) + '_' + str(b) + '_' + str(c) + '_' + str(d)])

                save_log = pd.concat((save_log, account_log), axis=1)

                # 记录每次回测的交易概述
                save_all = save_all.append([{"No": n,
                                             'a': a,
                                             'b': b,
                                             "c": c,
                                             "d": d,
                                             "t": period,
                                             "rate": rate,
                                             "drawdown": max_drawdown * 100,
                                             "trading": trading,
                                             "winrate": winrate}])


                if n % 10 == 0:
                    # 保存迭代的结果
                    save_log.index = day_data.time
                    save_all.reset_index(drop=True, inplace=True)
                    save_all.to_csv("Result/r_breaker/overview_mintick.csv", index_label=None)
                    save_log.to_csv("Result/r_breaker/account_mintick.csv", index_label=None)

                n += 1
                pbar.update(1)

        save_log.index = day_data.time
        save_all.reset_index(drop=True, inplace=True)
        save_all.to_csv("Result/r_breaker/overview_mintick.csv", index_label=None)
        save_log.to_csv("Result/r_breaker/account_mintick.csv", index_label=None)


if __name__ == "__main__":
    trade_algo = TRADING(slippage=0.5, handling_fee=5)

    time_start = time.time()

    # para = [[0.3, 1.06, 0.06, 0.25, 120]]
    # para = [[0.3, 1.06, 0.06, 0.3, 40]]
    para = [[0.3, 1.06, 0.06, 0.3, 60]]
    trade_algo.main(0, para, len(para), 1)

    print('times used: ', round(time.time() - time_start, 2))


