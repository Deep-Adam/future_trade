# -*-coding:utf-8-*-

import time
from thrust import TRADING


if __name__ == "__main__":
    # data = pd.read_csv("Data/try/account_15min.csv", index_col=0)
    # data = data.iloc[40212:, :]
    # # data.to_csv("15min_2016.csv", index_label=0)
    # data.reset_index(drop=True, inplace=True)
    # print(data)
    # data.to_csv("Data/thrust/15min_2018.csv", index_label=0)
    trade_algo = TRADING(slippage=0,
                         handling_fee=5,
                         file_path="Data/thrust/RB9999.XSGE_2020_30min.csv")

    counts = []
    t = time.time()
    processor_num = 1

    # k1_lis = [i / 100 for i in range(10, 30, 1)]
    # k2_lis = [i / 100 for i in range(10, 30, 1)]
    # period_lis = [i for i in range(10, 80, 2)]
    #
    # para = trade_algo.parameter_list(k1_lis, k2_lis, period_lis)

    # slip_len = len(para)
    para = [[0.1, 0.16, 18]]

    # slip_id_list = []
    # for i in range(processor_num - 1):
    #     slip_id_list.append(para[i * slip_len:(i + 1) * slip_len])
    # slip_id_list.append(para[(processor_num - 1) * slip_len:])

    trade_algo.main(0, para, 1)

    print("Singleprocess CPU", time.time() - t)

