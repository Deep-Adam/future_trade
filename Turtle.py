# -*-coding:utf-8-*-

import numpy as np
import pandas as pd
import time

# from tqdm import tqdm
# from xgboost import XGBClassifier

from maximum_drawdown import MaximumDrawdown


def data_process(data):
    return data


class TRADE:
    def __init__(self, balance, file_path, n_period, flen):
        self.balance = balance
        self.file_path = file_path
        self.n_period = n_period
        self.flen = flen

    # def trading_signal(self):

    def trading(self):
        data = pd.read_csv(self.file_path)
        print(data)
        save_all = pd.DataFrame(columns=["n_period", "correction", "balance", 'drawdown', "trading"])

        maxmum_order = 3

        for n_period in range(20, 80, 5):
            for flen in range(10, 20):
                for correction in range(1, 2):

                    # ===================================================
                    # =            initial hyper parameters             =
                    # ===================================================
                    position = 0        # current position, position number means long while negtive number mean short
                    pre_price = 0       # last operation price
                    balance = self.balance
                    account_log = []    # log account balance
                    trading_nums = 0    # trading volumes

                    order_log = []      # log order record
                    sub_order = []  # temporary order record

                    high_list = np.array(data["high"][:n_period].values)
                    low_list = np.array(data["low"][:n_period].values)

                    # ============= key lines of the turtle algorithm =============
                    up_line = np.max(high_list)                             # up line
                    down_line = np.min(low_list)                            # down line
                    middle_line = round((up_line + down_line) / 2, 1)       # middle line

                    # print(">>>>>> Calculate turtle lines: {:d}, {:d}, {:.1f}".format(up_line, down_line, middle_line))

                    # ============== calculation of N, the momentum ===============
                    # == TrueRange = Max(High−Low, High−PreClose, PreClose−Low) ===
                    # ========== n = (前19日的N值之和+当时的TrueRange) / 20 ==========
                    ture_range = []
                    for i in range(1, flen + 1):
                        sub_range = max(data["high"][i] - data["low"][i],
                                        data["high"][i] - data["close"][i - 1],
                                        data["close"][i - 1] - data["low"][i])
                        ture_range.append(sub_range)
                    n_range = round(np.sum(ture_range) / flen, 1)

                    trading_fees = 10

                    for i in range(n_period + 1, len(data)):
                        # ========= settle account =========
                        if position == 0:
                            if data["high"][i] >= up_line:

                                # check if the open price has already over the upline
                                if data["open"][i] >= up_line:
                                    pre_price = data["open"][i]
                                else:
                                    pre_price = up_line

                                balance += data["close"][i] - pre_price
                                # balance += position * (data["close"][i] - data["close"][i - 1])

                                sub_order = [{'long-short': 'first-long', 'time': data["time_min"][i],
                                              'open': pre_price, 'holding_time': 1, 'close': 0, 'profit': 0}]

                                position = 1
                                balance -= trading_fees
                                trading_nums += 1
                            elif data["low"][i] <= down_line:

                                if data["open"][i] <= down_line:
                                    pre_price = data["open"][i]
                                else:
                                    pre_price = down_line

                                balance -= data["close"][i] - pre_price
                                # balance -= position * (data["close"][i] - data["close"][i - 1])

                                # >>>>>>> order log initial <<<<<<<
                                sub_order = [{'long-short': 'first-short', 'time': data["time_min"][i],
                                              'open': pre_price, 'holding_time': 1, 'close': 0, 'profit': 0}]

                                position = -1
                                balance -= trading_fees
                                trading_nums += 1
                            else:
                                pass
                            # print(sub_order)

                        elif position > 0:
                            if data["high"][i] >= pre_price + 0.5 * n_range:
                                if position >= maxmum_order:
                                    # control the open interest
                                    for sub_log in sub_order:
                                        sub_log.update({'holding_time': sub_log['holding_time'] + 1})
                                else:
                                    if data["open"][i] >= pre_price + 0.5 * n_range:
                                        pre_price = data["open"][i]
                                    else:
                                        pre_price = int(pre_price + 0.5 * n_range + 0.5)

                                    balance += data["close"][i] - pre_price
                                    balance += position * (data["close"][i] - data["close"][i - 1])

                                    # >>>>>>> order log initial <<<<<<<
                                    for sub_log in sub_order:
                                        sub_log.update({'holding_time': sub_log['holding_time'] + 1})

                                    sub_order.append({'long-short': 'add-long',
                                                      'time': data["time_min"][i],
                                                      'open': pre_price,
                                                      'holding_time': 1,
                                                      'close': 0})

                                    position += 1
                                    balance -= trading_fees
                                    trading_nums += 1
                            elif data["low"][i] <= middle_line:
                                if data["open"][i] <= middle_line:
                                    pre_price = data["open"][i]
                                else:
                                    pre_price = int(middle_line - 0.5)

                                balance += position * (pre_price - data["close"][i - 1])

                                for sub_log in sub_order:
                                    sub_log.update({'close': pre_price})
                                    sub_log.update({'holding_time': sub_log['holding_time'] + 1,
                                                    'profit': sub_log['close'] - sub_log['open']})

                                order_log.extend(sub_order)
                                sub_order = []

                                position = 0
                            else:
                                for sub_log in sub_order:
                                    sub_log.update({'holding_time': sub_log['holding_time'] + 1})
                        else:
                            if data["low"][i] <= pre_price - 0.5 * n_range:
                                if position <= -maxmum_order:
                                    for sub_log in sub_order:
                                        sub_log.update({'holding_time': sub_log['holding_time'] + 1})
                                else:
                                    if data["open"][i] <= pre_price - 0.5 * n_range:
                                        pre_price = data["open"][i]
                                    else:
                                        pre_price = int(pre_price - 0.5 * n_range - 0.5)

                                    balance -= data["close"][i] - pre_price
                                    balance -= position * (data["close"][i] - data["close"][i - 1])

                                    for sub_log in sub_order:
                                        sub_log.update({'holding_time': sub_log['holding_time'] + 1})

                                    # print(sub_order)

                                    sub_order.append({'long-short': 'add-short',
                                                      'time': data["time_min"][i],
                                                      'open': pre_price,
                                                      'holding_time': 1,
                                                      'close': 0})

                                    position -= 1
                                    balance -= trading_fees
                                    trading_nums += 1
                            elif data["close"][i] >= middle_line:
                                if data["open"][i] >= middle_line:
                                    pre_price = data["open"][i]
                                else:
                                    pre_price = int(middle_line + 0.5)

                                balance -= position * (pre_price - data["close"][i - 1])

                                # print(sub_order)
                                for sub_log in sub_order:
                                    sub_log.update({'close': pre_price})
                                    sub_log.update({'holding_time': sub_log['holding_time'] + 1,
                                                    'profit': sub_log['open'] - sub_log['close']})

                                order_log.extend(sub_order)
                                sub_order = []

                                position = 0
                            else:
                                for sub_log in sub_order:
                                    sub_log.update({'holding_time': sub_log['holding_time'] + 1})

                        account_log.append([balance])

                        high_list = np.append(high_list[1:], data["high"][i])
                        low_list = np.append(low_list[1:], data["low"][i])
                        # print(high_list)
                        # ============= key lines of the turtle algorithm =============
                        up_line = np.max(high_list)  # up line
                        down_line = np.min(low_list)  # down line
                        middle_line = round((up_line + down_line) / 2, 1)  # middle line

                        # ============== calculation of N, the momentum ===============
                        # == TrueRange = Max(High−Low, High−PreClose, PreClose−Low) ===
                        # ========== N = (前19日的N值之和+当时的TrueRange)/20 ===========

                        ture_range = ture_range[1:]
                        ture_range.append(max(data["high"][i] - data["low"][i],
                                              data["high"][i] - data["close"][i - 1],
                                              data["close"][i - 1] - data["low"][i]))
                        # print(ture_range)
                        n_range = round(np.sum(ture_range) / flen, 1)

                        # print(">>>>>> Calculate turtle lines: {:d}, {:d}, {:.1f}    >>> ture range is: {:0.2f}".format(up_line, down_line, middle_line, n_range))

                    # print("No {:d}, {:d}      >>>>>> Balance:{:.1f}      >>>>>> Trading Num: {:d}".format(l, k, balance, trading))
                    print(">>>>>> {:d}  {:d}   Balance:{:.1f}      >>>>>> Trading Num: {:d}".format(n_period, flen, balance, trading_nums))
                    account_log = pd.DataFrame(account_log)
                    account_log.to_csv("result/turtle/account_log.csv", header=["Balance"])

                    # print(account_log)
                    drawdown_cal = MaximumDrawdown()
                    max_drawdown = drawdown_cal.calculate(account_log.values)

                    save_all = save_all.append([{"n_period": n_period,
                                                 'flen': flen,
                                                 'balance': balance,
                                                 "drawdown": max_drawdown,
                                                 "trading": trading_nums}])
            save_all.to_csv("result/turtle/save_all.csv", index_label=None)

            order_log = pd.DataFrame.from_dict(order_log, orient='columns')
            order_log.to_csv("result/turtle/order_log_turtle.csv")


def main():
    trade = TRADE(balance=20000, file_path="data/turtle/RB9999.XSGE_2020_30min.csv", n_period=20, flen=10)
    # trade = TRADE(balance=20000, file_path="data/turtle/RB9999.XSGE_2020.csv", n_period=20)
    trade.trading()


if __name__ == "__main__":
    main()
