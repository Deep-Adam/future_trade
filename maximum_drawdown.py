class MaximumDrawdown:
    def calculate(self, input_series):
        # print(input_series)
        input_series = [i[0] for i in input_series]
        highest = input_series[0]
        lowest = highest
        draw_down = 0
        for i in range(len(input_series)):
            if input_series[i] > highest:
                draw_down = min(draw_down, (lowest - highest)/highest)
                highest = input_series[i]
                lowest = highest
            if input_series[i] < lowest:
                lowest = input_series[i]

        draw_down = min(draw_down, (lowest - highest) / highest)

        return draw_down

#
# data = [[1],[2],[4],[3],[5],[2],[5],[6],[8],[9]]
#
# test = MaximumDrawdown()
# print(test.calculate(data))
