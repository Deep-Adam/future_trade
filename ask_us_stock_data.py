import akshare as ak
import pandas as pd
from tqdm import tqdm

# stock_us_daily_df = ak.stock_us_daily(symbol="AAPL", adjust="qfq")
# print(stock_us_daily_df)

# download stock name
# us_stock_current_df = ak.stock_us_spot()
# print(type(us_stock_current_df))
# us_stock_current_df.to_csv("data/us_stock/stockname.csv")
# data = us_stock_current_df

data = pd.read_csv("data/us_stock/stockname.csv", index_col=0)
# print(data[0:1])
# print(data.columns)
# print(data["market"])

market = list(data["symbol"])
# market = list(data["category"])
# print(market)

# res = []
# for i in market:
#     if i not in res:
#         res.append(i)
#     else:
#         pass
# print(res)


#  54%|██████████████████▍               | 6767/12481 [1:54:40<1:36:49,  1.02s/it]
# Traceback (most recent call last):
#   File "D:/Project/future_trade/ask_us_stock_data.py", line 35, in <module>
#     stock_us_daily_df.to_csv("data/us_stock/stock/" + i + ".csv")
#   File "C:\Application\Python\lib\site-packages\pandas\core\generic.py", line 3403, in to_csv
#     storage_options=storage_options,
#   File "C:\Application\Python\lib\site-packages\pandas\io\formats\format.py", line 1083, in to_csv
#     csv_formatter.save()
#   File "C:\Application\Python\lib\site-packages\pandas\io\formats\csvs.py", line 234, in save
#     storage_options=self.storage_options,
#   File "C:\Application\Python\lib\site-packages\pandas\io\common.py", line 647, in get_handle
#     newline="",
# FileNotFoundError: [Errno 2] No such file or directory: 'data/us_stock/stock/PRN.csv'


with tqdm(total=len(market), ncols=80) as pbar:
    for i in market:
        stock_us_daily_df = ak.stock_us_daily(symbol=i, adjust="qfq")
        # print(stock_us_daily_df)
        stock_us_daily_df.to_csv("data/us_stock/stock/" + i + ".csv")

        pbar.update(1)

# stock_name = data.
# for i in data:
#     print(i)

# stock_us_daily_df = ak.stock_us_daily(symbol="AAPL", adjust="qfq")
# print(stock_us_daily_df)

#  ,name,cname,category,symbol,price,diff,chg,preclose,open,high,low,amplitude,volume,mktcap,pe,market,category_id
# 0,"Apple, Inc.",苹果公司,计算机,AAPL,125.89,2.35,1.90,123.54,124.07,126.16,123.85,1.87%,75169343,2100805856405,27.97555542,NASDAQ,

