# 螺纹钢商品期货主连
# 使用R_Breaker算法，量化交易回测，多线程加速训练

from r_breaker_move import TRADING
import time

# from multiprocessing import Process, RLock


def main():
    trade_algo = TRADING(slippage=0, handling_fee=5)
    counts = []
    t = time.time()
    processor_num = 1

    # a_lis = [i / 100 for i in range(30, 60, 5)]  # 0.3 起
    # b_lis = [i / 100 for i in range(101, 121, 2)]  # 大一点好
    # c_lis = [i / 100 for i in range(4, 8, 1)]
    # d_lis = [i / 100 for i in range(5, 35, 5)]  # 小一点好

    a_lis = [i / 100 for i in range(30, 45, 5)]
    b_lis = [i / 100 for i in range(106, 107, 1)]
    c_lis = [i / 100 for i in range(6, 7, 1)]
    d_lis = [i / 100 for i in range(25, 30, 5)]
    # t_lis = [i for i in range(10, 150, 10)]
    t_lis = [i for i in range(120, 130, 10)]

    para = trade_algo.parameter_list(a_lis, b_lis, c_lis, d_lis, t_lis)

    slip_len = int(len(para) / processor_num)  # 每个核处理的参数列表的长度

    slip_id_list = []
    for i in range(processor_num - 1):
        slip_id_list.append(para[i * slip_len:(i + 1) * slip_len])
    slip_id_list.append(para[(processor_num - 1) * slip_len:])

    trade_algo.main(0, slip_id_list[0], slip_len, 0)

    # # lock = RLock()  # 进程锁
    # for x in range(processor_num):
    #     process = Process(target=trade_algo.main, args=(x, slip_id_list[x], slip_len, 0))
    #     counts.append(process)
    #     process.start()
    #
    # # for count in counts:
    # #     count.join()
    #
    # e = counts.__len__()
    # while True:
    #     for th in counts:
    #         if not th.is_alive():
    #             e -= 1
    #     if e <= 0:
    #         break
    # print("Multiprocess cpu", time.time() - t)


if __name__ == "__main__":
    main()
