# 螺纹钢商品期货主连
# 使用R_Breaker算法，量化交易回测

import numpy as np
import pandas as pd
from tqdm import tqdm
from maximum_drawdown import MaximumDrawdown
import re
import os
import math
import matplotlib as plt


def train_test_data(input_data):
    """
    将原始数据处理成时间序列
    :param input_data: 原始数据，包含open、high、low、close、volume和均线
    :return: 处理好的时间序列片段
    """
    # 按照一定的时间周期，将过去n minutes的数据整合成时间序列
    # 取n = 60
    period = 60
    length = len(input_data)
    # for i in range(period, length):


def data_15min():
    data = pd.read_csv("Data/RB9999.XSGE/RB9999.XSGE.csv")
    # train_test_data(data)
    data_15min = []
    period = 15
    for i in range(int(len(data) / period) - 1):
        data_15min.append([data.iloc[(i+1) * period - 1, 1],
                          data['open'][i * period], data['close'][(i+1) * period - 1],
                          max(data['high'][i * period:(i+1) * period]), min(data['low'][i * period:(i+1) * period])])
    account_log = pd.DataFrame(data_15min, columns=['time', 'open', 'close', 'high', 'low'])

    account_log.to_csv("account_15min.csv", index_label=None)


def time_trans(x):
    return x[:-9]


def day_k_process():
    data = pd.read_csv("Data/RB9999.XSGE/RB9999.XSGE.csv", index_col=0)
    data.columns = ['time_min', 'open', 'close', 'high', 'low', 'volume', 'money']
    time_day = data.time_min

    # time_day = pd.DataFrame(time_day, columns=['time_day'])
    print(type(time_day))
    time_day = time_day.map(lambda x: x[:-9])

    time_day = time_day.rename('time_day')

    data = pd.concat((time_day, data), axis=1)
    # # print(time_day, type(data.time_day[1]))
    # data.time_day = data.time_day.apply(time_trans)
    print(data.columns)
    data.iloc[:, 0:7].to_csv('Data/r_breaker/r_breaker.csv')

    # ===============================================================
    data = pd.read_csv("Data/r_breaker/r_breaker.csv")
    day_time = data.time_day
    daily = [day_time[0]]
    for i in day_time:
        if i == daily[-1]:
            pass
        else:
            daily.append(i)
    print(daily)
    day_data = pd.DataFrame(columns=["time", "open", "high", "low", "close"])
    with tqdm(total=len(daily)) as pbar:
        for day in daily:
            day_data = day_data.append([{'time': day,
                                         'open': data[data.time_day == day].open.iloc[0],
                                         'high': max(data[data.time_day == day].high),
                                         'low': min(data[data.time_day == day].low),
                                         'close': data[data.time_day == day].close.iloc[-1]}])
            pbar.update(1)
        # print(day_data)
        day_data.reset_index(drop=True, inplace=True)
        day_data.to_csv("day_data.csv")


if __name__ == "__main__":
    # r_breaker
    day_data = pd.read_csv("day_data.csv", index_col=0)                      # 读入处理好的日K数据
    # day_data = day_data.iloc[1689:, :]        # 从2016年开始回测
    day_data = day_data.iloc[2190:, :]          # 从2018年开始回测
    day_data.reset_index(drop=True, inplace=True)
    data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)          # 原始的1 min数据
    data = data.iloc[427260:, :]
    data.reset_index(drop=True, inplace=True)
    data_id = day_data.time                                     # 从日K数据中，提取日期信息
    # print(data.time_min)
    # print(day_data.time)

    drawdown_cal = MaximumDrawdown()
    # 记录每次交易的min级账户情况
    save_log = pd.DataFrame()
    # 记录所有交易的利润率等
    save_all = pd.DataFrame(columns=["No", "a", "b", "c", "d" "rate", "trading", 'maxdrawdown'])
    # 记录最后一次交易的min级交易详情，持仓等
    trade_log = pd.DataFrame()
    n = 0
    slippage = 0.5            # 滑点与交易手续费
    handling_fee = 5        # 交易手续费
    stop_loss_per = 0.95

    # a = 0.35
    # b = 1.07
    # c = 0.07
    # d = 0.25
    # 0.15
    # 1.09
    # 0.07
    # 0.25

    # for ia in range(35, 45, 10):
    #     a = ia / 100
    #     for ib in range(106, 108, 1):
    #         b = ib / 100
    #         for ic in range(5, 10, 1):
    #             c = ic / 100
    #             for id_d in range(20, 35, 5):
    # No:  1  a:  0.35  b:  1.07  c:  0.07  d:  0.2  Rating:  0.888  Drawdown:  -0.205   Trading times :  452
    for ia in range(50, 55, 10):
        a = ia / 100
        for ib in range(104, 105, 1):
            b = ib / 100
            for ic in range(4, 5, 1):
                c = ic / 100
                for id_d in range(20, 25, 5):
    # for ia in range(15, 55, 5):
    #     a = ia / 100
    #     for ib in range(100, 110, 1):
    #         b = ib / 100
    #         for ic in range(4, 8, 1):
    #             c = ic / 100
    #             for id_d in range(20, 35, 5):

                    d = id_d / 100
                    # 更新a,b,c,d参数后，重新初始化交易信息
                    account = 10000
                    account_log = []
                    position_log = []
                    trading = 0
                    # 策略交易的主体
                    with tqdm(total=len(data_id)-1) as pbar:
                        # for i in range(1, 50):
                        for i in range(1, len(data_id)):
                            open_price = day_data.iloc[i-1, 1]
                            high = day_data.iloc[i-1, 2]
                            low = day_data.iloc[i-1, 3]
                            close = day_data.iloc[i-1, 4]

                            # 计算r_breaker相关的指标
                            Bsetup = int(low - a * (high - close))           # 观察买入价
                            Ssetup = int(high + a * (close - low))           # 观察卖出价

                            Benter = int((b / 2) * (high + low) - c * high)    # 反转买入价
                            Senter = int((b / 2) * (high + low) - c * low)     # 反转卖出价

                            Sbreak = int(Bsetup - d * (Ssetup - Bsetup))     # 突破买入价
                            Bbreak = int(Ssetup + d * (Ssetup - Bsetup))     # 突破卖出价

                            # sub_min截取每天的1 min数据
                            sub_min = data[data.time_day == data_id[i]]
                            day_high = sub_min.iloc[0, 4]                   # 当日最高价
                            day_low = sub_min.iloc[0, 5]                    # 当日最低价
                            single_posi = int(account/6000)
                            trading_fee = handling_fee + 10 * single_posi * slippage  # 交易成本
                            # single_posi = 1
                            position_trend = 0

                            for k in range(1, len(sub_min)):
                                price = sub_min.iloc[k, 3]
                                day_high = max(day_high, sub_min.iloc[k, 4])
                                day_low = min(day_low, sub_min.iloc[k, 5])

                                # 判断是否为趋势行情
                                if position_trend == 0:
                                    if price > Bbreak:
                                        position_trend = single_posi
                                        stop_price = price
                                        trading += 1
                                        account -= trading_fee
                                    elif price < Sbreak:
                                        position_trend = -single_posi
                                        stop_price = price
                                        trading += 1
                                        account -= trading_fee
                                    else:
                                        pass
                                elif position_trend > 0:
                                    # 止损检测
                                    if stop_price * stop_loss_per > price:
                                        position_trend = 0
                                        account += 10 * single_posi * (sub_min.iloc[k, 3] - sub_min.iloc[k - 1, 3])
                                    else:
                                        stop_price = max(stop_price, price)
                                        account += 10 * single_posi * (sub_min.iloc[k, 3] - sub_min.iloc[k - 1, 3])
                                elif position_trend < 0:
                                    if stop_price * (2 - stop_loss_per) < price:
                                        position_trend = 0
                                        account -= 10 * single_posi * (sub_min.iloc[k, 3] - sub_min.iloc[k-1, 3])
                                    else:
                                        stop_price = min(stop_price, price)
                                        account -= 10 * single_posi * (sub_min.iloc[k, 3] - sub_min.iloc[k - 1, 3])

                                account_log.append([account])
                                position_log.append(position_trend)

                            pbar.update(1)

                    max_drawdown = round(drawdown_cal.calculate(account_log), 4)
                    account_log = pd.DataFrame(account_log, columns=[str(a) + '_' + str(b) + '_' + str(c) + '_' + str(d)])
                    rate = round((account - 10000) / 100, 2)
                    save_log = pd.concat((save_log, account_log), axis=1)

                    n += 1

                    save_all = save_all.append([{"No": n,
                                                 'a': a,
                                                 'b': b,
                                                 "c": c,
                                                 "d": d,
                                                 "rate": rate,
                                                 "trading": trading,
                                                 "maxdrawdown": max_drawdown * 100}])

                    print("No: ", n,
                          " a: ", a,
                          " b: ", b,
                          " c: ", c,
                          " d: ", d,
                          " Rate: ", rate,
                          " Drawdown: ", max_drawdown)  # " var : ", account_log.account.std()

                    if n % 10 == 0:
                        # 保存迭代的结果
                        save_all.reset_index(drop=True, inplace=True)
                        save_all.to_csv("Result/r_breaker/save_all.csv", index_label=None)
                        save_log.to_csv("Result/r_breaker/account_log" + ".csv", index_label=None)

    position_log = pd.DataFrame(position_log, columns=["position"])
    account_log.columns = ["account"]
    account_log = pd.concat((account_log, position_log), axis=1)
    account_log.to_csv('trading_trend.csv')
