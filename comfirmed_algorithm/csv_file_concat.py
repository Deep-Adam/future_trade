import pandas as pd
import os


class File_Concat():
    def file_concat(self):
        file_list = os.listdir("Result/r_breaker/multi")
        account_list = [i for i in file_list if i[0] == 'a']
        save_list = [i for i in file_list if i[0] == 's']
        account_log = pd.DataFrame()
        for i in account_list:
            # pd.concat((account_log, ))
            account_log = pd.concat([account_log, pd.read_csv(os.path.join("Result/r_breaker/multi", i), index_col=0)],
                                    axis=1)
            # print(account_log)
        account_log.to_csv(os.path.join("Result/r_breaker", "account_log.csv"))

        save_all = pd.DataFrame(columns=["No", "a", "b", "c", "d", "rate", "trading", "maxdrawdown"])
        for i in save_list:
            save_all = pd.concat([save_all, pd.read_csv(os.path.join("Result/r_breaker/multi", i), index_col=0)], axis=0)
            # print(pd.read_csv(os.path.join("Result/r_breaker/multi", i), index_col=0))
            # print(save_all)
        save_all.reset_index(drop=True, inplace=True)
        save_all.to_csv(os.path.join("Result/r_breaker", "save_all.csv"))


def main():
    file_caoncat = File_Concat()
    file_caoncat.file_concat()


if __name__ == "__main__":
    main()
