# from tqsdk import TqApi, TargetPosTask
# from tqsdk.tafunc import ma
import numpy as np
import pandas as pd
from tqdm import tqdm
from maximum_drawdown import MaximumDrawdown

# api = TqApi()

SYMBOL_rb = "SHFE.rb2001"
SYMBOL_i = "DCE.i2001"
SYMBOL_j = "DCE.j2001"


# klines_rb = api.get_kline_serial(SYMBOL_rb, 86400)      # rb price
# klines_i = api.get_kline_serial(SYMBOL_i, 86400)        # iron ore price
# klines_j = api.get_kline_serial(SYMBOL_j, 86400)        # coke price
#
# target_pos_rb = TargetPosTask(api, SYMBOL_rb)
# target_pos_i = TargetPosTask(api, SYMBOL_i)
# target_pos_j = TargetPosTask(api, SYMBOL_j)


class TRADING:
    def __init__(self, slippage, handling_fee):
        self.slippage = slippage            # 滑点与交易手续费
        self.handling_fee = handling_fee    # 交易手续费

    def dataprocess(self):
        data_rb = pd.read_csv("Data/profit/RB9999.XSGE.csv")
        data_i = pd.read_csv("Data/profit/I9999.XDCE.csv")
        data_j = pd.read_csv("Data/profit/J9999.XDCE.csv")
        print(data_rb)
        print(data_i)
        print(data_j)

    def moving_average(self, data_input, period):
        """
        计算移动平均线
        :param data_input: 股票价格
        :param period:
        :return: 移动平均线的数组
        """
        # 计算10/20/40/60/120日均线
        length = len(data_input)
        mv = []

        for t in period:
            sub_mv = []
            for i in range(length):
                sub_mv.append(round(np.average(data_input[max(0, i - t + 1): i + 1]), 2))

        mv.append(sub_mv)

        return mv

    # 计算钢厂利润线，并将利润线画到副图
    def cal_spread(self, klines_rb, klines_i, klines_j):
        index_spread = klines_rb - 1.6 * klines_i - 0.5 * klines_j  # profit of iron factory
        index_spread = round(index_spread, 2)
        # ma_spread = self.moving_average(index_spread, 10)  # moving average of period 10
        ma_spread = index_spread.mean(axis=0)
        ma_spread = round(ma_spread, 2)
        # print(ma_spread)

        spread_std = np.std(index_spread)  # 标准差
        spread_std = round(spread_std, 4)

        klines_rb["index_spread"] = index_spread
        klines_rb["index_spread.board"] = "index_spread"

        return index_spread, ma_spread, spread_std

    def main(self, order_save):
        year_backbone = 2020
        # 从2020年开始回测
        if year_backbone == 2020:
            day_data_I = pd.read_csv("Data/profit/day_I9999_2020.csv", index_col=0)
            day_data_J = pd.read_csv("Data/profit/day_J9999_2020.csv", index_col=0)
            day_data_RB = pd.read_csv("Data/profit/day_RB9999_2020.csv", index_col=0)

            # print(day_data_I)
            # print(day_data_J)
            # print(day_data_RB)

            data_I = pd.read_csv("Data/profit/I9999_2020.csv", index_col=0)
            data_J = pd.read_csv("Data/profit/J9999_2020.csv", index_col=0)
            data_RB = pd.read_csv("Data/profit/RB9999_2020.csv", index_col=0)

            # print(data_I)
            # print(data_J)
            # print(data_RB)

            # data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
            # data = data.iloc[769290:, :]
            # data.reset_index(drop=True, inplace=True)

        else:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)  # 读入处理好的日K数据
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)  # 原始的1 min数据

        len_day = len(day_data_I)
        position = 0

        account = 10000
        single_posi = int(account / 8000)
        account_log = []
        order_log = []
        position_log = []

        trading = 0
        sub_order_i = dict()
        sub_order_j = dict()
        sub_order_rb = dict()
        print(len_day)

        for i in range(10, len_day):
            klines_i = day_data_I.close[i - 10: i]
            klines_j = day_data_J.close[i - 10: i]
            klines_rb = day_data_RB.close[i - 10: i]

            index_spread, ma_spread, spread_std = self.cal_spread(klines_rb, klines_i, klines_j)

            # data_i = data_I[data_I.date == day_data_I[i]]
            # data_i.reset_index(drop=True, inplace=True)
            # data_j = data_J[data_J.date == day_data_I[i]]
            # data_j.reset_index(drop=True, inplace=True)
            # data_rb = data_RB[data_RB.date == day_data_I[i]]
            # data_rb.reset_index(drop=True, inplace=True)

            single_posi = 1
            trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # 交易成本
            # print(spread_std)
            # print(ma_spread)
            # print(index_spread.iloc[-1])

            # 价差序列下穿上轨，利润冲高回落进行回复，策略空螺纹钢、多焦煤焦炭
            if index_spread.iloc[-1] > 0.5 * spread_std + ma_spread:
                # 当前没有持仓
                if position == 0:
                    position = single_posi
                    trading += 1
                    sub_order_i.update({'longorshort': 'long', 'time': day_data_I.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_I.iloc[i, 4], 'quantity': single_posi,
                                        'high': day_data_I.iloc[i, 4], 'low': day_data_I.iloc[i, 4]})
                    sub_order_j.update({'longorshort': 'long', 'time': day_data_J.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_J.iloc[i, 4], 'quantity': single_posi,
                                        'high': day_data_J.iloc[i, 4], 'low': day_data_J.iloc[i, 4]})
                    sub_order_rb.update({'longorshort': 'short', 'time': day_data_RB.iloc[i, 0],
                                         'holding_time': 1,
                                         'open': day_data_RB.iloc[i, 4], 'quantity': -single_posi,
                                         'high': day_data_RB.iloc[i, 4], 'low': day_data_RB.iloc[i, 4]})
                elif position > 0:
                    sub_order_i.update({'holding_time': sub_order_i['holding_time'] + 1,
                                        'high': max(sub_order_i['high'], day_data_I.iloc[i, 2]),
                                        'low': min(sub_order_i['low'], day_data_I.iloc[i, 3])})
                    sub_order_j.update({'holding_time': sub_order_j['holding_time'] + 1,
                                        'high': max(sub_order_j['high'], day_data_J.iloc[i, 2]),
                                        'low': min(sub_order_j['low'], day_data_J.iloc[i, 3])})
                    sub_order_rb.update({'holding_time': sub_order_rb['holding_time'] + 1,
                                         'high': max(sub_order_rb['high'], day_data_RB.iloc[i, 2]),
                                         'low': min(sub_order_rb['low'], day_data_RB.iloc[i, 3])})
                else:
                    # close position
                    sub_order_i.update({'close': day_data_I.iloc[i, 4]})
                    sub_order_j.update({'close': day_data_J.iloc[i, 4]})
                    sub_order_rb.update({'close': day_data_RB.iloc[i, 4]})

                    sub_order_i.update(
                        {"profit": sub_order_i['quantity'] * 10 * (
                                    sub_order_i['close'] - sub_order_i['open']) - trading_fee})
                    sub_order_j.update(
                        {"profit": sub_order_j['quantity'] * 10 * (
                                    sub_order_j['close'] - sub_order_j['open']) - trading_fee})
                    sub_order_rb.update(
                        {"profit": sub_order_rb['quantity'] * 10 * (
                                    sub_order_rb['close'] - sub_order_rb['open']) - trading_fee})

                    order_profit = sub_order_i['profit'] + sub_order_j['profit'] + sub_order_rb['profit']

                    account += order_profit

                    order_log.extend([sub_order_i, sub_order_j, sub_order_rb])
                    sub_order_i = {}
                    sub_order_j = {}
                    sub_order_rb = {}

                    # a new position
                    position = single_posi
                    trading += 1
                    sub_order_i.update({'longorshort': 'long', 'time': day_data_I.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_I.iloc[i, 4], 'quantity': single_posi,
                                        'high': day_data_I.iloc[i, 4], 'low': day_data_I.iloc[i, 4]})
                    sub_order_j.update({'longorshort': 'long', 'time': day_data_J.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_J.iloc[i, 4], 'quantity': single_posi,
                                        'high': day_data_J.iloc[i, 4], 'low': day_data_J.iloc[i, 4]})
                    sub_order_rb.update({'longorshort': 'short', 'time': day_data_RB.iloc[i, 0],
                                         'holding_time': 1,
                                         'open': day_data_RB.iloc[i, 4], 'quantity': -single_posi,
                                         'high': day_data_RB.iloc[i, 4], 'low': day_data_RB.iloc[i, 4]})

            # 价差序列上穿下轨，利润过低回复上升，策略多螺纹钢、空焦煤焦炭
            elif index_spread.iloc[-1] < ma_spread - 0.5 * spread_std:
                if position == 0:
                    position = -single_posi
                    trading += 1
                    sub_order_i.update({'longorshort': 'short', 'time': day_data_I.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_I.iloc[i, 4], 'quantity': -single_posi,
                                        'high': day_data_I.iloc[i, 4], 'low': day_data_I.iloc[i, 4]})
                    sub_order_j.update({'longorshort': 'short', 'time': day_data_J.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_J.iloc[i, 4], 'quantity': -single_posi,
                                        'high': day_data_J.iloc[i, 4], 'low': day_data_J.iloc[i, 4]})
                    sub_order_rb.update({'longorshort': 'short', 'time': day_data_RB.iloc[i, 0],
                                         'holding_time': 1,
                                         'open': day_data_RB.iloc[i, 4], 'quantity': -single_posi,
                                         'high': day_data_RB.iloc[i, 4], 'low': day_data_RB.iloc[i, 4]})
                elif position < 0:
                    sub_order_i.update({'holding_time': sub_order_i['holding_time'] + 1,
                                        'high': max(sub_order_i['high'], day_data_I.iloc[i, 2]),
                                        'low': min(sub_order_i['low'], day_data_I.iloc[i, 3])})
                    sub_order_j.update({'holding_time': sub_order_j['holding_time'] + 1,
                                        'high': max(sub_order_j['high'], day_data_J.iloc[i, 2]),
                                        'low': min(sub_order_j['low'], day_data_J.iloc[i, 3])})
                    sub_order_rb.update({'holding_time': sub_order_rb['holding_time'] + 1,
                                         'high': max(sub_order_rb['high'], day_data_RB.iloc[i, 2]),
                                         'low': min(sub_order_rb['low'], day_data_RB.iloc[i, 3])})
                else:
                    # close position
                    sub_order_i.update({'close': day_data_I.iloc[i, 4]})
                    sub_order_j.update({'close': day_data_J.iloc[i, 4]})
                    sub_order_rb.update({'close': day_data_RB.iloc[i, 4]})

                    sub_order_i.update(
                        {"profit": sub_order_i['quantity'] * 10 * (
                                    sub_order_i['close'] - sub_order_i['open']) - trading_fee})
                    sub_order_j.update(
                        {"profit": sub_order_j['quantity'] * 10 * (
                                    sub_order_j['close'] - sub_order_j['open']) - trading_fee})
                    sub_order_rb.update(
                        {"profit": sub_order_rb['quantity'] * 10 * (
                                    sub_order_rb['close'] - sub_order_rb['open']) - trading_fee})

                    order_profit = sub_order_i['profit'] + sub_order_j['profit'] + sub_order_rb['profit']

                    account += order_profit
                    order_log.extend([sub_order_i, sub_order_j, sub_order_rb])
                    sub_order_i = {}
                    sub_order_j = {}
                    sub_order_rb = {}

                    # a new position
                    position = -single_posi
                    trading += 1
                    sub_order_i.update({'longorshort': 'short', 'time': day_data_I.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_I.iloc[i, 4], 'quantity': -single_posi,
                                        'high': day_data_I.iloc[i, 4], 'low': day_data_I.iloc[i, 4]})
                    sub_order_j.update({'longorshort': 'short', 'time': day_data_J.iloc[i, 0],
                                        'holding_time': 1,
                                        'open': day_data_J.iloc[i, 4], 'quantity': -single_posi,
                                        'high': day_data_J.iloc[i, 4], 'low': day_data_J.iloc[i, 4]})
                    sub_order_rb.update({'longorshort': 'short', 'time': day_data_RB.iloc[i, 0],
                                         'holding_time': 1,
                                         'open': day_data_RB.iloc[i, 4], 'quantity': -single_posi,
                                         'high': day_data_RB.iloc[i, 4], 'low': day_data_RB.iloc[i, 4]})
            else:
                if position == 0:
                    pass
                else:
                    sub_order_i.update({'holding_time': sub_order_i['holding_time'] + 1,
                                        'high': max(sub_order_i['high'], day_data_I.iloc[i, 2]),
                                        'low': min(sub_order_i['low'], day_data_I.iloc[i, 3])})
                    sub_order_j.update({'holding_time': sub_order_j['holding_time'] + 1,
                                        'high': max(sub_order_j['high'], day_data_J.iloc[i, 2]),
                                        'low': min(sub_order_j['low'], day_data_J.iloc[i, 3])})
                    sub_order_rb.update({'holding_time': sub_order_rb['holding_time'] + 1,
                                         'high': max(sub_order_rb['high'], day_data_RB.iloc[i, 2]),
                                         'low': min(sub_order_rb['low'], day_data_RB.iloc[i, 3])})

            position_log.append(position)
            account_log.append([account])


        winrate = 0
        for i in order_log:
            if i["profit"] > 0:
                winrate += 1
        winrate = round(100 * winrate / (len(order_log) + 0.00001), 2)

        if order_save == 1:
            drawdown_cal = MaximumDrawdown()
            print(account_log)

            rate = round(((account_log[-1][0] - account_log[0][0]) / (account_log[0][0])) * 100, 2)
            max_drawdown = drawdown_cal.calculate(account_log)
            print('rate:', rate,
                  '\ntrading:', trading,
                  '\ndraw:', round(max_drawdown * 100, 2))
            order_log = pd.DataFrame.from_dict(order_log, orient='columns')
            order_log.to_csv("Result/r_breaker/order_log.csv")

            print('winrate:', winrate)

        save_log = pd.DataFrame()
        # 记录所有交易的利润率等
        save_all = pd.DataFrame(columns=["No", "a", "b", "c", "d", "t", "rate", 'drawdown', "trading", "winrate"])
        account_log = pd.DataFrame(account_log, columns=['account'])

        save_log = pd.concat((save_log, account_log), axis=1)

        # save_log.index = day_data.time[1: (len(save_log) + 1)]
        save_all.reset_index(drop=True, inplace=True)
        save_all.to_csv("Result/profit/save_all.csv", index_label=None)
        save_log.to_csv("Result/profit/account_log.csv", index_label=None)

        return account_log, position_log, trading, winrate

        # data_id = day_data.time  # 从日K数据中，提取日期信息
        #
        # # lock.acquire()      # 进程锁
        #
        # drawdown_cal = MaximumDrawdown()
        # # 记录每次交易的min级账户情况
        # save_log = pd.DataFrame()
        # # 记录所有交易的利润率等
        # save_all = pd.DataFrame(columns=["No", "a", "b", "c", "d", "t", "rate", 'drawdown', "trading", "winrate"])
        # # 记录最后一次交易的min级交易详情，持仓等
        # n = pro_id * stride
        # stop_loss_per = 0.90
        # len_data = len(day_data)
        #
        # with tqdm(total=stride, ncols=80) as pbar:
        #     for [a, b, c, d, period] in para_list:
        #         account_log, position_log, trading, winrate = self.main_trading(data_id, day_data, data, len_data,
        #                                                                         order_trigger)
        #
        #         max_drawdown = round(drawdown_cal.calculate(account_log), 4)
        #         rate = round((account_log[-1][0] - 10000) / 100, 2)
        #
        #         account_log = pd.DataFrame(account_log,
        #                                    columns=[str(a) + '_' + str(b) + '_' + str(c) + '_' + str(d)])
        #
        #         save_log = pd.concat((save_log, account_log), axis=1)
        #
        #         # 记录每次回测的交易概述
        #         save_all = save_all.append([{"No": n,
        #                                      'a': a,
        #                                      'b': b,
        #                                      "c": c,
        #                                      "d": d,
        #                                      "t": period,
        #                                      "rate": rate,
        #                                      "drawdown": max_drawdown * 100,
        #                                      "trading": trading,
        #                                      "winrate": winrate}])
        #
        #         # print("No: ", n,
        #         #       " a: ", a,
        #         #       " b: ", b,
        #         #       " c: ", c,
        #         #       " d: ", d,
        #         #       " Rate: ", rate,
        #         #       " Drawdown: ", max_drawdown * 100)
        #         # print(save_log.index, day_data.time[1:])
        #
        #         if n % 10 == 0:
        #             # 保存迭代的结果
        #             save_log.index = day_data.time[1: (len(save_log) + 1)]
        #             save_all.reset_index(drop=True, inplace=True)
        #             save_all.to_csv("Result/r_breaker/save_all_" + str(pro_id) + ".csv", index_label=None)
        #             save_log.to_csv("Result/r_breaker/account_log_" + str(pro_id) + ".csv", index_label=None)
        #
        #         n += 1
        #         pbar.update(1)
        #



# index_spread, ma_spread, spread_std = cal_spread(klines_rb, klines_i, klines_j)
#
# print("ma_spread是%.2f,index_spread是%.2f,spread_std是%.2f" % (ma_spread.iloc[-1], index_spread.iloc[-1], spread_std))
#
#
# while True:
#     # api.wait_update()
#     # 每次有新日线生成时重新计算利润线
#     if api.is_changing(klines_j.iloc[-1], "datetime"):
#         index_spread, ma_spread, spread_std = cal_spread(klines_rb, klines_i, klines_j)
#         print("ma_spread是%.2f,index_spread是%.2f,spread_std是%.2f" % (
#             ma_spread.iloc[-1], index_spread.iloc[-1], spread_std))
#
#     # 价差序列下穿上轨，利润冲高回落进行回复，策略空螺纹钢、多焦煤焦炭
#     if index_spread.iloc[-1] > 0.5 * spread_std + ma_spread.iloc[-1]:
#         target_pos_rb.set_target_volume(-100)
#         target_pos_i.set_target_volume(100)
#         target_pos_j.set_target_volume(100)
#
#     # 价差序列上穿下轨，利润过低回复上升，策略多螺纹钢、空焦煤焦炭
#     elif index_spread.iloc[-1] < ma_spread.iloc[-1] - 0.5 * spread_std:
#         target_pos_rb.set_target_volume(100)
#         target_pos_i.set_target_volume(-100)
#         target_pos_j.set_target_volume(-100)


def main():
    trade = TRADING(slippage=0, handling_fee=5)
    trade.main(1)


if __name__ == "__main__":
    main()
