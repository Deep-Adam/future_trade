from jqdatasdk import *

auth("18701936720", "Abcdef66")

# import talib
# import re


class DownloadData:
    # def __init__(self, futureid):
    #     self.futureid = futureid

    def data_download(self, futureid):
        self.futureid = futureid
        start = get_query_count()['spare']
        # # data = get_all_securities(types=['futures'], date=[:2])
        # data = get_all_securities(['futures'], '2020-11-10')

        # data = get_price("RB9999.XSGE", start_date='2020-11-01', end_date='2020-11-07', frequency='minute',
        #                  fields=None, skip_paused=False, fq='pre')
        # data = get_price("RB9999.XSGE", start_date='2011-01-01', end_date='2019-12-31', frequency='minute',
        #                  fields=None, skip_paused=False, fq='pre')
        # data = get_price("RB9999.XSGE", start_date='2009-01-01', end_date='2010-12-31', frequency='minute',
        #                  fields=None, skip_paused=False, fq='pre')
        # data = get_price("RB9999.XSGE", start_date='2008-01-01', end_date='2008-12-31', frequency='minute',
        #                  fields=None, skip_paused=False, fq='pre')

        # data = get_price("I9999.XDCE", start_date='2009-01-01', end_date='2020-11-07', frequency='minute',
        #                  fields=None, skip_paused=False, fq='pre')

        # data = get_price(self.futureid, start_date='2009-01-01', end_date='2020-11-07', frequency='minute',
        #                  fields=None,
        #                  skip_paused=False, fq='pre')

        # data = get_price(self.futureid, start_date='2009-01-01', end_date='2020-11-07', frequency='minute',
        #                  fields=None, skip_paused=False, fq='pre')

        # data = get_price(self.futureid, start_date='2020-11-07', end_date='2021-01-26', frequency='minute', fields=None,
        #                  skip_paused=False, fq='pre')
        data = get_price(self.futureid, start_date='2021-01-27', end_date='2021-03-01', frequency='minute', fields=None,
                         skip_paused=False, fq='pre')

        print(get_query_count(), '\n',
              "left spare is : ", start-get_query_count()['spare'])

        data.to_csv(self.futureid + ".csv")

        return 1
        # stocks = list(get_all_securities(['stock']).index)        # 查看所有股票列表
        # print(stocks)


downloaddata = DownloadData()
print(downloaddata.data_download("RB9999.XSGE"))
