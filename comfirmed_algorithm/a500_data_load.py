import pandas as pd
import numpy as np
import os


def data_load(file_path):
    data = pd.read_csv(file_path)
    return data


def data_process(data_input):
    data_out = data_input
    return data_out


def moving_average(data_input):
    """
    计算移动平均线
    :param data_input: 股票价格
    :return: 移动平均线的数组
    """
    # 计算10/20/40/60/120日均线
    days = len(data_input)

    # 10日均线计算
    mv_10 = []
    for i in range(9, days):
        mv_10.append(np.average(data_input[max(0, i - 9): i+1]))

    # 20日均线计算
    mv_20 = []
    for i in range(19, days):
        mv_20.append(np.average(data_input[max(0, i - 19): i + 1]))

    # 40日均线计算
    mv_40 = []
    for i in range(39, days):
        mv_40.append(np.average(data_input[max(0, i - 39): i + 1]))

    # 60日均线计算
    mv_60 = []
    for i in range(59, days):
        mv_60.append(np.average(data_input[max(0, i - 59): i + 1]))

    # 120日均线计算
    mv_120 = []
    for i in range(119, days):
        mv_120.append(np.average(data_input[max(0, i - 119): i + 1]))

    return mv_10, mv_20, mv_40, mv_60, mv_120


def feature_engineer(data_input):
    """
    计算价格相关特征
    :param data_input: 输入数据，收盘价
    :return: 输出新的特征
    """
    mv_10, mv_20, mv_40, mv_60, mv_120 = moving_average(data_input)     # 计算均线特征
    mv = [mv_10, mv_20, mv_40, mv_60, mv_120]

    return mv


if __name__ == "__main__":
    # ======================= 数据导入 ========================
    filelist = os.listdir("data/A500")

    data = pd.DataFrame()

    for file in filelist:
        file_data = pd.read_csv(os.path.join("data/A500", file))
        length_file = len(file_data)

        print(file_data.ticker.iloc[0])

        while length_file > 0:
            sub_ID = file_data.ticker.iloc[0]
            sub_data = file_data[file_data.ticker == sub_ID]

            sub_ID = "0" * (6-len(str(sub_ID))) + str(sub_ID)
            print(sub_ID, type(sub_ID))
            # print("Data/a500_single" + sub_ID + ".csv")
            sub_data.to_csv(os.path.join("Data/a500_single", sub_ID + ".csv"))

            file_data = file_data.iloc[len(sub_data):, :]
            length_file -= len(sub_data)


        # for i in range(file_data):
        #     if file_data[i][0] == '0':


        # start_index = file_data[file[0] == 0]
        # print(start_index)

        # data = pd.concat([data, file_data], axis=1)
    print(data)

    # ticker
    print(len(data))



    # data_pdd = data_load("Data/PDD.csv")
    # data_nio = data_load("Data/NIO.csv")
    # a = data_pdd.iloc[33:, :].Close
    # b = data_nio.Close

    # ======================= 数据清洗 ========================
    # stock_pro = data_process(stock_data)

    # ======================= 特征工程 ========================
    # pdd_close = np.array(data_pdd.Close)
    # # print(pdd_close)
    # pdd_mv = feature_engineer(pdd_close)
    # [pdd_mv_10, pdd_mv_20, pdd_mv_40, pdd_mv_60, pdd_mv_120] = pdd_mv
    # print(pdd_mv_10)
    # print('====== ', len(pdd_mv_10))

    # ======================= 权重计算 ========================
    # 可以试着引入波动率

