import pandas as pd


# open_price = 3986
# high_price = 4127
# low_price = 3984
# close_price = 4087
#
# a = 0.35
# b = 1.04
# c = 0.04
# d = 0.3
#
# # 计算r_breaker相关的指标
# bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
# ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价
#
# benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
# senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价
#
# sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
# bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价
#
# print(bbreak)
# print(sbreak)
# print(ssetup, senter)
# print(bsetup, benter)


def set_price_cal(high_price, low_price, close_price):
    # >>>>>>>>>>>> 计算关键价格 <<<<<<<<<<<<<<
    a = 0.5
    b = 1.04
    c = 0.04
    d = 0.2

    # 计算r_breaker相关的指标
    bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
    ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价

    benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
    senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

    sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
    bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

    return bsetup, benter, bbreak, ssetup, senter, sbreak


def thrust():
    para = [0.13, 0.15, 26]
    k1 = 0.13
    k2 = 0.15
    period = 26

    hh = 4588		# N日max high
    lc = 		# N日min close
    hc = 		# N日max close
    ll = 		# N日min low
    range_price = max(hh - lc, hc - ll)
    buyline = data['open'][i] + k1 * range_price
    sellline = data['open'][i] - k2 * range_price


# high_price	low_price	close_price
# 4362	        3296	    4303


if __name__ == "__main__":
    config = pd.read_csv("ctpapi/config.csv")
    bsetup, benter, bbreak, ssetup, senter, sbreak = set_price_cal(high_price=config.high_price,
                                                                   low_price=config.low_price,
                                                                   close_price=config.close_price)
    set_price_use = {"bsetup": bsetup,
                     "benter": benter,
                     "bbreak": bbreak,
                     "ssetup": ssetup,
                     "senter": senter,
                     "sbreak": sbreak}
    print(set_price_use)
