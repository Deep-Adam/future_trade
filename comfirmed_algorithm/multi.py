import time
from threading import Thread
from multiprocessing import Process
import pandas as pd

time_start = time.time()
print(time_start)


def count(x, y):
    # 使程序完成50万计算
    c = 0
    while c < 500000:
        c += 1
        x += x
        y += y


def print_time(threadName, delay, counter):
    while counter:
        output = time.ctime(time.time())
        time.sleep(delay)
        print("%s: %s" % (threadName, output))
        counter -= 1


def main():
    """
    实现打开csv并写入数字的功能
    :return:
    """
    data = pd.DataFrame(columns=["test"])
    data.to_csv("test.csv")

    for i in range(10000):
        data = pd.read_csv("test.csv", index_col=0)
        data = data.append([{"test": i}], ignore_index=True)
        data.to_csv("test.csv")


if __name__ == "__main__":
    # # CPU密集操作
    # # 单线程
    # t = time.time()
    # for x in range(10):
    #     count(1, 1)
    # print("Line cpu", time.time() - t)
    # # time used :  75.82883810997009

    # counts = []
    # t = time.time()
    # for x in range(10):
    #     thread = Thread(target=count, args=(1, 1))
    #     counts.append(thread)
    #     thread.start()
    # e = counts.__len__()
    # while True:
    #     for th in counts:
    #         if not th.is_alive():
    #             e -= 1
    #     if e <= 0:
    #         break
    # print(time.time() - t)
    # # time used :  59.45371413230896
    # # time used :  2.1672191619873047

    counts = []
    t = time.time()
    for x in range(10):
        process = Process(target=count, args=(1, 1))
        counts.append(process)
        process.start()
    e = counts.__len__()
    while True:
        for th in counts:
            if not th.is_alive():
                e -= 1
        if e <= 0:
            break
    print("Multiprocess cpu", time.time() - t)

    # main()

print("\ntime used : ", time.time() - time_start)

# time used :  75.82883810997009


# class myThread(threading.Thread):
#     def __init__(self, threadID, name, counter):
#         threading.Thread.__init__(self)
#         self.threadID = threadID
#         self.name = name
#         self.counter = counter
#
#     def run(self):
#         print("Starting " + self.name)
#         # 获得锁，成功获得锁定后返回True
#         # 可选的timeout参数不填时将一直阻塞直到获得锁定
#         # 否则超时后将返回False
#         threadLock.acquire()
#         # print_time(self.name, self.counter, 3)
#         main()
#         # 释放锁
#         threadLock.release()

# threadLock = threading.Lock()
# threads = []
#
# # 创建新线程
# thread1 = myThread(1, "Thread-1", 1)
# thread2 = myThread(2, "Thread-2", 1)
# thread3 = myThread(3, "Thread-3", 1)
#
# # 开启新线程
# thread1.start()
# thread2.start()
# thread3.start()
#
# # 添加线程到线程列表
# threads.append(thread1)
# threads.append(thread2)
# threads.append(thread3)
#
# # 等待所有线程完成
# for t in threads:
#     t.join()
# print("Exiting Main Thread")


#
# if __name__ == "__main__":
#     time_start = time.clock()
#     array = Array('i', 1)
#     array[0] = 10
#     lock = RLock()
#
#     for i in range(10):
#         p = Process(target=func, args=(i, array, lock))
#         p.start()
#         # func(i, array, lock)
#     print("use time : ", time.clock()-time_start)



# def fun1(a, b):
#     print('fun1')
#     print(a, b)
#     time.sleep(1)
#
#
# def fun2():
#     print('fun2')
#     time.sleep(1)
#
#
# def fun3():
#     print('fun3')
#     time.sleep(2)
#
#
# def fun4():
#     print('fun4')
#     time.sleep(1)
#
#
# def fun5():
#     print('fun5')
#     time.sleep(1)
#     fun4()
#
#
# fun1('foo', 'bar')
# fun2()
# fun3()
# fun5()


# def profileTest():
#     Total =1;
#     for i in range(10):
#         Total=Total*(i+1)
#         print(Total)
#     return Total
#
#
# if __name__ == "__main__":
#    profile.run("profileTest()")


# import os
# import multiprocessing
#
#
# def foo(i):
#     # 同样的参数传递方法
#     print("这里是 ", multiprocessing.current_process().name)
#     print('模块名称:', __name__)
#     print('父进程 id:', os.getppid())  # 获取父进程id
#     print('当前子进程 id:', os.getpid())  # 获取自己的进程id
#     print('------------------------')
#
#
# if __name__ == '__main__':
#     for i in range(5):
#         p = multiprocessing.Process(target=foo, args=(i,))
#         p.start()
