作者：诸神黄昏
链接：https: // zhuanlan.zhihu.com / p / 296522267
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

# !/usr/bin/env python
#  -*- coding: utf-8 -*-
# import asyncio
from tqsdk import TqApi, TqAuth
from contextlib import closing
from time import time

# 创建API实例.
api = TqApi(auth=TqAuth("天勤账号", "账号密码"))


async def taoli(Call='', Put='', Lr=3, Dlr=1):  # 默认3跳，1跳对手价平仓损耗，1跳抹除手续费，1跳纯利润
    quote_C = api.get_quote(Call)
    quote_P = api.get_quote(Put)
    futer = quote_C.underlying_symbol
    quote_F = api.get_quote(futer)

    async with api.register_update_notify([quote_C, quote_P, quote_F]) as update_chan:  #
        async for _ in update_chan:  #
            D_time = (quote_C.last_exercise_datetime - time()) * Lr / (Dlr * 86400)
            if quote_C.exercise_type == 'A':
                lr = quote_F.price_tick * Lr  # 行权盈利点数
                if D_time <= Lr:
                    dlr = lr
                else:
                    dlr = quote_F.price_tick * D_time  # 价格变动百分比，到期盈利幅度

            elif quote_C.exercise_type == 'E':
                if D_time <= Lr:
                    lr = dlr = quote_F.price_tick * Lr
                else:
                    lr = dlr = quote_F.price_tick * D_time
            Cb = quote_C.strike_price + quote_C.ask_price1  # K+C,构建期货多头价格
            Pb = quote_P.strike_price - quote_P.ask_price1  # K-P,构建期货空头价格
            CbPs_Fs = quote_P.strike_price + quote_C.ask_price1 - quote_P.bid_price1  # K+C-P,构建期货多头价格
            PbCs_Fb = quote_P.strike_price + quote_C.bid_price1 - quote_P.ask_price1  # K+C-P,构建期货空头价格
            Fb = quote_F.ask_price1  # 期货买入价
            Fs = quote_F.bid_price1  # 期货卖出价
            kai = False
            if Fs - Cb >= lr:
                print(Call, '-', futer, 'K+C<F:多看涨期权+空期货', Cb, Fs, Cb < Fs, ' 利润:', Fs - Cb)
                kai = True
                break

            if Pb - Fb >= lr:
                print(Put, '+', futer, 'K-P>F:多看跌期权+多期货', Pb, Fb, Pb > Fb, ' 利润:', Pb - Fb)
                kai = True
                break

            if Fs - CbPs_Fs >= dlr:
                print(Call, '-', Put, '-', futer, 'K+C-P<F:多看涨期权+空看跌期权+空期货', CbPs_Fs, Fs, CbPs_Fs < Fs, ' 利润:',
                      Fs - CbPs_Fs)
                kai = True
                break

            if PbCs_Fb - Fb >= dlr:
                print(Put, '-', Call, '+', futer, 'K+C-P>F:多看跌期权+空看涨期权+多期货', PbCs_Fb, Fb, PbCs_Fb > Fb, ' 利润:',
                      PbCs_Fb - Fb)
                kai = True
                break

            if not kai:
                print('监控中：')
                print(Call, '-', futer, 'K+C<F:多看涨期权+空期货', Cb, Fs, Cb < Fs)
                print(Put, '+', futer, 'K-P>F:多看跌期权+多期货', Pb, Fb, Pb > Fb)
                print(Call, '-', Put, '-', futer, 'K+C-P<F:多看涨期权+空看跌期权+空期货', CbPs_Fs, Fs, CbPs_Fs < Fs)
                print(Put, '-', Call, '+', futer, 'K+C-P>F:多看跌期权+空看涨期权+多期货', PbCs_Fb, Fb, PbCs_Fb > Fb)
                # break


C = []
P = []
exchange = ['SHFE', 'DCE', 'CZCE', 'INE']
for e in exchange:
    quote = api.query_quotes(ins_class='OPTION', exchange_id=e, expired=False)
    for i in quote:
        q = api.get_quote(i)
        if q.option_class == 'CALL':
            C.append([i, q.underlying_symbol, q.strike_price])
        elif q.option_class == "PUT":
            P.append([i, q.underlying_symbol, q.strike_price])
    del quote

Options = []
for i in C:
    for j in P:
        if i[1] == j[1] and i[2] == j[2]:
            Options.append([i[0], j[0]])
del C, P

for i in Options:
    api.create_task(taoli(Call=i[0], Put=i[1], Lr=3, Dlr=7))
with closing(api):
    while True:
        api.wait_update()