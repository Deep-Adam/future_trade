# encoding:utf-8

from multiprocessing import Process, Queue
from AlgoPlus.CTP.MdApi import run_tick_engine
from ReqTrade import run_req_authenticate, ReqAuthenticate
from AlgoPlus.CTP.FutureAccount import get_simnow_account


def trade(str_signal, parameter_dict):
    """
    交易函数，发出开单或平仓请求
    :param str_signal:
    :param parameter_dict:
    :return:
    """
    if str_signal == "buy_open":
        # 开多单
        ReqAuthenticate.buy_open(parameter_dict['ExchangeID'],
                                 parameter_dict['InstrumentID'],
                                 parameter_dict['UpperLimitPrice'],
                                 parameter_dict['Volume'])
        # self.write_log(f"=>{ikk}=>发出涨停买开仓请求！")
    elif str_signal == "sell_close":
        # 平多单
        ReqAuthenticate.sell_close(parameter_dict['ExchangeID'],
                                   parameter_dict['InstrumentID'],
                                   parameter_dict['LowerLimitPrice'],
                                   parameter_dict['Volume'],
                                   True)
        # self.write_log(f"=>{ikk}=>发出跌停卖平仓请求！")
    elif str_signal == "sell_open":
        # 开空单
        ReqAuthenticate.sell_open(parameter_dict['ExchangeID'],
                                  parameter_dict['InstrumentID'],
                                  parameter_dict['LowerLimitPrice'],
                                  parameter_dict['Volume'])
        # self.write_log(f"=>{ikk}=>发出跌停卖开仓请求！")
    elif str_signal == "buy_close":
        # 平空单
        ReqAuthenticate.buy_close(parameter_dict['ExchangeID'],
                                  parameter_dict['InstrumentID'],
                                  parameter_dict['UpperLimitPrice'],
                                  parameter_dict['Volume'],
                                  True)
        # self.write_log(f"=>{ikk}=>发出涨停买平仓请求！")


def print_tick(md_queue):
    while True:
        if not md_queue.empty():
            tick = md_queue.get(block=False)
            print(tick)
            # keys = ['TradingDay', 'UpdateTime', 'LastPrice', 'PreSettlementPrice', 'PreClosePrice',
            #         'PreOpenInterest', 'OpenPrice', 'HighestPrice', 'LowestPrice', 'Volume']
            # useful_price = itemgetter(*keys)(test)
            # useful_price = {i: test[i] for i in keys}

            # {'TradingDay': b'20201229', 'InstrumentID': b'rb2105', 'ExchangeID': b'', 'ExchangeInstID': b'',
            # 'LastPrice': 4238.0,
            # 'PreSettlementPrice': 4300.0,
            # 'PreClosePrice': 4244.0,
            # 'PreOpenInterest': 1175467.0,
            # 'OpenPrice': 4256.0,
            # 'HighestPrice': 4282.0,
            # 'LowestPrice': 4206.0,
            # 'Volume': 2596507,
            #
            # 'Turnover': 110201882880.0, 'OpenInterest': 1186624.0, 'ClosePrice': 1.7976931348623157e+308,
            #
            # 'SettlementPrice': 1.7976931348623157e+308, 'UpperLimitPrice': 4515.0, 'LowerLimitPrice': 4085.0,
            # 'PreDelta': 0.0, 'CurrDelta': 1.7976931348623157e+308,
            #
            # 'UpdateTime': b'14:18:51',
            #
            # 'UpdateMillisec': 0,
            # 'BidPrice1': 4238.0, 'BidVolume1': 232, 'AskPrice1': 4239.0, 'AskVolume1': 205,
            #
            # 'BidPrice2': 1.7976931348623157e+308, 'BidVolume2': 0, 'AskPrice2': 1.7976931348623157e+308,
            # 'AskVolume2': 0, 'BidPrice3': 1.7976931348623157e+308, 'BidVolume3': 0,
            # 'AskPrice3': 1.7976931348623157e+308, 'AskVolume3': 0, 'BidPrice4': 1.7976931348623157e+308,
            # 'BidVolume4': 0, 'AskPrice4': 1.7976931348623157e+308, 'AskVolume4': 0,
            # 'BidPrice5': 1.7976931348623157e+308, 'BidVolume5': 0, 'AskPrice5': 1.7976931348623157e+308,
            # 'AskVolume5': 0, 'AveragePrice': 42442.35924647998, 'ActionDay': b'20201229'}

            # print(test)


if __name__ == '__main__':
    # 账户配置
    instrument_id_list = [b'rb2110']
    future_account = get_simnow_account(
        investor_id='180692',                   # SimNow账户
        password='Abcdef99',                     # SimNow账户密码
        instrument_id_list=instrument_id_list,  # 合约列表
        # server_name='TEST'                      # 电信1、电信2、移动、TEST
        server_name='电信1'                      # 电信1、电信2、移动、TEST
    )

    # 共享队列
    share_queue = Queue(maxsize=100)
    print("==============")
    print(share_queue)
    print("==============")
    # 行情进程
    md_process = Process(target=run_tick_engine, args=(future_account, [share_queue]))
    # 交易进程
    print_process = Process(target=print_tick, args=(share_queue, ))

    # 交易参数
    parameter_dict = {
        'ExchangeID': b'SHFE',  # 交易所
        'InstrumentID': b'rb2110',  # 合约代码
        'UpperLimitPrice': 5391,  # 涨停板
        'LowerLimitPrice': 4780,  # 跌停板
        'Volume': 1,  # 报单手数
    }

    # 共享队列
    share_queue = Queue(maxsize=100)
    share_queue.put(parameter_dict)

    # run_req_authenticate(account, share_queue)

    #
    md_process.start()
    print_process.start()

    #
    md_process.join()
    print_process.join()
