# encoding:utf-8

# 模拟交易的代码，产生交易行为

import time
from AlgoPlus.CTP.TraderApiBase import TraderApiBase
from AlgoPlus.CTP.FutureAccount import FutureAccount


class ReqAuthenticate(TraderApiBase):
    def __init__(self, broker_id, td_server, investor_id, password, app_id, auth_code, md_queue=None,
                 page_dir='', private_resume_type=2, public_resume_type=2):
        pass

    def init_extra(self):
        """
        初始化策略参数
        :return:
        """
        # {
        #     'ExchangeID': b'',  # 交易所
        #     'InstrumentID': b'',  # 合约代码
        #     'UpperLimitPrice': 0.0,  # 涨停板
        #     'LowerLimitPrice': 0.0,  # 跌停板
        #     'Volume': 1,  # 报单手数
        # }
        self.parameter_dict = self.md_queue.get(block=False)

    # ############################################################################# #
    def OnRtnOrder(self, pOrder):
        # self.write_log('OnRtnOrder', pOrder)
        pass

    # ############################################################################# #
    def OnRtnTrade(self, pTrade):
        # self.write_log('OnRtnTrade', pTrade)
        pass

    def OnRspQryOrder(self, pOrder, pRspInfo, nRequestID, bIsLast):
        if bIsLast:
            self.write_log('OnRspQryOrder', "查询结果，避免内容太长不输出。")

    def OnRspQryTrade(self, pTrade, pRspInfo, nRequestID, bIsLast):
        if bIsLast:
            self.write_log('OnRspQryTrade', "查询结果，避免内容太长不输出。")

    def OnRspQryInvestorPosition(self, pInvestorPosition, pRspInfo, nRequestID, bIsLast):
        if bIsLast:
            self.write_log('OnRspQryInvestorPosition', "查询结果，避免内容太长不输出。")

    def OnRspQryTradingAccount(self, pTradingAccount, pRspInfo, nRequestID, bIsLast):
        if bIsLast:
            self.write_log('OnRspQryTradingAccount', "查询结果，避免内容太长不输出。")

    def Join(self):
        while True:
            if self.status >= 0 and isinstance(self.parameter_dict, dict):
                # ############################################################################# #
                # 连续5次买开 - 卖平
                ikk = 0
                while ikk < 5:
                    ikk += 1
                    self.buy_open(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.parameter_dict['UpperLimitPrice'], self.parameter_dict['Volume'])
                    self.write_log(f"=>{ikk}=>发出涨停买开仓请求！")
                    time.sleep(3)

                    # 跌停卖平仓
                    self.sell_close(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.parameter_dict['LowerLimitPrice'], self.parameter_dict['Volume'], True)
                    self.write_log(f"=>{ikk}=>发出跌停卖平仓请求！")

                # ############################################################################# #
                # 连续5次卖开 - 买平
                ikk = 0
                while ikk < 5:
                    ikk += 1
                    # 跌停卖开仓
                    self.sell_open(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.parameter_dict['LowerLimitPrice'], self.parameter_dict['Volume'])
                    self.write_log(f"=>{ikk}=>发出跌停卖开仓请求！")
                    time.sleep(3)

                    # 涨停买平仓
                    self.buy_close(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.parameter_dict['UpperLimitPrice'], self.parameter_dict['Volume'], True)
                    self.write_log(f"=>{ikk}=>发出涨停买平仓请求！")

                # ############################################################################# #
                # 买开 - 撤单
                self.buy_open(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.parameter_dict['LowerLimitPrice'], self.parameter_dict['Volume'])
                self.write_log(f"=>发出涨停买开仓请求！")
                time.sleep(3)

                # 撤单
                self.req_order_action(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.order_ref)
                self.write_log(f"=>发出撤单请求！")

                # ############################################################################# #
                # 卖开 - 撤单
                self.sell_open(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.parameter_dict['UpperLimitPrice'], self.parameter_dict['Volume'])
                self.write_log(f"=>发出跌停卖开仓请求！")
                time.sleep(3)

                # 撤单
                self.req_order_action(self.parameter_dict['ExchangeID'], self.parameter_dict['InstrumentID'], self.order_ref)
                self.write_log(f"=>发出撤单请求！")

                # ############################################################################# #
                # 查询订单
                self.req_qry_order()
                self.write_log(f"=>发出查询订单请求！")
                time.sleep(3)

                # ############################################################################# #
                # 查询成交
                self.req_qry_trade()
                self.write_log(f"=>发出查询成交请求！")
                time.sleep(3)

                # ############################################################################# #
                # 查询持仓
                self.req_qry_investor_position()
                self.write_log(f"=>发出查询持仓请求！")
                time.sleep(3)

                # ############################################################################# #
                # 查询资金
                self.req_qry_trading_account()
                self.write_log(f"=>发出查询资金请求！")
                time.sleep(3)

                # ############################################################################# #
                print("看穿式监管认证仿真交易已经完成！可联系期货公司！")
                break

            time.sleep(1)

    def trade_signal(self, tick_price, open_contracts):
        if open_contracts == 0:
            if tick_price["LastPrice"] >= self.set_price["bbreak"]:
                open_contracts = 1
                return "buy_open", open_contracts
            elif tick_price["LastPrice"] <= self.set_price["sbreak"]:
                open_contracts = -1
                return "sell_open", open_contracts
            else:
                if tick_price["HighestPrice"] >= self.set_price["ssetup"] and tick_price["LastPrice"] <= self.set_price["senter"]:
                    open_contracts = -1
                    return "sell_open", open_contracts
                elif tick_price["LowestPrice"] <= self.set_price["bsetup"] and tick_price["LastPrice"] <= self.set_price["benter"]:
                    open_contracts = 1
                    return "buy_open", open_contracts
                else:
                    return "None", open_contracts
        elif open_contracts > 0:
            if tick_price["LastPrice"] >= self.set_price["bbreak"]:
                return "None", open_contracts
            elif tick_price["LastPrice"] <= self.set_price["sbreak"]:
                open_contracts = -1
                return "sell_close", open_contracts
            else:
                if tick_price["HighestPrice"] >= self.set_price["ssetup"] and tick_price["LastPrice"] <= self.set_price["senter"]:
                    open_contracts = -1
                    return "sell_close", open_contracts
                elif tick_price["LowestPrice"] <= self.set_price["bsetup"] and tick_price["LastPrice"] <= self.set_price["benter"]:
                    return "None", open_contracts
                else:
                    return "None", open_contracts
        elif open_contracts < 0:
            if tick_price["LastPrice"] >= self.set_price["bbreak"]:
                open_contracts = 1
                return "buy_close", open_contracts
            elif tick_price["LastPrice"] <= self.set_price["sbreak"]:
                return "None", open_contracts
            else:
                if tick_price["HighestPrice"] >= self.set_price["ssetup"] and tick_price["LastPrice"] <= self.set_price["senter"]:
                    return "None", open_contracts
                elif tick_price["LowestPrice"] <= self.set_price["bsetup"] and tick_price["LastPrice"] <= self.set_price["benter"]:
                    open_contracts = 1
                    return "buy_close", open_contracts
                else:
                    return "None", open_contracts

    def trade(self, md_queue, set_price_use, open_contracts):
        self.set_price = set_price_use
        tick = {}
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n", set_price_use)

        while True:
            time_curr = time.asctime(time.localtime(time.time()))
            time_curr = time_curr[11:19]
            time_tick = int(time_curr[:2] + time_curr[3:5] + time_curr[6:8])

            if 90000 <= time_tick <= 113000 or 133000 <= time_tick <= 145900 or 210000 <= time_tick <= 230000:
                num = self.req_qry_trading_account()
                print(num)
                # if not md_queue.empty():
                #     tick = md_queue.get(block=False)
                #     # print(tick)
                #
                # if self.status >= 0 and isinstance(self.parameter_dict, dict):
                #
                #     # # ############################################################################# #
                #     # # 查询资金
                #     # self.req_qry_trading_account()
                #     # self.write_log(f"=>发出查询资金请求！")
                #     # time.sleep(3)
                #     if tick is {}:
                #         continue
                #
                #     _trade_signal, open_contracts = self.trade_signal(tick, open_contracts)
                #     if _trade_signal == "buy_open":
                #         # 买开
                #         self.buy_open(self.parameter_dict['ExchangeID'],
                #                       self.parameter_dict['InstrumentID'],
                #                       self.parameter_dict['UpperLimitPrice'],
                #                       self.parameter_dict['Volume'])
                #     elif _trade_signal == "sell_close":
                #         # 跌停卖平仓
                #         self.sell_close(self.parameter_dict['ExchangeID'],
                #                         self.parameter_dict['InstrumentID'],
                #                         self.parameter_dict['LowerLimitPrice'],
                #                         self.parameter_dict['Volume'], True)
                #         self.sell_open(self.parameter_dict['ExchangeID'],
                #                        self.parameter_dict['InstrumentID'],
                #                        self.parameter_dict['LowerLimitPrice'],
                #                        self.parameter_dict['Volume'])
                #     elif _trade_signal == "sell_open":
                #         self.sell_open(self.parameter_dict['ExchangeID'],
                #                        self.parameter_dict['InstrumentID'],
                #                        self.parameter_dict['LowerLimitPrice'],
                #                        self.parameter_dict['Volume'])
                #     elif _trade_signal == "buy_close":
                #         self.buy_close(self.parameter_dict['ExchangeID'],
                #                        self.parameter_dict['InstrumentID'],
                #                        self.parameter_dict['LowerLimitPrice'],
                #                        self.parameter_dict['Volume'], True)
                #         self.buy_open(self.parameter_dict['ExchangeID'],
                #                       self.parameter_dict['InstrumentID'],
                #                       self.parameter_dict['UpperLimitPrice'],
                #                       self.parameter_dict['Volume'])
                #     else:
                #         pass
                #
                #     # self.ReqOrderInsert(self.parameter_dict['ExchangeID'],
                #     #                     self.parameter_dict['InstrumentID'],
                #     #                     4460,
                #     #                     self.parameter_dict['Volume'],
                #     #                     '买: 0',
                #     #                     '开仓: 0')
                #
                #     print(tick["LastPrice"], tick["HighestPrice"], tick["LowestPrice"], open_contracts)
            elif 145900 <= time_tick <= 150000:
                if open_contracts > 0:
                    self.sell_close(self.parameter_dict['ExchangeID'],
                                    self.parameter_dict['InstrumentID'],
                                    self.parameter_dict['LowerLimitPrice'],
                                    self.parameter_dict['Volume'], True)
                elif open_contracts < 0:
                    self.buy_close(self.parameter_dict['ExchangeID'],
                                   self.parameter_dict['InstrumentID'],
                                   self.parameter_dict['LowerLimitPrice'],
                                   self.parameter_dict['Volume'], True)
                else:
                    pass

            else:
                time.sleep(10)

            time.sleep(1)


def run_req_authenticate(account, md_queue, set_price_use, open_contracts):
    if isinstance(account, FutureAccount):
        trader_engine = ReqAuthenticate(
            account.broker_id,
            account.server_dict['TDServer'],
            account.investor_id,
            account.password,
            account.app_id,
            account.auth_code,
            md_queue,
            account.td_page_dir
        )
        # trader_engine.Join()
        trader_engine.trade(md_queue, set_price_use, open_contracts)
