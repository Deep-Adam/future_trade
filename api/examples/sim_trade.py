# encoding:utf-8

import time
import pandas as pd

from multiprocessing import Process, Queue
from AlgoPlus.CTP.MdApi import run_tick_engine
from ReqTrade import run_req_authenticate, ReqAuthenticate
from AlgoPlus.CTP.FutureAccount import get_simnow_account
from AlgoPlus.CTP.FutureAccount import FutureAccount
from AlgoPlus.CTP.TraderApi import TraderApi


def set_price(high_price, low_price, close_price):
    # >>>>>>>>>>>> 计算关键价格 <<<<<<<<<<<<<<
    a = 0.35
    b = 1.04
    c = 0.04
    d = 0.3

    # 计算r_breaker相关的指标
    bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
    ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价

    benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
    senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

    sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
    bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

    return bsetup, benter, bbreak, ssetup, senter, sbreak


def print_tick(md_queue):
    while True:
        if not md_queue.empty():
            tick = md_queue.get(block=False)
            print(tick)
            # keys = ['TradingDay', 'UpdateTime', 'LastPrice', 'PreSettlementPrice', 'PreClosePrice',
            #         'PreOpenInterest', 'OpenPrice', 'HighestPrice', 'LowestPrice', 'Volume']
            # useful_price = {i: test[i] for i in keys}


if __name__ == '__main__':
    config = pd.read_csv("config.csv")

    bsetup, benter, bbreak, ssetup, senter, sbreak = set_price(high_price=config.high_price,
                                                               low_price=config.low_price,
                                                               close_price=config.close_price)
    set_price_use = {"bsetup": bsetup,
                     "benter": benter,
                     "bbreak": bbreak,
                     "ssetup": ssetup,
                     "senter": senter,
                     "sbreak": sbreak}

    # 账户配置
    instrument_id_list = [b'rb2110']
    future_account = get_simnow_account(
        investor_id='180692',                   # SimNow账户
        password='Abcdef99',                    # SimNow账户密码
        instrument_id_list=instrument_id_list,  # 合约列表
        server_name='电信1'                      # 电信1、电信2、移动、TEST
    )

    # 交易进程
    # print_process = Process(target=print_tick, args=(share_queue, set_price_use, parameter_dict))

    # 交易参数
    parameter_dict = {
        'ExchangeID': b'SHFE',                  # 交易所
        'InstrumentID': b'rb2110',              # 合约代码
        'UpperLimitPrice': config.up_limit,     # 涨停板
        'LowerLimitPrice': config.low_limit,    # 跌停板
        'Volume': 1,  # 报单手数
    }

    # 共享队列
    share_queue = Queue(maxsize=100)
    share_queue.put(parameter_dict)

    md_process = Process(target=run_tick_engine, args=(future_account, [share_queue]))

    trade_process = Process(target=run_req_authenticate, args=(future_account,
                                                               share_queue,
                                                               set_price_use,
                                                               int(config.open_contracts)))

    md_process.start()
    trade_process.start()

    md_process.join()
    trade_process.join()
