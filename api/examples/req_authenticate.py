# encoding:utf-8
# 看穿式认证

from multiprocessing import Queue
# from AlgoPlus.CTP.ReqAuthenticate import run_req_authenticate
from ReqTrade import run_req_authenticate, ReqAuthenticate
from AlgoPlus.CTP.FutureAccount import get_simnow_account
from AlgoPlus.CTP.MdApi import run_tick_engine


def set_price(high_price, low_price, close_price):
    # >>>>>>>>>>>> 计算关键价格 <<<<<<<<<<<<<<

    a = 0.35
    b = 1.04
    c = 0.04
    d = 0.3

    # 计算r_breaker相关的指标
    bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
    ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价

    benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
    senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

    sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
    bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

    return bsetup, benter, bbreak, ssetup, senter, sbreak


def trade(str_signal, parameter_dict):
    """
    交易函数，发出开单或平仓请求
    :param str_signal:
    :param parameter_dict:
    :return:
    """
    if str_signal == "buy_open":
        # 开多单
        ReqAuthenticate.buy_open(parameter_dict['ExchangeID'],
                                 parameter_dict['InstrumentID'],
                                 parameter_dict['UpperLimitPrice'],
                                 parameter_dict['Volume'])
        # self.write_log(f"=>{ikk}=>发出涨停买开仓请求！")
    elif str_signal == "sell_close":
        # 平多单
        ReqAuthenticate.sell_close(parameter_dict['ExchangeID'],
                                   parameter_dict['InstrumentID'],
                                   parameter_dict['LowerLimitPrice'],
                                   parameter_dict['Volume'],
                                   True)
        # self.write_log(f"=>{ikk}=>发出跌停卖平仓请求！")
    elif str_signal == "sell_open":
        # 开空单
        ReqAuthenticate.sell_open(parameter_dict['ExchangeID'],
                                  parameter_dict['InstrumentID'],
                                  parameter_dict['LowerLimitPrice'],
                                  parameter_dict['Volume'])
        # self.write_log(f"=>{ikk}=>发出跌停卖开仓请求！")
    elif str_signal == "buy_close":
        # 平空单
        ReqAuthenticate.buy_close(parameter_dict['ExchangeID'],
                                  parameter_dict['InstrumentID'],
                                  parameter_dict['UpperLimitPrice'],
                                  parameter_dict['Volume'],
                                  True)
        # self.write_log(f"=>{ikk}=>发出涨停买平仓请求！")


def main():
    # 账户配置
    account = get_simnow_account(
        investor_id='180692',
        password='Abcdef99',
        server_name='电信1'
    )

    # 交易参数
    parameter_dict = {
        'ExchangeID': b'SHFE',  # 交易所
        'InstrumentID': b'rb2105',  # 合约代码
        'UpperLimitPrice': 4500,  # 涨停板
        'LowerLimitPrice': 4100,  # 跌停板
        'Volume': 1,  # 报单手数
    }

    # 共享队列
    share_queue = Queue(maxsize=100)
    share_queue.put(parameter_dict)

    run_req_authenticate(account, share_queue)


if __name__ == '__main__':
    main()
