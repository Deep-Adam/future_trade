import numpy as np
import pandas as pd
import time

from tqdm import tqdm


class DATA:
    def __init__(self, file_path):
        self.file_path = file_path

    def tick2min(self):
        data = pd.read_csv(self.file_path)
        # print(data)
        # print(data['UpdateTime'])
        # print(data.columns)
        # ['TradingDay', 'InstrumentID', 'ExchangeID', 'ExchangeInstID',
        #        'LastPrice', 'PreSettlementPrice', 'PreClosePrice', 'PreOpenInterest',
        #        'OpenPrice', 'HighestPrice', 'LowestPrice', 'Volume', 'Turnover',
        #        'OpenInterest', 'ClosePrice', 'SettlementPrice', 'UpperLimitPrice',
        #        'LowerLimitPrice', 'PreDelta', 'CurrDelta', 'UpdateTime',
        #        'UpdateMillisec', 'BidPrice1', 'BidVolume1', 'AskPrice1', 'AskVolume1',
        #        'BidPrice2', 'BidVolume2', 'AskPrice2', 'AskVolume2', 'BidPrice3',
        #        'BidVolume3', 'AskPrice3', 'AskVolume3', 'BidPrice4', 'BidVolume4',
        #        'AskPrice4', 'AskVolume4', 'BidPrice5', 'BidVolume5', 'AskPrice5',
        #        'AskVolume5', 'AveragePrice', 'ActionDay']

        sub_min = {"time": data["UpdateTime"][0],
                   "open": data["LastPrice"][0],
                   "close": data["LastPrice"][0],
                   "high": data["LastPrice"][0],
                   "low": data["LastPrice"][0],
                   "volume": int(data["Volume"][0])}
        print(sub_min["volume"])
        print(type(sub_min["volume"]))
        print(int(sub_min["volume"]))
        min_k_data = []
        for i in range(len(data)):
            if data["Volume"][i] == "Volume":
                continue
            else:
                # time structure "13:36:26", type str
                if data["UpdateTime"][i][:5] == sub_min["time"][:5]:
                    sub_min.update({"high": max(sub_min["high"], data["LastPrice"][i]),
                                    "low": min(sub_min["low"], data["LastPrice"][i])})
                else:
                    # print(data["Volume"][i], sub_min["volume"])
                    # print(int(data["Volume"][i]), sub_min["volume"])
                    sub_min.update({"high": max(sub_min["high"], data["LastPrice"][i]),
                                    "low": min(sub_min["low"], data["LastPrice"][i]),
                                    "close": data["LastPrice"][i],
                                    "volume": int(data["Volume"][i]) - sub_min["volume"]})
                    min_k_data.append(sub_min)
                    sub_min = {"time": data["UpdateTime"][i],
                               "open": data["LastPrice"][i],
                               "close": data["LastPrice"][i],
                               "high": data["LastPrice"][i],
                               "low": data["LastPrice"][i],
                               "volume": int(data["Volume"][i])}

        print(min_k_data)
        print(len(min_k_data))
        # a = [{'time': '13:36:26', 'open': '5940.0', 'close': '5931.0', 'high': '5943.0', 'low': '5928.0', 'volumn': '2808417'}, {'time': '13:37:00', 'open': '5931.0', 'close': '5922.0', 'high': '5934.0', 'low': '5922.0', 'volumn': '2815938'}, {'time': '13:38:00', 'open': '5922.0', 'close': 'LastPrice', 'high': 'LastPrice', 'low': '5920.0', 'volumn': '2829647'}, {'time': 'UpdateTime', 'open': 'LastPrice', 'close': '5428.0', 'high': 'LastPrice', 'low': '5428.0', 'volumn': 'Volume'}, {'time': '14:13:40', 'open': '5428.0', 'close': '5427.0', 'high': '5431.0', 'low': '5426.0', 'volumn': '1084259'}, {'time': '14:14:00', 'open': '5427.0', 'close': '5431.0', 'high': '5431.0', 'low': '5426.0', 'volumn': '1086938'}, {'time': '14:15:00', 'open': '5431.0', 'close': '5431.0', 'high': '5434.000000000001', 'low': '5430.0', 'volumn': '1088448'}, {'time': '14:16:00', 'open': '5431.0', 'close': '5429.0', 'high': '5433.000000000001', 'low': '5428.0', 'volumn': '1090569'}, {'time': '14:17:00', 'open': '5429.0', 'close': '5431.0', 'high': '5432.0', 'low': '5428.0', 'volumn': '1092290'}, {'time': '14:18:00', 'open': '5431.0', 'close': '5433.0', 'high': '5434.0', 'low': '5430.0', 'volumn': '1093046'}, {'time': '14:19:00', 'open': '5433.0', 'close': '5435.0', 'high': '5437.000000000001', 'low': '5432.0', 'volumn': '1094364'}]


def main():
    data_load = DATA(file_path='api/examples/MarketData/180692/rb2110-20210806.csv')
    data_load.tick2min()


if __name__ == "__main__":
    main()
