# -*-coding:utf-8-*-

# 测试产品：螺纹钢商品期货主连
# 使用R_Breaker算法，量化交易回测，单线程回测
# 相比较之前的版本，交易信号触发改为1 min K线收盘价
# author: zhangwei

# import talib as ta
import numpy as np
import pandas as pd
from tqdm import tqdm
from maximum_drawdown import MaximumDrawdown
# from multiprocessing import RLock
# import re
# import os
# import math


class TRADING:
    def __init__(self, slippage, handling_fee):
        self.slippage = slippage            # 滑点与交易手续费
        self.handling_fee = handling_fee    # 交易手续费

    @staticmethod
    def time_trans(x):
        return x[:-9]

    @staticmethod
    def parameter_list(a_lis, b_lis, c_lis, d_lis, t_lis):
        para_list = []
        for a in a_lis:
            for b in b_lis:
                for c in c_lis:
                    for d in d_lis:
                        for t in t_lis:
                            para_list.append([a, b, c, d, t])

        return para_list

    @staticmethod
    def trading_log_dict(longorshort, open_price, open_time, close_price, close_time,
                         holding_time, profit_percentage, max_drawdown, max_profit):
        __trading_log = {'longorshort': longorshort,
                         'open_price': open_price,
                         'open_time': open_time,
                         'close_price': close_price,
                         'close_time': close_time,
                         'holding_time': holding_time,
                         'profit_percentage': profit_percentage,
                         'max_drawdown': max_drawdown,
                         'max_profit': max_profit}
        return __trading_log

    @staticmethod
    def set_price_cal(high_price, low_price, close_price, a, b, c, d):
        # 计算r_breaker相关的指标
        bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
        ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价
        # print([low_price, a, high_price, close_price, bsetup])

        benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
        senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

        sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
        bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

        return bsetup, benter, bbreak, ssetup, senter, sbreak

    def main_trading(self, data_id, day_data, data, _len_data, a, b, c, d, t, order_save):
        """
        回测算法，根据参数回测算法表现
        :param data_id:
        :param day_data:
        :param data:
        :param _len_data:
        :param order_save: 参数表达是否为单个参数组合的回测，确认是否保留每一单交易
        :return:
        """
        # 更新a,b,c,d参数后，重新初始化交易信息
        account = 10000
        single_posi = int(account / 8000)
        account_log = []
        position_log = []
        order_log = []
        trading = 0
        sub_order = dict()

        slippage = 10
        stop_loss_point = 10    # stop loss percentage
        target_pro_point = 10   # target profit point

        print(data)
        print(data_id)
        # print(data[data.date == data_id[0]])

        last_t_high = np.array(data[data.date == data_id[0]].iloc[-t:, 4])
        last_t_low = np.array(data[data.date == data_id[0]].iloc[-t:, 5])

        # 策略交易的主体
        for i in range(1, _len_data):
            # sub_min截取每天的1 min数据
            sub_min = data[data.date == data_id[i]]
            day_high = sub_min.iloc[0, 4]       # high price of a trading day
            day_low = sub_min.iloc[0, 5]        # low price of a trading day
            single_posi = max(single_posi, int(account / 8000))
            trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # 交易成本
            position = 0
            close = day_data.iloc[i - 1, 4]

            for k in range(0, len(sub_min)-1):
                day_high = max(day_high, sub_min.iloc[k, 4])
                day_low = min(day_low, sub_min.iloc[k, 5])
                high = max(last_t_high)
                low = min(last_t_low)

                # 计算r_breaker相关的指标
                bsetup, benter, bbreak, ssetup, senter, sbreak = self.set_price_cal(high, low, close, a, b, c, d)

                # print("time", sub_min.iloc[k, 1],
                #       "hl", sub_min.iloc[k, 4], sub_min.iloc[k, 5],
                #       "day", day_high, day_low,
                #       "set", high, low, close,
                #       "b", bsetup, benter, bbreak,
                #       "s", ssetup, senter, sbreak)

                # 判断是否为趋势行情
                if sub_min.iloc[k, 4] >= bbreak:
                    # 开仓做多，判断是否跳空
                    if position == 0:
                        if sub_min.iloc[k, 2] - bbreak > slippage:
                            pass
                        else:
                            position = single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'bbreak', 'time': sub_min.iloc[k, 1],
                                              'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                              'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})
                    elif position > 0:
                        sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                          'high': max(sub_min.iloc[k, 4], sub_order['high']),
                                          'low': min(sub_min.iloc[k, 5], sub_order['low'])})

                    else:
                        # 上一单结算，判断是否跳空
                        sub_order.update({'close': sub_min.iloc[k, 3], 'close_time': sub_min.iloc[k, 1]})

                        order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                        sub_order.update({"profit": order_profit})
                        account += order_profit
                        order_log.append(sub_order)
                        sub_order = {'holding_time': 0}

                        # 记录新一单交易，判断是否跳空
                        if sub_min.iloc[k, 2] - bbreak > slippage:
                            position = 0
                            pass
                        else:
                            position = single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'bbreak', 'time': sub_min.iloc[k, 1],
                                              'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                              'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})

                elif sub_min.iloc[k, 5] <= sbreak:
                    if position == 0:
                        # 记录新一单交易，判断是否跳空
                        if sbreak - sub_min.iloc[k, 2] > slippage:
                            pass
                        else:
                            position = -single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'sbreak', 'time': sub_min.iloc[k, 1],
                                              'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                              'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})

                    elif position > 0:
                        # 上一单结算，判断是否跳空
                        sub_order.update({'close': sub_min.iloc[k, 3], 'close_time': sub_min.iloc[k, 1]})

                        order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                        sub_order.update({"profit": order_profit})
                        account += order_profit
                        order_log.append(sub_order)
                        sub_order = {'holding_time': 0}

                        # 记录新一单交易，判断是否跳空
                        if sbreak - sub_min.iloc[k, 2] > slippage:
                            position = 0
                            pass
                        else:
                            position = -single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'sbreak', 'time': sub_min.iloc[k, 1],
                                              'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                              'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})
                    else:
                        sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                          'high': max(sub_min.iloc[k, 4], sub_order['high']),
                                          'low': min(sub_min.iloc[k, 5], sub_order['low'])})

                else:
                    # 判断是否为反转行情
                    if day_high > ssetup and sub_min.iloc[k, 5] <= senter:
                        if position == 0:
                            # 记录新一单交易
                            if senter - sub_min.iloc[k, 2] > slippage:
                                pass
                            else:
                                position = -single_posi
                                trading += 1
                                sub_order.update({'longorshort': 'senter', 'time': sub_min.iloc[k, 1],
                                                  'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                                  'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})
                        elif position > 0:
                            # 上一单结算
                            sub_order.update({'close': sub_min.iloc[k, 3], 'close_time': sub_min.iloc[k, 1],
                                              'holding_time': sub_order['holding_time'] + 1})

                            order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                            sub_order.update({"profit": order_profit})
                            account += order_profit
                            order_log.append(sub_order)
                            sub_order = {'holding_time': 0}

                            # 记录新一单交易
                            if senter - sub_min.iloc[k, 2] > slippage:
                                position = 0
                                pass
                            else:
                                position = -single_posi
                                trading += 1
                                sub_order.update({'longorshort': 'senter', 'time': sub_min.iloc[k, 1],
                                                  'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                                  'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})
                        else:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(sub_min.iloc[k, 4], sub_order['high']),
                                              'low': min(sub_min.iloc[k, 5], sub_order['low'])})

                    elif day_low < bsetup and sub_min.iloc[k, 4] > benter:
                        if position == 0:
                            # 记录新一单交易
                            if sub_min.iloc[k, 2] - benter > slippage:
                                pass
                            else:
                                position = single_posi
                                trading += 1
                                sub_order.update({'longorshort': 'benter', 'time': sub_min.iloc[k, 1],
                                                  'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                                  'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})
                        elif position > 0:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(sub_min.iloc[k, 4], sub_order['high']),
                                              'low': min(sub_min.iloc[k, 5], sub_order['low'])})

                        else:
                            # 上一单结算
                            sub_order.update({'close': sub_min.iloc[k, 3], 'close_time': sub_min.iloc[k, 1],
                                              'holding_time': sub_order['holding_time'] + 1})

                            order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                            sub_order.update({"profit": order_profit})
                            account += order_profit
                            order_log.append(sub_order)
                            sub_order = {'holding_time': 0}

                            # 记录新一单交易
                            if sub_min.iloc[k, 2] - benter > slippage:
                                position = 0
                                pass
                            else:
                                position = single_posi
                                trading += 1
                                sub_order.update({'longorshort': 'benter', 'time': sub_min.iloc[k, 1],
                                                  'quantity': single_posi, 'open': sub_min.iloc[k, 3], 'holding_time': 1,
                                                  'high': sub_min.iloc[k, 3], 'low': sub_min.iloc[k, 3]})

                    else:
                        # no trading signal
                        if position > 0:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(sub_min.iloc[k, 4], sub_order['high']),
                                              'low': min(sub_min.iloc[k, 5], sub_order['low'])})
                        elif position < 0:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(sub_min.iloc[k, 4], sub_order['high']),
                                              'low': min(sub_min.iloc[k, 5], sub_order['low'])})
                        else:
                            pass

                position_log.append(position)

                last_t_high = np.append(last_t_high[1:], sub_min.iloc[k, 4])
                last_t_low = np.append(last_t_low[1:], sub_min.iloc[k, 5])

                # ===================================
                # =            stoploss             =
                # ===================================
                if position > 0 and sub_order['holding_time'] > 0:
                    if sub_order['open'] - sub_order['low'] >= stop_loss_point:
                        sub_order.update({'close': sub_order['open'] - stop_loss_point,
                                          'close_time': sub_min.iloc[k, 1]})
                        order_profit = single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                        sub_order.update({"profit": order_profit})
                        account += order_profit
                        order_log.append(sub_order)
                        sub_order = {'holding_time': 0}
                        position = 0
                        continue
                    # target stop
                    if sub_order['high'] - sub_order['open'] > target_pro_point:
                        sub_order.update({'close': sub_order['open'] + target_pro_point,
                                          'close_time': sub_min.iloc[k, 1]})
                        order_profit = single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                        sub_order.update({"profit": order_profit})
                        account += order_profit
                        order_log.append(sub_order)
                        sub_order = {'holding_time': 0}
                        position = 0
                        continue

                elif position < 0 and sub_order['holding_time'] > 0:
                    if sub_order['high'] - sub_order['open'] >= stop_loss_point:
                        sub_order.update({'close': sub_order['open'] + stop_loss_point,
                                          'close_time': sub_min.iloc[k, 1]})
                        order_profit = -single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                        sub_order.update({"profit": order_profit})
                        account += order_profit
                        order_log.append(sub_order)
                        sub_order = {'holding_time': 0}
                        position = 0
                        continue
                    # target stop
                    if sub_order['open'] - sub_order['low'] > target_pro_point:
                        sub_order.update({'close': sub_order['open'] - target_pro_point,
                                          'close_time': sub_min.iloc[k, 1]})
                        order_profit = -single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                        sub_order.update({"profit": order_profit})
                        account += order_profit
                        order_log.append(sub_order)
                        sub_order = {'holding_time': 0}
                        position = 0
                        continue
                else:
                    pass

            if position is not 0:
                sub_order.update({'close': sub_min.iloc[k, 3], 'holding_time': sub_order['holding_time'] + 1,
                                  'close_time': sub_min.iloc[k, 1]})
                if position > 0:
                    order_profit = single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit
                else:
                    order_profit = -single_posi * 10 * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    account += order_profit

                account_log.append([account])
                order_log.append(sub_order)
                sub_order = {'holding_time': 0}
            else:
                account_log.append([account])

        winrate = 0
        for i in order_log:
            if i["profit"] > 0:
                winrate += 1
        winrate = round(100 * winrate / (len(order_log)+0.00001), 2)

        if order_save == 1:
            drawdown_cal = MaximumDrawdown()
            rate = round(((account_log[-1][0] - account_log[0][0]) / (account_log[0][0]+0.0001)) * 100, 2)
            max_drawdown = drawdown_cal.calculate(account_log)
            print('a:', a,
                  '\nb:', b,
                  '\nc:', c,
                  '\nd:', d,
                  '\nt:', t,
                  '\nrate:', rate,
                  '\ntrading:', trading,
                  '\ndraw:', round(max_drawdown * 100, 2))
            order_log = pd.DataFrame.from_dict(order_log, orient='columns')
            order_log.to_csv("Result/r_breaker/order_log.csv")

            print('winrate:', winrate)

        return account_log, position_log, trading, winrate

    def main(self, pro_id, para_list, stride, order_trigger):
        year_backbone = 2021
        # 从2019年开始回测
        if year_backbone == 2019:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)     # 读入处理好的日K数据
            day_data = day_data.iloc[2433:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)             # 原始的1 min数据
            data = data.iloc[686175:, :]
            data.reset_index(drop=True, inplace=True)
        # 从20116年开始回测
        elif year_backbone == 2016:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)
            day_data = day_data.iloc[1689:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
        # 从20118年开始回测
        elif year_backbone == 2018:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)
            day_data = day_data.iloc[2190:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
        # 从2020年开始回测
        elif year_backbone == 2020:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)
            day_data = day_data.iloc[2676:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)
            data = data.iloc[769290:, :]
            data.reset_index(drop=True, inplace=True)
        # 从2021年开始回测
        elif year_backbone == 2021:
            day_data = pd.read_csv("Data/r_breaker/day_data_20201107-20210226.csv", index_col=0)
            day_data.reset_index(drop=True, inplace=True)
            day_data = day_data.iloc[58:, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker_20201107-20210226.csv", index_col=0)
        # 测试
        elif year_backbone == 2022:
            day_data = pd.read_csv("Data/r_breaker/day_data_20201107-20210226.csv", index_col=0)
            day_data = day_data.iloc[39:41, :]
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker_20201107-20210226.csv", index_col=0)
        else:
            day_data = pd.read_csv("Data/r_breaker/day_data_2019.csv", index_col=0)  # 读入处理好的日K数据
            day_data.reset_index(drop=True, inplace=True)
            data = pd.read_csv("Data/r_breaker/r_breaker.csv", index_col=0)  # 原始的1 min数据

        data_id = day_data.time  # 从日K数据中，提取日期信息

        # lock.acquire()      # 进程锁

        drawdown_cal = MaximumDrawdown()
        # 记录每次交易的min级账户情况
        save_log = pd.DataFrame()
        # 记录所有交易的利润率等
        save_all = pd.DataFrame(columns=["No", "a", "b", "c", "d", "t", "rate", 'drawdown', "trading", "winrate"])
        # 记录最后一次交易的min级交易详情，持仓等
        n = pro_id * stride
        stop_loss_per = 0.90
        len_data = len(day_data)

        with tqdm(total=stride, ncols=80) as pbar:
            for [a, b, c, d, period] in para_list:
                account_log, position_log, trading, winrate = self.main_trading(data_id, day_data, data, len_data,
                                                                                a, b, c, d, period, order_trigger)

                max_drawdown = round(drawdown_cal.calculate(account_log), 4)
                rate = round((account_log[-1][0] - 10000) / 100, 2)

                account_log = pd.DataFrame(account_log,
                                           columns=[str(a) + '_' + str(b) + '_' + str(c) + '_' + str(d)])

                save_log = pd.concat((save_log, account_log), axis=1)

                # 记录每次回测的交易概述
                save_all = save_all.append([{"No": n,
                                             'a': a,
                                             'b': b,
                                             "c": c,
                                             "d": d,
                                             "t": period,
                                             "rate": rate,
                                             "drawdown": max_drawdown * 100,
                                             "trading": trading,
                                             "winrate": winrate}])

                # print("No: ", n,
                #       " a: ", a,
                #       " b: ", b,
                #       " c: ", c,
                #       " d: ", d,
                #       " Rate: ", rate,
                #       " Drawdown: ", max_drawdown * 100)
                # print(save_log.index, day_data.time[1:])

                if n % 10 == 0:
                    # 保存迭代的结果
                    save_log.index = day_data.time[1: (len(save_log) + 1)]
                    save_all.reset_index(drop=True, inplace=True)
                    save_all.to_csv("Result/r_breaker/save_all_" + str(pro_id) + ".csv", index_label=None)
                    save_log.to_csv("Result/r_breaker/account_log_" + str(pro_id) + ".csv", index_label=None)

                n += 1
                pbar.update(1)

        save_log.index = day_data.time[1: (len(save_log) + 1)]
        save_all.reset_index(drop=True, inplace=True)
        save_all.to_csv("Result/r_breaker/save_all_" + str(pro_id) + ".csv", index_label=None)
        save_log.to_csv("Result/r_breaker/account_log_" + str(pro_id) + ".csv", index_label=None)

        # lock.release()

        # position_log = pd.DataFrame(position_log, columns=["position"])
        # account_log.columns = ["account"]
        # account_log = pd.concat((account_log, position_log), axis=1)
        # account_log.to_csv('trading.csv')



