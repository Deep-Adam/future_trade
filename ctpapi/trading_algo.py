class TRADEALGO():
    def __init__(self, tick_price, set_price, open_contracts):
        self.tick_price = tick_price
        self.set_price = set_price
        self.open_contracts = open_contracts

    def trade_signal(self):
        if self.open_contracts == 0:
            if self.tick_price["LastPrice"] >= self.set_price["bbreak"]:
                open_contracts = 1
                return "buy_open"
            elif self.tick_price["LastPrice"] <= self.set_price["sbreak"]:
                open_contracts = -1
                return "sell_open"
            else:
                if self.tick_price["HighestPrice"] >= self.set_price["ssetup"] and self.tick_price["LastPrice"] <= self.set_price["senter"]:
                    open_contracts = -1
                    return "sell_open"
                elif self.tick_price["LowestPrice"] <= self.set_price["bsetup"] and self.tick_price["LastPrice"] <= self.set_price["benter"]:
                    open_contracts = 1
                    return "buy_open"
                else:
                    return "None"
        elif self.open_contracts > 0:
            if self.tick_price["LastPrice"] >= self.set_price["bbreak"]:
                return "None"
            elif self.tick_price["LastPrice"] <= self.set_price["sbreak"]:
                open_contracts = -1
                return "sell_close"
            else:
                if self.tick_price["HighestPrice"] >= self.set_price["ssetup"] and self.tick_price["LastPrice"] <= self.set_price["senter"]:
                    open_contracts = -1
                    return "sell_close"
                elif self.tick_price["LowestPrice"] <= self.set_price["bsetup"] and self.tick_price["LastPrice"] <= self.set_price["benter"]:
                    return "None"
                else:
                    return "None"
        elif self.open_contracts < 0:
            if self.tick_price["LastPrice"] >= self.set_price["bbreak"]:
                open_contracts = 1
                return "buy_close"
            elif self.tick_price["LastPrice"] <= self.set_price["sbreak"]:
                return "None"
            else:
                if self.tick_price["HighestPrice"] >= self.set_price["ssetup"] and self.tick_price["LastPrice"] <= self.set_price["senter"]:
                    return "None"
                elif self.tick_price["LowestPrice"] <= self.set_price["bsetup"] and self.tick_price["LastPrice"] <= self.set_price["benter"]:
                    open_contracts = 1
                    return "buy_close"
                else:
                    return "None"


