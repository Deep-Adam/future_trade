import csv
import time

time_start = time.time()
fields = ['2018/10/2', 6.6, 6.6, 5.87, 5.93, 5.93, 19440000]

with open(r'test.csv', 'a') as f:
    writer = csv.writer(f)
    writer.writerow(fields)

f.close()

print("time used", time.time() - time_start)
