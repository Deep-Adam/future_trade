# -*- coding: utf-8 -*-
# 交易库
# thosttraderapi.py     交易头文件
# _thosttraderapi.pyd   交易库转换文件
# thosttraderapi.dll    交易官方动态库，穿透式版为thosttraderapi_se.dll

# 行情查询库
# thostmduserapi.py     行情头文件
# _thostmduserapi.pyd   行情库转换文件
# thostmduserapi.dll    行情官方动态库，穿透式版为thostmduserapi_se.dll

# Req: 请求
# OnRsp: 响应
# ReqQry: 查询
# OnRspQry: 查询响应的请求
# OnRtn: 回报
# OnErrRtn: 错误回报

import thosttraderapi as api
import pandas as pd
import time

# Addr
FrontAddr = "tcp://180.168.146.187:10101"
# LoginInfo
BROKERID = "9999"
USERID = "180692"
PASSWORD = "Abcdef99"
# OrderInfo
EXCHANGEID = "SHFE"
INSTRUMENTID = "rb2105"
subID = ['rb2105']
# PRICE = 4490
PRICE = 4320
VOLUME = 1
# DIRECTION = api.THOST_FTDC_D_Sell		# 挂限价卖单
# DIRECTION = api.THOST_FTDC_D_Buy  # 挂限价买单
DIRECTION = api.THOST_FTDC_DEN_Sell  # 挂限价买单
# open
# OFFSET = "0"

# close
OFFSET = "1"


# THOST_FTDC_PSRC_Buy = _thosttraderapi.THOST_FTDC_PSRC_Buy
# THOST_FTDC_DEN_Buy = _thosttraderapi.THOST_FTDC_DEN_Buy
# THOST_FTDC_D_Buy = _thosttraderapi.THOST_FTDC_D_Buy

def ReqorderfieldInsert(tradeapi):
    print("ReqOrderInsert Start")
    orderfield = api.CThostFtdcInputOrderField()    # CThostFtdcInputOrderField 函数初始化
    orderfield.BrokerID = BROKERID
    orderfield.ExchangeID = EXCHANGEID
    orderfield.InstrumentID = INSTRUMENTID          # 交易品种
    orderfield.UserID = USERID
    orderfield.InvestorID = USERID
    orderfield.Direction = DIRECTION                # 开单类型
    orderfield.LimitPrice = PRICE                   # 开单价格
    orderfield.VolumeTotalOriginal = VOLUME         # 手数
    orderfield.OrderPriceType = api.THOST_FTDC_OPT_LimitPrice
    orderfield.ContingentCondition = api.THOST_FTDC_CC_Immediately
    orderfield.TimeCondition = api.THOST_FTDC_TC_GFD
    orderfield.VolumeCondition = api.THOST_FTDC_VC_AV
    orderfield.CombHedgeFlag = "1"                  # Hedge
    orderfield.CombOffsetFlag = OFFSET
    orderfield.GTDDate = ""
    orderfield.orderfieldRef = "1"
    orderfield.MinVolume = 0
    orderfield.ForceCloseReason = api.THOST_FTDC_FCC_NotForceClose
    orderfield.IsAutoSuspend = 0
    tradeapi.ReqOrderInsert(orderfield, 0)
    print("ReqOrderInsert End")


class CTradeSpi(api.CThostFtdcTraderSpi):
    tapi = ''

    def __init__(self, tapi):
        api.CThostFtdcTraderSpi.__init__(self)
        self.tapi = tapi

    def OnFrontConnected(self) -> "void":
        """
        首先运行
        account info, with userid and appid
        :return: nothing
        """
        print("OnFrontConnected")
        authfield = api.CThostFtdcReqAuthenticateField()
        authfield.BrokerID = BROKERID
        authfield.UserID = USERID
        authfield.AppID = "simnow_client_test"
        authfield.AuthCode = "0000000000000000"
        self.tapi.ReqAuthenticate(authfield, 0)     # 确认账号连接正确
        print("send ReqAuthenticate ok")

    def OnRspAuthenticate(self, pRspAuthenticateField: 'CThostFtdcRspAuthenticateField',
                          pRspInfo: 'CThostFtdcRspInfoField',
                          nRequestID: 'int',
                          bIsLast: 'bool') -> "void":
        """
        在 OnFrontConnected 之后运行，输出账户信息
        :param pRspAuthenticateField:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("BrokerID=", pRspAuthenticateField.BrokerID)
        print("UserID=", pRspAuthenticateField.UserID)
        print("AppID=", pRspAuthenticateField.AppID)
        print("AppType=", pRspAuthenticateField.AppType)
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)
        if not pRspInfo.ErrorID:
            loginfield = api.CThostFtdcReqUserLoginField()
            loginfield.BrokerID = BROKERID
            loginfield.UserID = USERID
            loginfield.Password = PASSWORD
            loginfield.UserProductInfo = "python dll"
            self.tapi.ReqUserLogin(loginfield, 0)   # 请求登陆
            print("send login ok")

    def OnRspUserLogin(self, pRspUserLogin: 'CThostFtdcRspUserLoginField',
                       pRspInfo: 'CThostFtdcRspInfoField',
                       nRequestID: 'int',
                       bIsLast: 'bool') -> "void":
        """
        第二个运行
        :param pRspUserLogin:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("OnRspUserLogin")
        print("TradingDay=", pRspUserLogin.TradingDay)
        print("SessionID=", pRspUserLogin.SessionID)
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)
        # self.tapi.SubscribeMarketData(['rb2105'], 1)

        qryinfofield = api.CThostFtdcQrySettlementInfoField()
        qryinfofield.BrokerID = BROKERID
        qryinfofield.InvestorID = USERID
        qryinfofield.TradingDay = pRspUserLogin.TradingDay
        self.tapi.ReqQrySettlementInfo(qryinfofield, 0)
        print("send ReqQrySettlementInfo ok")

    def OnRspQrySettlementInfo(self, pSettlementInfo: 'CThostFtdcSettlementInfoField',
                               pRspInfo: 'CThostFtdcRspInfoField',
                               nRequestID: 'int',
                               bIsLast: 'bool') -> "void":
        """
        第三个运行
        请求昨天的结算单，然后在OnRspQrySettlementInfo中记下SettlementID。
        用Api::ReqSettlementInfoConfirm确认这个结算单就好了。
        :param pSettlementInfo:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("OnRspQrySettlementInfo")
        if pSettlementInfo is not None:
            print("content:", pSettlementInfo.Content)
        else:
            print("content null")
        pSettlementInfoConfirm = api.CThostFtdcSettlementInfoConfirmField()
        pSettlementInfoConfirm.BrokerID = BROKERID
        pSettlementInfoConfirm.InvestorID = USERID
        self.tapi.ReqSettlementInfoConfirm(pSettlementInfoConfirm, 0)
        print("send ReqSettlementInfoConfirm ok")

    def OnRspSettlementInfoConfirm(self,
                                   pSettlementInfoConfirm: 'CThostFtdcSettlementInfoConfirmField',
                                   pRspInfo: 'CThostFtdcRspInfoField',
                                   nRequestID: 'int',
                                   bIsLast: 'bool') -> "void":
        """
        第四个运行
        确认前日的结算单后，正式开始交易
        :param pSettlementInfoConfirm:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("OnRspSettlementInfoConfirm")
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)

        # 声明需要请求查询投资者持仓响应，等待 OnRspQryInvestorPosition 函数返回响应
        pInvestorPosition = api.CThostFtdcQryInvestorPositionField()
        self.tapi.ReqQryInvestorPosition(pInvestorPosition, 1)

    def OnRspQryInvestorPosition(self,
                                 pInvestorPosition: 'CThostFtdcInvestorPositionField',
                                 pRspInfo: 'CThostFtdcRspInfoField',
                                 nRequestID: 'int',
                                 bIsLast: 'bool') -> "void":
        """
        查询投资者持仓的响应，返回当前持仓情况
        :param pInvestorPosition:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print(">>>>>>>> <<<<<<<<<<<<<<Check positon>>>>>>>>>>>>>> <<<<<<<<<<:")
        print("OnRspQryInvestorPosition")
        print("Position=", pInvestorPosition.Position)
        print("Profit=", pInvestorPosition.PositionProfit)
        print("\n")

        # 请求查询账户资金，等待 OnRspQryTradingAccount 函数返回响应
        pTradingAccount = api.CThostFtdcQryTradingAccountField()
        self.tapi.ReqQryTradingAccount(pTradingAccount, 2)

    def OnRspQryTradingAccount(self,
                               pTradingAccount: 'CThostFtdcTradingAccountField',
                               pRspInfo: 'CThostFtdcRspInfoField',
                               nRequestID: 'int',
                               bIsLast: 'bool') -> "void":
        """
        查询账户资金的回调函数，返回账户资金情况
        :param pTradingAccount:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("============")
        print(pTradingAccount.Balance)

        # 发出交易请求
        ReqorderfieldInsert(self.tapi)
        print("send ReqorderfieldInsert ok")

    # 调用方法为继承该函数所在类，重写该函数调用。
    # def OnRtnTrade(self, pTrade: 'CThostFtdcTradeField') -> "void":
    #     print("======================")
    #     print(pTrade.TradeID)
    #     print(pTrade.Direction)
    #     print(pTrade.Volume)

    def OnRtnOrder(self, pOrder: 'CThostFtdcOrderField') -> "void":
        """
        输出报单成交信息
        :param pOrder:
        :return:
        """
        print("OnRtnDepthMarketData")
        print("OnRtnOrder")
        print("OrderStatus=", pOrder.OrderStatus)
        print("StatusMsg=", pOrder.StatusMsg)
        print("LimitPrice=", pOrder.LimitPrice)

    def OnRspOrderInsert(self, pInputOrder: 'CThostFtdcInputOrderField',
                         pRspInfo: 'CThostFtdcRspInfoField',
                         nRequestID: 'int',
                         bIsLast: 'bool') -> "void":
        """
        返回报单信息
        :param pInputOrder:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("OnRspOrderInsert")
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)


def main():
    # 创建API实例，初始化API
    tradeapi = api.CThostFtdcTraderApi_CreateFtdcTraderApi()

    # 创建SPI实例。CTradeSpi是继承自头文件中CThostFtdcTraderSpi的类型，用于收从CTP的回复，可以重写父类中的函数来实现自己的逻辑
    tradespi = CTradeSpi(tradeapi)

    # 注册前置地址，是指将CTP前置的IP地址注册进API实例内
    tradeapi.RegisterFront(FrontAddr)

    # 将创建的SPI实例注册进实例，这样该API实例发出的请求对应的回报，就会回调到对应的SPI实例的函数
    tradeapi.RegisterSpi(tradespi)

    # 订阅共有流与私有流。订阅方式主要有三种，分为断点续传，重传和连接建立开始传三种。
    tradeapi.SubscribePrivateTopic(api.THOST_TERT_QUICK)
    tradeapi.SubscribePublicTopic(api.THOST_TERT_QUICK)

    # API启动，init之后就会启动一个内部线程读写，并去连CTP前置
    tradeapi.Init()

    # Join函数是使得函数阻塞在这里，等待api实例创建的内部线程的结束。内部线程需要调用release函数才会释放结束
    tradeapi.Join()


if __name__ == '__main__':
    main()
