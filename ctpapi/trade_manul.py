# -*- coding: utf-8 -*-
# 交易库
# thosttraderapi.py     交易头文件
# _thosttraderapi.pyd   交易库转换文件
# thosttraderapi.dll    交易官方动态库，穿透式版为thosttraderapi_se.dll

# 行情查询库
# thostmduserapi.py     行情头文件
# _thostmduserapi.pyd   行情库转换文件
# thostmduserapi.dll    行情官方动态库，穿透式版为thostmduserapi_se.dll

# Req: 请求
# OnRsp: 响应
# ReqQry: 查询
# OnRspQry: 查询响应的请求
# OnRtn: 回报
# OnErrRtn: 错误回报

import thosttraderapi as api
import pandas as pd
import time
from md_demo import CFtdcMdSpi
from trading_algo import TRADEALGO
import csv

# Addr
FrontAddr = "tcp://180.168.146.187:10201"
# LoginInfo
BROKERID = "9999"
USERID = "180692"
PASSWORD = "Abcdef99"
# OrderInfo
EXCHANGEID = "SHFE"
INSTRUMENTID = "rb2110"
subID = ['rb2110']
# PRICE = 4490
PRICE = 4200
VOLUME = 6
DIRECTION = api.THOST_FTDC_D_Sell		# 挂限价卖单
# DIRECTION = api.THOST_FTDC_OF_Close  # 挂限价买单
# DIRECTION = api.THOST_FTDC_DEN_Sell  # 挂限价买单
# open
OFFSET = "0"

# close
# OFFSET = "1"


# THOST_FTDC_PSRC_Buy = _thosttraderapi.THOST_FTDC_PSRC_Buy
# THOST_FTDC_DEN_Buy = _thosttraderapi.THOST_FTDC_DEN_Buy
# THOST_FTDC_D_Buy = _thosttraderapi.THOST_FTDC_D_Buy


def set_price_cal(high_price, low_price, close_price):
    # >>>>>>>>>>>> 计算关键价格 <<<<<<<<<<<<<<
    a = 0.35
    b = 1.04
    c = 0.04
    d = 0.3

    # 计算r_breaker相关的指标
    bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
    ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价

    benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
    senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

    sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
    bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

    return bsetup, benter, bbreak, ssetup, senter, sbreak


def ReqorderfieldInsert(tradeapi):
    print("ReqOrderInsert Start")
    orderfield = api.CThostFtdcInputOrderField()    # CThostFtdcInputOrderField 函数初始化
    orderfield.BrokerID = BROKERID
    orderfield.ExchangeID = EXCHANGEID
    orderfield.InstrumentID = INSTRUMENTID          # 交易品种
    orderfield.UserID = USERID
    orderfield.InvestorID = USERID
    orderfield.Direction = DIRECTION                # 开单类型
    orderfield.LimitPrice = PRICE                   # 开单价格
    orderfield.VolumeTotalOriginal = VOLUME         # 手数
    orderfield.OrderPriceType = api.THOST_FTDC_OPT_LimitPrice
    orderfield.ContingentCondition = api.THOST_FTDC_CC_Immediately
    orderfield.TimeCondition = api.THOST_FTDC_TC_GFD
    orderfield.VolumeCondition = api.THOST_FTDC_VC_AV
    orderfield.CombHedgeFlag = "1"                  # Hedge
    orderfield.CombOffsetFlag = OFFSET
    orderfield.GTDDate = ""
    orderfield.orderfieldRef = "1"
    orderfield.MinVolume = 0
    orderfield.ForceCloseReason = api.THOST_FTDC_FCC_NotForceClose
    orderfield.IsAutoSuspend = 0
    tradeapi.ReqOrderInsert(orderfield, 0)
    print("ReqOrderInsert End")


class CTradeSpi(api.CThostFtdcTraderSpi):
    tapi = ''

    def __init__(self, tapi, set_price):
        api.CThostFtdcTraderSpi.__init__(self)
        self.tapi = tapi
        self.set_price = set_price
        self.open_contract = 0

    def OnFrontConnected(self) -> "void":
        """
        首先运行
        account info, with userid and appid
        :return: nothing
        """
        print("OnFrontConnected")
        authfield = api.CThostFtdcReqAuthenticateField()
        authfield.BrokerID = BROKERID
        authfield.UserID = USERID
        authfield.AppID = "simnow_client_test"
        authfield.AuthCode = "0000000000000000"
        self.tapi.ReqAuthenticate(authfield, 0)     # 确认账号连接正确
        print("send ReqAuthenticate ok")

    def OnRspAuthenticate(self, pRspAuthenticateField: 'CThostFtdcRspAuthenticateField',
                          pRspInfo: 'CThostFtdcRspInfoField',
                          nRequestID: 'int',
                          bIsLast: 'bool') -> "void":
        """
        在 OnFrontConnected 之后运行，输出账户信息
        :param pRspAuthenticateField:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("BrokerID=", pRspAuthenticateField.BrokerID)
        print("UserID=", pRspAuthenticateField.UserID)
        print("AppID=", pRspAuthenticateField.AppID)
        print("AppType=", pRspAuthenticateField.AppType)
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)
        if not pRspInfo.ErrorID:
            loginfield = api.CThostFtdcReqUserLoginField()
            loginfield.BrokerID = BROKERID
            loginfield.UserID = USERID
            loginfield.Password = PASSWORD
            loginfield.UserProductInfo = "python dll"
            self.tapi.ReqUserLogin(loginfield, 0)   # 请求登陆
            print("send login ok")

    def OnRspUserLogin(self, pRspUserLogin: 'CThostFtdcRspUserLoginField',
                       pRspInfo: 'CThostFtdcRspInfoField',
                       nRequestID: 'int',
                       bIsLast: 'bool') -> "void":
        """
        登陆模块，并发出昨日结算确认请求
        :param pRspUserLogin:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("\nOnRspUserLogin")
        print("TradingDay=", pRspUserLogin.TradingDay)
        print("SessionID=", pRspUserLogin.SessionID)
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)
        # self.tapi.SubscribeMarketData(['rb2105'], 1)

        qryinfofield = api.CThostFtdcQrySettlementInfoField()
        qryinfofield.BrokerID = BROKERID
        qryinfofield.InvestorID = USERID
        qryinfofield.TradingDay = pRspUserLogin.TradingDay
        self.tapi.ReqQrySettlementInfo(qryinfofield, 0)
        print("send ReqQrySettlementInfo ok")

    def OnRspQrySettlementInfo(self, pSettlementInfo: 'CThostFtdcSettlementInfoField',
                               pRspInfo: 'CThostFtdcRspInfoField',
                               nRequestID: 'int',
                               bIsLast: 'bool') -> "void":
        """
        第三个运行
        请求昨天的结算单，然后在OnRspQrySettlementInfo中记下SettlementID。
        用Api::ReqSettlementInfoConfirm确认这个结算单就好了。
        :param pSettlementInfo:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("\nOnRspQrySettlementInfo")
        if pSettlementInfo is not None:
            print("content:", pSettlementInfo.Content)
        else:
            print("content null")
        pSettlementInfoConfirm = api.CThostFtdcSettlementInfoConfirmField()
        pSettlementInfoConfirm.BrokerID = BROKERID
        pSettlementInfoConfirm.InvestorID = USERID
        self.tapi.ReqSettlementInfoConfirm(pSettlementInfoConfirm, 0)
        print("send ReqSettlementInfoConfirm ok")

    def OnRspSettlementInfoConfirm(self,
                                   pSettlementInfoConfirm: 'CThostFtdcSettlementInfoConfirmField',
                                   pRspInfo: 'CThostFtdcRspInfoField',
                                   nRequestID: 'int',
                                   bIsLast: 'bool') -> "void":
        """
        第四个运行
        确认前日的结算单后，正式开始交易
        :param pSettlementInfoConfirm:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("\nOnRspSettlementInfoConfirm")
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)

        # 声明需要请求查询投资者持仓响应，等待 OnRspQryInvestorPosition 函数返回响应
        pInvestorPosition = api.CThostFtdcQryInvestorPositionField()
        self.tapi.ReqQryInvestorPosition(pInvestorPosition, 1)

    def OnRspQryInvestorPosition(self,
                                 pInvestorPosition: 'CThostFtdcInvestorPositionField',
                                 pRspInfo: 'CThostFtdcRspInfoField',
                                 nRequestID: 'int',
                                 bIsLast: 'bool') -> "void":
        """
        查询投资者持仓的响应，返回当前持仓情况
        :param pInvestorPosition:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("\nOnRspQryInvestorPosition")
        print("Position=", pInvestorPosition.Position)
        print("Profit=", pInvestorPosition.PositionProfit)
        self.open_contract = pInvestorPosition.Position

        # 请求查询账户资金，等待 OnRspQryTradingAccount 函数返回响应
        pTradingAccount = api.CThostFtdcQryTradingAccountField()
        self.tapi.ReqQryTradingAccount(pTradingAccount, 2)

    def OnRspQryTradingAccount(self,
                               pTradingAccount: 'CThostFtdcTradingAccountField',
                               pRspInfo: 'CThostFtdcRspInfoField',
                               nRequestID: 'int',
                               bIsLast: 'bool') -> "void":
        """
        查询账户资金的回调函数，返回账户资金情况
        :param pTradingAccount:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("============")
        print(pTradingAccount.Balance)

        # 进入询价循环
        pDepthMarketData = api.CThostFtdcQryDepthMarketDataField()
        pDepthMarketData.InstrumentID = INSTRUMENTID
        self.tapi.ReqQryDepthMarketData(pDepthMarketData, 4)

        # 发出交易请求
        # ReqorderfieldInsert(self.tapi)
        print("send ReqorderfieldInsert ok")

    def OnRspOrderInsert(self, pInputOrder: 'CThostFtdcInputOrderField',
                         pRspInfo: 'CThostFtdcRspInfoField',
                         nRequestID: 'int',
                         bIsLast: 'bool') -> "void":
        """
        返回报单信息
        :param pInputOrder:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        print("\nOnRspOrderInsert")
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)

    def OnRspQryDepthMarketData(self, pDepthMarketData: 'CThostFtdcDepthMarketDataField',
                                pRspInfo: 'CThostFtdcRspInfoField',
                                nRequestID: 'int',
                                bIsLast: 'bool') -> "void":
        """
        请求查询行情的响应。
        在函数中增加了循环模块，每隔0.2秒，查询一次行情价格
        :param pDepthMarketData:
        :param pRspInfo:
        :param nRequestID:
        :param bIsLast:
        :return:
        """
        last_price = pDepthMarketData.LastPrice
        print("\nOnRspQryDepthMarketData")
        print(last_price)

        # 循环询问行情
        pDepthMarketData = api.CThostFtdcQryDepthMarketDataField()
        pDepthMarketData.InstrumentID = INSTRUMENTID
        self.tapi.ReqQryDepthMarketData(pDepthMarketData, 4)

    # PRICE = 4320
    # VOLUME = 1
    # # DIRECTION = api.THOST_FTDC_D_Sell	# 挂限价卖单
    # # DIRECTION = api.THOST_FTDC_D_Buy    # 挂限价买单
    # DIRECTION = api.THOST_FTDC_DEN_Sell   # 挂限价买单
    # DIRECTION = api.THOST_FTDC_OF_Sell    # 平当日单
    # THOST_FTDC_PSRC_Buy
    # # open
    # # OFFSET = "0"
    # # close
    # OFFSET = "1"
    @staticmethod
    def trading_para_set(trade_signal, cur_price):
        global DIRECTION
        global OFFSET
        global VOLUME
        global PRICE
        PRICE = cur_price
        if trade_signal == "buy_open":
            DIRECTION = api.THOST_FTDC_D_Buy
            OFFSET = "0"
        elif trade_signal == "sell_open":
            DIRECTION = api.THOST_FTDC_D_Sell
            OFFSET = "0"
        elif trade_signal == "buy_close":
            DIRECTION = api.THOST_FTDC_PSRC_Buy
            OFFSET = "1"
        elif trade_signal == "sell_close":
            DIRECTION = api.THOST_FTDC_PSRC_Sell
            OFFSET = "1"
        else:
            pass

    def OnRtnOrder(self, pOrder: 'CThostFtdcOrderField') -> "void":
        """
        输出报单成交信息
        :param pOrder:
        :return:
        """
        print("\nOnRtnDepthMarketData")
        print("OnRtnOrder")
        print("OrderStatus=", pOrder.OrderStatus)
        print("StatusMsg=", pOrder.StatusMsg)
        print("LimitPrice=", pOrder.LimitPrice)


def main():
    config = pd.read_csv("config.csv")
    bsetup, benter, bbreak, ssetup, senter, sbreak = set_price_cal(high_price=config.high_price,
                                                                   low_price=config.low_price,
                                                                   close_price=config.close_price)
    set_price_use = {"bsetup": bsetup,
                     "benter": benter,
                     "bbreak": bbreak,
                     "ssetup": ssetup,
                     "senter": senter,
                     "sbreak": sbreak}

    # 创建API实例，初始化API
    tradeapi = api.CThostFtdcTraderApi_CreateFtdcTraderApi()

    # 创建SPI实例。CTradeSpi是继承自头文件中CThostFtdcTraderSpi的类型，用于收从CTP的回复，可以重写父类中的函数来实现自己的逻辑
    tradespi = CTradeSpi(tradeapi, set_price_use)

    # 注册前置地址，是指将CTP前置的IP地址注册进API实例内
    tradeapi.RegisterFront(FrontAddr)

    # 将创建的SPI实例注册进实例，这样该API实例发出的请求对应的回报，就会回调到对应的SPI实例的函数
    tradeapi.RegisterSpi(tradespi)

    # 订阅共有流与私有流。订阅方式主要有三种，分为断点续传，重传和连接建立开始传三种。
    tradeapi.SubscribePrivateTopic(api.THOST_TERT_QUICK)
    tradeapi.SubscribePublicTopic(api.THOST_TERT_QUICK)

    # API启动，init之后就会启动一个内部线程读写，并去连CTP前置
    tradeapi.Init()

    # Join函数是使得函数阻塞在这里，等待api实例创建的内部线程的结束。内部线程需要调用release函数才会释放结束
    tradeapi.Join()


if __name__ == '__main__':
    main()

