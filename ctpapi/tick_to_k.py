# -*- coding: utf-8 -*-

import thostmduserapi as mdapi
import csv
import time
import os

'''
以下为需要订阅行情的合约号，注意选择有效合约；
有效连上但没有行情可能是过期合约或者不再交易时间内导致
'''

# subID = ["au1912","IC1909","i2001","TA001"]
subID = ["rb2110"]


class CFtdcMdSpi(mdapi.CThostFtdcMdSpi):
    def __init__(self, tapi):
        mdapi.CThostFtdcMdSpi.__init__(self)
        self.tapi = tapi
        self.csvfiles = {}
        self.submap = {}
        self.mink = []

    def __openCsv(self):
        csvheader = ({"TradingDay", "InstrumentID", "LastPrice", "PreSettlementPrice", "PreClosePrice",
                      "PreOpenInterest", "OpenPrice", "HighestPrice", "LowestPrice", "Volume", "Turnover",
                      "OpenInterest", "ClosePrice", "SettlementPrice", "UpperLimitPrice", "LowerLimitPrice",
                      "PreDelta", "CurrDelta", "UpdateTime", "UpdateMillisec", "BidPrice1", "BidVolume1",
                      "AskPrice1", "AskVolume1", "AveragePrice", "ActionDay"})
        for id in subID:
            csvname = f"{id}.csv"
            csvfile = open(csvname, 'w', newline='')
            csvfile_w = csv.writer(csvfile)
            csvfile_w.writerow(csvheader)
            self.csvfiles[id] = csvfile
            self.submap[id] = csvfile_w

    def OnFrontConnected(self) -> "void":
        print("OnFrontConnected")
        loginfield = mdapi.CThostFtdcReqUserLoginField()
        loginfield.BrokerID = "9999"
        loginfield.UserID = "180692"
        loginfield.Password = "Abcdef99"
        loginfield.AppID = "simnow_client_test"
        loginfield.AuthCode = "0000000000000000"
        loginfield.UserProductInfo = "python dll"
        self.tapi.ReqUserLogin(loginfield, 0)
        self.__openCsv()

    def OnRspUserLogin(self,
                       pRspUserLogin: 'CThostFtdcRspUserLoginField',
                       pRspInfo: 'CThostFtdcRspInfoField',
                       nRequestID: 'int',
                       bIsLast: 'bool') -> "void":
        print(
            f"OnRspUserLogin, SessionID={pRspUserLogin.SessionID},ErrorID={pRspInfo.ErrorID},ErrorMsg={pRspInfo.ErrorMsg}")
        ret = self.tapi.SubscribeMarketData([id.encode('utf-8') for id in subID], len(subID))

    def OnRtnDepthMarketData(self, pDepthMarketData: 'CThostFtdcDepthMarketDataField') -> "void":
        """
        Receive message of offer per 500ms
        :param pDepthMarketData: update time means the last rewrite time
        :return:
        """
        # print("OnRtnDepthMarketData")
        mdlist = ([pDepthMarketData.TradingDay, pDepthMarketData.InstrumentID, pDepthMarketData.LastPrice,
                   pDepthMarketData.PreSettlementPrice, pDepthMarketData.PreClosePrice,
                   pDepthMarketData.PreOpenInterest, pDepthMarketData.OpenPrice, pDepthMarketData.HighestPrice,
                   pDepthMarketData.LowestPrice, pDepthMarketData.Volume, pDepthMarketData.Turnover,
                   pDepthMarketData.OpenInterest, pDepthMarketData.ClosePrice, pDepthMarketData.SettlementPrice,
                   pDepthMarketData.UpperLimitPrice, pDepthMarketData.LowerLimitPrice, pDepthMarketData.PreDelta,
                   pDepthMarketData.CurrDelta, pDepthMarketData.UpdateTime, pDepthMarketData.UpdateMillisec,
                   pDepthMarketData.BidPrice1, pDepthMarketData.BidVolume1, pDepthMarketData.AskPrice1,
                   pDepthMarketData.AskVolume1, pDepthMarketData.AveragePrice, pDepthMarketData.ActionDay])

        # mdlist = ({"TradingDay": pDepthMarketData.TradingDay, "InstrumentID": pDepthMarketData.InstrumentID,
        #            "LastPrice": pDepthMarketData.LastPrice, "OpenPrice": pDepthMarketData.OpenPrice,
        #            "HighestPrice": pDepthMarketData.HighestPrice, "LowestPrice": pDepthMarketData.LowestPrice,
        #            "Volume": pDepthMarketData.Volume,
        #            "UpdateTime": pDepthMarketData.UpdateTime, "UpdateMillisec": pDepthMarketData.UpdateMillisec})
        #
        # # 检测价格是 0ms 或者 500ms 发出的
        # if self.mink is not []:
        #     if pDepthMarketData.UpdateMillisec == 500:
        #         sub_md = mdlist
        #     else:
        #         self.mink = {"TradingDay": pDepthMarketData.TradingDay, "InstrumentID": pDepthMarketData.InstrumentID,
        #                      "LastPrice": pDepthMarketData.LastPrice, "OpenPrice": pDepthMarketData.OpenPrice,
        #                      "HighestPrice": pDepthMarketData.HighestPrice, "LowestPrice": pDepthMarketData.LowestPrice,
        #                      "Volume": pDepthMarketData.Volume,
        #                      "UpdateTime": pDepthMarketData.UpdateTime, "UpdateMillisec": pDepthMarketData.UpdateMillisec}

        # if self.mink[]

        # 确认运行时间段
        time_curr = time.asctime(time.localtime(time.time()))
        time_curr = time_curr[11:19]
        time_tick = int(time_curr[:2] + time_curr[3:5] + time_curr[6:8])
        while time_tick < 90000 or 113001 < time_tick < 133000 or 150001 < time_tick < 210000 or time_tick > 230001:
            # 休盘时间
            time.sleep(1)

        fields = mdlist

        with open(r"data/{}.csv".format(pDepthMarketData.TradingDay), 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(fields)

        f.close()

        print(mdlist, end='\r')

        self.submap[pDepthMarketData.InstrumentID].writerow(mdlist)
        self.csvfiles[pDepthMarketData.InstrumentID].flush()

    def OnRspSubMarketData(self, pSpecificInstrument: 'CThostFtdcSpecificInstrumentField',
                           pRspInfo: 'CThostFtdcRspInfoField',
                           nRequestID: 'int',
                           bIsLast: 'bool') -> "void":
        print("OnRspSubMarketData")
        print("InstrumentID=", pSpecificInstrument.InstrumentID)
        print("ErrorID=", pRspInfo.ErrorID)
        print("ErrorMsg=", pRspInfo.ErrorMsg)


def main():
    mduserapi = mdapi.CThostFtdcMdApi_CreateFtdcMdApi()
    mduserspi = CFtdcMdSpi(mduserapi)
    mduserapi.RegisterFront("tcp://101.230.209.178:53313")
    # mduserapi.RegisterFront("tcp://180.168.146.187:10131")
    # mduserapi.RegisterFront("tcp://180.168.146.187:10101")
    mduserapi.RegisterSpi(mduserspi)
    mduserapi.Init()

    # 进入循环
    mduserapi.Join()


# ['20210112', 'rb2105', 4317.0, 4404.0, 4348.0, 1247798.0, 4349.0, 4355.0, 4262.0, 1741351,
# 74852671710.0, 1181441.0, 1.7976931348623157e+308, 1.7976931348623157e+308, 4624.0, 4183.0, 0.0,
# 1.7976931348623157e+308, '16:16:37', 0, 4316.0, 33, 4317.0, 326, 42985.40139810986, '20210111']
# 20200112信息


if __name__ == '__main__':
    main()
