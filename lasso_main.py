# -*-coding:utf-8-*-

import time
import pandas as pd
import numpy as np
from lasso import DATAPROCESS
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Lasso


if __name__ == "__main__":
    # xau_data = pd.read_csv("Data/XAUUSD_m15_BidAndAsk.csv")  # 原始的15 min数据
    #
    # data_process = DATAPROCESS()
    # mv_10, mv_20, mv_40, mv_60, mv_120 = data_process.moving_average(xau_data.CloseAsk)
    # mv_10 = pd.DataFrame(mv_10, columns=["mv_10"])
    # mv_20 = pd.DataFrame(mv_20, columns=["mv_20"])
    # mv_40 = pd.DataFrame(mv_40, columns=["mv_40"])
    # mv_60 = pd.DataFrame(mv_60, columns=["mv_60"])
    # mv_120 = pd.DataFrame(mv_120, columns=["mv_120"])
    #
    # xau_data = pd.concat([xau_data, mv_10, mv_20, mv_40, mv_60, mv_120], axis=1)
    # # print(xau_data)
    # xau_data.to_csv("Data/xau_data_lasso.csv", index_label=None)
    xau_data = pd.read_csv("Data/xau_data_lasso.csv", index_col=0)
    last_n = 20

    for i in range(last_n):
        sub_p = xau_data.iloc[:, 9]

    # p1 = xau_data.iloc[]

    # print(xau_data)
    X = xau_data.iloc[:-1, 2:]
    Y = xau_data.iloc[1:, 9]
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=3)

    # print(len(x_train))
    # print(x_train)
    # print(x_test)
    for alpha in range(1, 2):
        lasso = Lasso(alpha=round(alpha/10, 1))  # 默认alpha =1
        lasso.fit(x_train, y_train)

        train_score = lasso.score(x_train, y_train)
        test_score = lasso.score(x_test, y_test)

        y_predict = lasso.predict(x_test)
        # y_delta = np.subtract(y_predict, y_test)
        # true_delta = np.subtract()

        n = 0
        y_true = np.array(y_test)
        for i in range(len(x_test)):
            id = y_test.index[i]
            y_predict[i] = y_predict[i] > xau_data.iloc[id-1, 9]
            y_true[i] = y_true[i] > xau_data.iloc[id-1, 9]
            if y_predict[i] == y_true[i]:
                n += 1
        # print(y_predict)
        print("total num is", len(x_test))
        print("ture prediction is", n)
        print("predict accuracy is", n/len(x_test))
        print("\n")
        coeff_used = np.sum(lasso.coef_ != 0)
        print("training score:", train_score)
        print("test score: ", test_score)
        print("number of features used: ", coeff_used)

    # trade_algo = TRADING(balance=10000, slippage=0, handling_fee=0.5)
    #
    # counts = []
    # t = time.time()
    # processor_num = 1
    #
    # para = [[0.15, 0.19, 76]]
    #
    # trade_algo.main(0, para, 1, 1)
    #
    # print("Singleprocess cpu", time.time() - t)

