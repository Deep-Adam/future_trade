# r_breaker策略，第一次回测

from PythonApi import *
import numpy as np
import pandas as pd
import talib
import math


def set_price_cal(high_price, low_price, close_price, a, b, c, d):
    # 计算r_breaker相关的指标
    bsetup = int(low_price - a * (high_price - close_price))  # 观察买入价
    ssetup = int(high_price + a * (close_price - low_price))  # 观察卖出价
    # print([low_price, a, high_price, close_price, bsetup])

    benter = int((b / 2) * (high_price + low_price) - c * high_price)  # 反转买入价
    senter = int((b / 2) * (high_price + low_price) - c * low_price)  # 反转卖出价

    sbreak = int(bsetup - d * (ssetup - bsetup))  # 突破买入价
    bbreak = int(ssetup + d * (ssetup - bsetup))  # 突破卖出价

    return bsetup, benter, bbreak, ssetup, senter, sbreak


def trading(context, price, refe_price, signal, unit, text_out):
    # 检测是否持仓
    portfolio = get_portfolio(context.s, 2)
    print([portfolio.buy_quantity, portfolio.sell_quantity, signal, price, refe_price])

    if portfolio.buy_quantity > 0:
        if signal == 1:
            pass
        else:
            sell_close(context.s, "Market", 0, portfolio.buy_quantity, serial_id=1)
            print(["平仓", text_out])
            # 判断是否滑点过大
            if refe_price - price > 10:
                pass
            else:
                sell_open(context.s, "Market", 0, unit, serial_id=2)
                print(text_out)
    elif portfolio.sell_quantity > 0:
        if signal == -1:
            pass
        else:
            buy_close(context.s, "Market", 0, portfolio.sell_quantity, serial_id=3)
            print(["平仓", text_out])
            # 判断是否滑点过大
            if price - refe_price > 10:
                pass
            else:
                buy_open(context.s, "Market", 0, unit, serial_id=4)
                print(text_out)
    else:
        if signal > 0:
            # 判断是否滑点过大
            if price - refe_price > 10:
                pass
            else:
                buy_open(context.s, "Market", 0, unit, serial_id=5)
                print(text_out)
        else:
            # 判断是否滑点过大
            if refe_price - price > 10:
                pass
            else:
                sell_open(context.s, "Market", 0, unit, serial_id=6)
                print(text_out)


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。--(必须实现)
def init(context):
    context.a = 0.3
    context.b = 1.06
    context.c = 0.06
    context.d = 0.3
    context.t = 60

    # 每日最高最低价
    context.day_high = 0
    context.day_low = 10000
    # 记录建仓的atr
    context.entry = 0
    # 记录交易次数
    context.num = 0
    # 交易标的
    context.s = context.run_info.base_book_id
    # 记录上次开仓价
    context.enterprice = 0
    # 记录前一个交易日的收盘价
    context.close_pre = 0
    context.n = 0


# 你选择的品种的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新。--(必须实现)
def handle_bar(context):
    time = history_bars(context.s, 1, 'self', 'datetime', include_now=True)
    close = history_bars(context.s, 3, 'self', 'close', include_now=True)
    day_close = [4411, 4381, 4387, 4476, 4377, 4280, 4308, 4273, 4371, 4345, 4324, 4297, 4304, 4366, 4287, 4332, 4371,
                 4338, 4289, 4268, 4205, 4141, 4194, 4236, 4281, 4329, 4392, 4404, 4555, 4530, 4612, 4553, 4644, 4672,
                 4696]

    sub_time = str(int(time[0]))  # 当前K线的时间

    ##获取20180901至20180910区间的日线收盘价序列
    # bar_close=history_bars_date('SQRB00','20180901','20180910','1d','close')
    ##对返回值k线数量进行检查
    # print(bar_close)

    if sub_time[-6:-2] == "0901":
        context.close_pre = day_close[context.n]
        context.n += 1

    high = history_bars(context.s, context.t, 'self', 'high', include_now=True)
    low = history_bars(context.s, context.t, 'self', 'low', include_now=True)
    open_price = history_bars(context.s, 1, 'self', 'open', include_now=True)

    highest = max(high[:-1])
    lowest = max(low[:-1])
    open_price = open_price[-1]
    unit = 1
    # print(high[:-1])
    # print(max(high[:-1]))

    # 检测是否持仓
    # portfolio = get_portfolio(context.s, 2)
    # if portfolio.buy_quantity > 0:
    #     sell_close(context.s, "Market", 0, 1,serial_id = 1)

    context.day_high = max(context.day_high, high[-1])
    context.day_low = min(context.day_low, low[-1])

    bsetup, benter, bbreak, ssetup, senter, sbreak = set_price_cal(highest, lowest, context.close_pre, context.a,
                                                                   context.b, context.c, context.d)

    pri_log = ["time:", sub_time[-6:], "hl:", high[-1], low[-1], "day:", context.day_high, context.day_low, "set:",
               highest, lowest, context.close_pre, "b:", bsetup, benter, bbreak, "s:", ssetup, senter, sbreak]
    print(pri_log)

    # price_curr = get_dynainf('SQRB00',7)

    if high[-1] >= bbreak:
        trading(context, open_price, bbreak, 1, unit, 'bbreak')
        print([sub_time[-6:], 'bbreak', open_price, bbreak, 1, unit])
    elif low[-1] <= sbreak:
        trading(context, open_price, sbreak, -1, unit, 'sbreak')
        print([sub_time[-6:], 'sbreak', open_price, sbreak, -1, unit])
    else:
        if context.day_high > ssetup and low[-1] < senter:
            trading(context, open_price, senter, -1, unit, 'senter')
            print([sub_time[-6:], 'senter', open_price, senter, -1, unit])
        elif context.day_low < bsetup and high[-1] > benter:
            trading(context, open_price, benter, 1, unit, 'benter')
            print([sub_time[-6:], 'benter', open_price, benter, 1, unit])
        else:
            pass

    # if high[-1] >= bbreak:
    #    trading(context, high[-1], bbreak, 1, unit, 'bbreak')
    # elif low[-1] <= sbreak:
    #    trading(context, low[-1], sbreak, -1, unit, 'sbreak')
    # else:
    #    if context.day_high > ssetup and low[-1] < senter:
    #        trading(context, low[-1], senter, -1, unit, 'senter')
    #    elif context.day_low < bsetup and high[-1] > benter:
    #        trading(context, high[-1], benter, 1, unit, 'benter')
    #    else:
    #        pass

    # sub_time = str(int(time[0]))  # 20210301175200

    # print("\n")

    # print(sub_time)
    # buy_open(context.s, "Market", 0, unit,serial_id = 7)
    # print([sub_time, pre_date, next_date])


# r_breaker策略，第一次回测

from PythonApi import *
import numpy as np
import pandas as pd
import talib
import math


def trading(context, signal, unit):
    # 检测是否持仓
    portfolio = get_portfolio(context.s, 2)
    # print([ portfolio.buy_quantity, portfolio.sell_quantity, signal, price, refe_price])

    if portfolio.buy_quantity > 0:
        if signal == "buy":
            pass
        elif signal == "sell":
            sell_close(context.s, "Market", 0, portfolio.buy_quantity, serial_id=1)
            sell_open(context.s, "Market", 0, unit, serial_id=2)
        else:
            sell_close(context.s, "Market", 0, portfolio.buy_quantity, serial_id=3)

    elif portfolio.sell_quantity > 0:
        if signal == "sell":
            pass
        elif signal == "buy":
            buy_close(context.s, "Market", 0, portfolio.sell_quantity, serial_id=4)
            buy_open(context.s, "Market", 0, unit, serial_id=5)
        else:
            buy_close(context.s, "Market", 0, portfolio.sell_quantity, serial_id=6)
    else:
        if signal == "buy":
            buy_open(context.s, "Market", 0, unit, serial_id=7)

        elif signal == "sell":
            sell_open(context.s, "Market", 0, unit, serial_id=8)

        else:
            pass


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。--(必须实现)
def init(context):
    # 记录建仓的atr
    context.entry = 0
    # 记录交易次数
    context.num = 0
    # 交易标的
    context.s = context.run_info.base_book_id
    # 记录上次开仓价
    context.enterprice = 0
    # 记录前一个交易日的收盘价
    context.close_pre = 0
    context.n = 0


# 你选择的品种的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新。--(必须实现)
def handle_bar(context):
    time = history_bars(context.s, 1, 'self', 'datetime', include_now=True)
    open_price = history_bars(context.s, 2, 'self', 'open', include_now=True)
    close_price = history_bars(context.s, 20, 'self', 'close', include_now=True)
    volume = history_bars(context.s, 2, 'self', 'volume', include_now=True)

    mv_20 = np.average(close_price[:])

    sub_time = str(int(time[0]))  # 20210301175200

    pri_log = ["time:", sub_time[-6:], "price:", open_price[-1], close_price[-1], "volums", volume[-1], mv_20]
    print(pri_log)

    if sub_time[-6:-2] == "0901":
        pass
    else:
        if close_price[-1] > open_price[-1] and volume[-1] > volume[-2] and close_price[-1] > mv_20:
            trading(context, "buy", 1)
        elif close_price[-1] < open_price[-1] and volume[-1] > volume[-2] and close_price[-1] < mv_20:
            trading(context, "sell", 1)
        else:
            trading(context, "close", 1)

    # if sub_time[-6:-2] == "0901":
    #    context.close_pre = day_close[context.n]
    #    context.n += 1

    # if high[-1] >= bbreak:
    #    trading(context, high[-1], bbreak, 1, unit, 'bbreak')
    # elif low[-1] <= sbreak:
    #    trading(context, low[-1], sbreak, -1, unit, 'sbreak')
    # else:
    #    if context.day_high > ssetup and low[-1] < senter:
    #        trading(context, low[-1], senter, -1, unit, 'senter')
    #    elif context.day_low < bsetup and high[-1] > benter:
    #        trading(context, high[-1], benter, 1, unit, 'benter')
    #    else:
    #        pass

    # sub_time = str(int(time[0]))  # 20210301175200

    # print("\n")

    # print(sub_time)
    # buy_open(context.s, "Market", 0, unit,serial_id = 7)
    # print([sub_time, pre_date, next_date])

    a = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1,
         1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1,
         1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1,
         0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1,
         1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
         0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1,
         0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0,
         0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0,
         1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1,
         1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0,
         0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1,
         1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0,
         0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1,
         1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0,
         1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1,
         1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0,
         1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
         1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1,
         0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1,
         1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0,
         1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0,
         0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
         0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1,
         1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0,
         0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
         1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0,
         0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0,
         1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0,
         0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1,
         1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
         1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
         0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1,
         0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0,
         1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0,
         0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
         0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0,
         0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1,
         0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1,
         1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1,
         1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0,
         1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,
         1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1,
         0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0,
         1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1,
         1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
         1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,
         0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1]

    if sub_time[-6:-2] == "0901":
        pass
    else:
        if close_price[-1] > open_price[-1] and volume[-1] > volume[-2] and close_price[-1] > mv_20:
            trading(context, "buy", 1)
        elif close_price[-1] < open_price[-1] and volume[-1] > volume[-2] and close_price[-1] < mv_20:
            trading(context, "sell", 1)
        else:
            trading(context, "close", 1)



"""================================"""
# r_breaker策略，第一次回测

from PythonApi import *
import numpy as np
import pandas as pd
import talib
import math
import os


def trading(context, signal, unit):
    # print(_signal[i], "   ", X.iloc[i + 1, 4], X.iloc[i + 1, 5], X.iloc[i, 3])

    portfolio = get_portfolio(context.s, 2)
    # print([signal, portfolio.buy_quantity, portfolio.sell_quantity])
    if signal == 1:
        if portfolio.buy_quantity > 0:
            pass
        elif portfolio.sell_quantity > 0:
            buy_close(context.s, "Market", 0, unit, serial_id=1)
            buy_open(context.s, "Market", 0, unit, serial_id=2)
        else:
            buy_open(context.s, "Market", 0, unit, serial_id=3)
    else:
        if portfolio.buy_quantity > 0:
            sell_close(context.s, "Market", 0, portfolio.buy_quantity, serial_id=4)
            sell_open(context.s, "Market", 0, unit, serial_id=5)

            # sell_close(context.s, "Market", 0, portfolio.buy_quantity,serial_id = 7)
            # sell_open(context.s, "Market", 0, unit,serial_id = 8)

        elif portfolio.sell_quantity > 0:
            pass
        else:
            sell_open(context.s, "Market", 0, unit, serial_id=6)


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。--(必须实现)
def init(context):
    # 记录建仓的atr
    context.entry = 0
    # 记录交易次数
    context.num = 0
    # 交易标的
    context.s = context.run_info.base_book_id
    # 记录上次开仓价
    context.enterprice = 0
    # 记录前一个交易日的收盘价
    context.close_pre = 0
    context.n = 0

    context.signal = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0,
                      1, 0, 1, 1,
                      1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0,
                      1, 1, 1, 1,
                      0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0,
                      0, 0, 0, 1,
                      1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1,
                      1, 1, 1, 0,
                      0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 0, 1,
                      0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1,
                      0, 1, 0, 0,
                      0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
                      1, 1, 0, 1,
                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1,
                      1, 0, 0, 0,
                      1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
                      1, 1, 0, 1,
                      1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
                      1, 1, 0, 0,
                      0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0,
                      0, 0, 1, 1,
                      1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1,
                      0, 1, 1, 0,
                      0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1,
                      1, 1, 0, 0,
                      1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                      1, 1, 1, 1,
                      1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0,
                      0, 0, 1, 0,
                      1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 0, 0,
                      1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0,
                      0, 0, 1, 1,
                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0,
                      1, 0, 1, 1,
                      0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1,
                      0, 0, 1, 1,
                      1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1,
                      1, 1, 1, 0,
                      1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1,
                      1, 0, 1, 0,
                      0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1,
                      0, 1, 1, 1,
                      1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 0, 0,
                      0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0,
                      0, 0, 0, 1,
                      1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1,
                      0, 0, 0, 0,
                      1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0,
                      0, 0, 1, 0,
                      0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1,
                      1, 1, 1, 0,
                      1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0,
                      1, 0, 0, 0,
                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0,
                      0, 0, 0, 0,
                      0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
                      1, 1, 1, 1,
                      1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1,
                      0, 0, 0, 0,
                      1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
                      0, 1, 0, 0,
                      0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1,
                      1, 0, 1, 1,
                      0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0,
                      0, 0, 0, 0,
                      1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0,
                      1, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1,
                      0, 0, 0, 0,
                      0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0,
                      0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 1, 0, 0,
                      0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1,
                      0, 1, 0, 0,
                      0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1,
                      0, 0, 0, 1,
                      0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
                      1, 1, 0, 1,
                      1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1,
                      0, 0, 1, 1,
                      1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0,
                      0, 0, 1, 0,
                      1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1,
                      1, 0, 0, 0,
                      1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1,
                      1, 1, 1, 1,
                      0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1,
                      1, 0, 1, 0,
                      1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1,
                      0, 0, 1, 1,
                      1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1,
                      1, 1, 1, 1,
                      1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
                      1, 1, 1, 1,
                      0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1,
                      0, 0, 0, 1,
                      0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1,
                      1, 1, 1, 1,
                      1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1]


# 你选择的品种的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新。--(必须实现)
def handle_bar(context):
    time = history_bars(context.s, 1, 'self', 'datetime', include_now=True)
    # open_price = history_bars(context.s, 2, 'self', 'open', include_now=True)
    sub_time = str(int(time[0]))  # 20210301175200

    pri_log = ["n:", context.n, context.signal[context.n], "time:", sub_time]
    print(pri_log)

    trading(context, context.signal[context.n], 1)
    context.n += 1

    # if sub_time[-6:-2] == "0901":
    #    context.close_pre = day_close[context.n]
    #    context.n += 1

    # if high[-1] >= bbreak:
    #    trading(context, high[-1], bbreak, 1, unit, 'bbreak')
    # elif low[-1] <= sbreak:
    #    trading(context, low[-1], sbreak, -1, unit, 'sbreak')
    # else:
    #    if context.day_high > ssetup and low[-1] < senter:
    #        trading(context, low[-1], senter, -1, unit, 'senter')
    #    elif context.day_low < bsetup and high[-1] > benter:
    #        trading(context, high[-1], benter, 1, unit, 'benter')
    #    else:
    #        pass

    #  "price:", open_price[-1], close_price[-1], "volums", volume[-1], mv_20



