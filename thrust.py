import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import math
from maximum_drawdown import MaximumDrawdown


class TRADING:
    def __init__(self, slippage=0, handling_fee=5, file_path=""):
        self.slippage = slippage
        self.handling_fee = handling_fee
        self.file_path = file_path

    def data_load(self):
        data = pd.read_csv(self.file_path, index_col=0)
        # data = pd.read_csv("Data/thrust/15min_2018.csv", index_col=0)
        # data = pd.read_csv("Data/RB9999.XSGE/RB9999.XSGE.csv", index_col=0)

        # data = data.iloc[5533:, :]       # 从2019年开始

        # data = data.iloc[11074:, :]      # 从2020年开始
        # data.reset_index(drop=True, inplace=True)

        return data

    @staticmethod
    def parameter_list(k1_lis, k2_lis, period_lis):
        para_list = []
        for k1 in k1_lis:
            for k2 in k2_lis:
                for period in period_lis:
                    para_list.append([k1, k2, period])

        return para_list

    def main(self, pro_id, para_list, stride):
        data = self.data_load()

        length = len(data)
        n = pro_id * stride
        save_log = pd.DataFrame()
        save_all = pd.DataFrame(columns=["No", 'k1', 'k2', "period", 'rate', 'trading', 'maxdrawdown'])

        with tqdm(total=stride, ncols=100) as pbar:
            for [k1, k2, period] in para_list:
                position = 0
                account = 10000
                account_log = []
                trading = 0
                single_posi = int(account / 8000)

                for i in range(period, length):
                    hh = max(data['high'][(i - period):(i + 1)])
                    lc = min(data['close'][(i - period):(i + 1)])
                    hc = max(data['close'][(i - period):(i + 1)])
                    ll = min(data['low'][(i - period):(i + 1)])
                    range_price = max(hh - lc, hc - ll)
                    buyline = data['open'][i] + k1 * range_price
                    sellline = data['open'][i] - k2 * range_price

                    # single_posi = max(single_posi, int(account / 8000))
                    trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # 交易成本
                    # single_posi = 1

                    if data['close'][i] > buyline:
                        if position < 0:
                            position = single_posi
                            account -= trading_fee
                            # account += 10 * single_posi * (data['close'][i - 1] + data['close'][i] - 2 * buyline)
                            account += 10 * single_posi * (data['close'][i - 1] - data['close'][i])
                            trading += 1
                        elif position == 0:
                            position = single_posi
                            trading += 1
                            account -= trading_fee
                            # account += 10 * single_posi * (data['close'][i] - buyline)
                        else:
                            account += 10 * single_posi * (data['close'][i] - data['close'][i - 1])
                    elif data['close'][i] < sellline:
                        if position < 0:
                            account += 10 * single_posi * (data['close'][i - 1] - data['close'][i])
                        elif position == 0:
                            position = -single_posi
                            trading += 1
                            account -= trading_fee
                            # account -= 10 * single_posi * (data['close'][i] - buyline)
                        else:
                            position = -single_posi
                            trading += 1
                            # account += 10 * single_posi * (2 * sellline - data['close'][i] - data['close'][i - 1])
                            account += 10 * single_posi * (data['close'][i] - data['close'][i - 1])
                            account -= trading_fee
                    else:
                        if position > 0:
                            account += 10 * single_posi * (data['close'][i] - data['close'][i - 1])
                        elif position < 0:
                            account += 10 * single_posi * (data['close'][i - 1] - data['close'][i])
                        else:
                            pass
                    # print(account)

                    account_log.append([account])
                # print(account_log)

                drawdown_cal = MaximumDrawdown()
                max_drawdown = round(drawdown_cal.calculate(account_log), 4)
                account_log = pd.DataFrame(account_log, columns=[str(k1) + '_' + str(k2) + '_' + str(period)])
                save_log = pd.concat((save_log, account_log), axis=1)

                save_all = save_all.append([{"No": n,
                                             'k1': k1,
                                             'k2': k2,
                                             "period": period,
                                             'rate': round((account - 10000) / 100, 2),
                                             'trading': trading,
                                             'maxdrawdown': max_drawdown}])

                if n % 10 == 0:
                    # 保存迭代的结果
                    save_all.to_csv("Result/thrust/save_all_" + str(pro_id) + ".csv", index_label=None)
                    save_log.to_csv("Result/thrust/account_log_" + str(pro_id) + ".csv", index_label=None)

                n += 1
                pbar.update(1)

        save_all.to_csv("Result/thrust/save_all_" + str(pro_id) + ".csv", index_label=None)
        save_log.to_csv("Result/thrust/account_log_" + str(pro_id) + ".csv", index_label=None)


# if __name__ == "__main__":
#     # downloaddata = DownloadData()
#     # downloaddata.data_download("AU9999.XSGE")
#     # Dual thrust
#     # (1)N日High的最高价HH, N日Close的最低价LC;
#     # (2)N日Close的最高价HC，N日Low的最低价LL;
#     # (3)Range = Max(HH-LC,HC-LL)
#     # (4)BuyLine = Open + K1*Range
#     # (5)SellLine = Open + K2*Range
#
#     # data = pd.read_csv("Data/try/account_15min.csv", index_col=0)
#     # data = data.iloc[40212:, :]
#     # # data.to_csv("15min_2016.csv", index_label=0)
#     # data.reset_index(drop=True, inplace=True)
#     # print(data)
#     # data.to_csv("Data/thrust/15min_2018.csv", index_label=0)
#
#     # for k in range(18, 20, 2):
#     #     for j in range(16, 18, 2):
#     #         for p in range(35, 40, 5):
#
#     trade_algo = TRADING(slippage=0, handling_fee=5)
#
#     para = trade_algo.parameter_list([0.18], [0.16], [35])
#
#     trade_algo.main(0, para, len(para))

