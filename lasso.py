import numpy as np
import pandas as pd
from tqdm import tqdm
import re
import os
import math
import matplotlib as plt
from maximum_drawdown import MaximumDrawdown


class DATAPROCESS:
    # def __init__(self):

    def moving_average(self, data_input):
        """
        计算移动平均线
        :param data_input: 股票价格
        :return: 移动平均线的数组
        """
        # 计算10/20/40/60/120日均线
        days = len(data_input)

        # 10日均线计算
        mv_10 = []
        for i in range(days):
            mv_10.append(np.average(data_input[max(0, i - 9): i+1]))

        # 20日均线计算
        mv_20 = []
        for i in range(days):
            mv_20.append(np.average(data_input[max(0, i - 19): i + 1]))

        # 40日均线计算
        mv_40 = []
        for i in range(days):
            mv_40.append(np.average(data_input[max(0, i - 39): i + 1]))

        # 60日均线计算
        mv_60 = []
        for i in range(days):
            mv_60.append(np.average(data_input[max(0, i - 59): i + 1]))

        # 120日均线计算
        mv_120 = []
        for i in range(days):
            mv_120.append(np.average(data_input[max(0, i - 119): i + 1]))

        mv_10 = np.around(mv_10, 2)
        mv_20 = np.around(mv_20, 2)
        mv_40 = np.around(mv_40, 2)
        mv_60 = np.around(mv_60, 2)
        mv_120 = np.around(mv_120, 2)

        return mv_10, mv_20, mv_40, mv_60, mv_120


class TRADING:
    def __init__(self, balance, slippage, handling_fee=5):
        self.slippage = slippage
        self.handling_fee = handling_fee
        self.balance = balance

    @staticmethod
    def parameter_list(k1_lis, k2_lis, period_lis):
        para_list = []
        for k1 in k1_lis:
            for k2 in k2_lis:
                for period in period_lis:
                    para_list.append([k1, k2, period])

        return para_list

    def main(self, pro_id, para_list, stride, order_save):
        xau_data = pd.read_csv("Data/XAUUSD_m15_BidAndAsk.csv")  # 原始的1 min数据

        length = len(xau_data)
        n = pro_id * stride
        save_log = pd.DataFrame()
        save_all = pd.DataFrame(columns=["No", 'k1', 'k2', "period", 'rate', 'trading', 'maxdrawdown'])

        with tqdm(total=stride, ncols=100) as pbar:
            for [k1, k2, period] in para_list:
                position = 0
                balance = self.balance
                account_log = []
                account_index = []
                trading = 0
                order_log = []
                sub_order = {}      # 记录每一笔单的详情
                single_posi = round(balance / 8000, 1)
                pre_date = xau_data.Date[0]

                # Date	Time	OpenBid	HighBid	LowBid	CloseBid	OpenAsk	HighAsk	LowAsk	CloseAsk	Total Ticks
                for i in range(period, length):
                    hh = max(xau_data.HighAsk[(i - period):(i + 1)])
                    lc = min(xau_data.CloseBid[(i - period):(i + 1)])
                    hc = max(xau_data.CloseAsk[(i - period):(i + 1)])
                    ll = min(xau_data.LowBid[(i - period):(i + 1)])
                    range_price = max(hh - lc, hc - ll)
                    buyline = xau_data.OpenAsk[i] + k1 * range_price
                    sellline = xau_data.OpenBid[i] - k2 * range_price

                    single_posi = max(single_posi, round(balance / 8000, 1))
                    trading_fee = self.handling_fee + 10 * single_posi * self.slippage  # 交易成本
                    if pre_date == xau_data.Date[i]:
                        pass
                    else:
                        account_log.append([balance])
                        account_index.append(pre_date)
                        pre_date = xau_data.Date[i]

                    if xau_data.CloseAsk[i] > buyline:
                        if position == 0:
                            # none position and buy one
                            position = single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'buyline', "position": single_posi,
                                              'open_time': xau_data.Date[i] + " " + xau_data.Time[i],
                                              'open': xau_data.CloseAsk[i], 'holding_time': 1,
                                              'high': max(xau_data.CloseBid[i], buyline),
                                              'low': min(xau_data.CloseAsk[i], buyline)})
                        elif position < 0:
                            # have a sell position and reverse to buy
                            sub_order.update({'close': xau_data.CloseAsk[i],
                                              'holding_time': sub_order['holding_time'] + 1,
                                              'close_time': xau_data.Date[i] + " " + xau_data.Time[i]})
                            order_profit = - 10 * single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                            sub_order.update({"profit": order_profit})
                            balance += order_profit
                            order_log.append(sub_order)
                            sub_order = dict()

                            # buy a position
                            position = single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'buyline', "position": single_posi,
                                              'open_time': xau_data.Date[i] + " " + xau_data.Time[i],
                                              'open': xau_data.CloseAsk[i], 'holding_time': 1,
                                              'high': max(xau_data.CloseBid[i], buyline),
                                              'low': min(xau_data.CloseAsk[i], buyline)})

                        else:
                            # already haven a position
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(xau_data.HighBid[i], sub_order['high']),
                                              'low': min(xau_data.LowAsk[i], sub_order['low'])})

                    elif xau_data.CloseBid[i] < sellline:
                        if position == 0:
                            position = -single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'sellline', "position": single_posi,
                                              'open_time': xau_data.Date[i] + " " + xau_data.Time[i],
                                              'open': xau_data.CloseBid[i], 'holding_time': 1,
                                              'high': max(xau_data.CloseBid[i], sellline),
                                              'low': min(xau_data.CloseAsk[i], sellline)})
                        elif position > 0:
                            sub_order.update({'close': xau_data.CloseBid[i],
                                              'holding_time': sub_order['holding_time'] + 1,
                                              'close_time': xau_data.Date[i] + " " + xau_data.Time[i]})
                            order_profit = 10 * single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                            sub_order.update({"profit": order_profit})
                            balance += order_profit
                            order_log.append(sub_order)
                            sub_order = dict()

                            position = -single_posi
                            trading += 1
                            sub_order.update({'longorshort': 'sellline', "position": single_posi,
                                              'open_time': xau_data.Date[i] + " " + xau_data.Time[i],
                                              'open': xau_data.CloseBid[i], 'holding_time': 1,
                                              'high': max(xau_data.CloseBid[i], sellline),
                                              'low': min(xau_data.CloseAsk[i], sellline)})
                        else:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(xau_data.HighBid[i], sub_order['high']),
                                              'low': min(xau_data.LowAsk[i], sub_order['low'])})

                    else:
                        if position > 0:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(xau_data.HighBid[i], sub_order['high']),
                                              'low': min(xau_data.LowAsk[i], sub_order['low'])})
                        elif position < 0:
                            sub_order.update({'holding_time': sub_order['holding_time'] + 1,
                                              'high': max(xau_data.HighBid[i], sub_order['high']),
                                              'low': min(xau_data.LowAsk[i], sub_order['low'])})
                        else:
                            pass
                    # print(account)

                if position > 0:
                    sub_order.update({'close': xau_data.CloseBid[i],
                                      'close_time': xau_data.Date[i] + " " + xau_data.Time[i]})
                    order_profit = 10 * single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    balance += order_profit
                    account_log.append([balance])
                    order_log.append(sub_order)
                elif position < 0:
                    sub_order.update({'close': xau_data.CloseAsk[i],
                                      'close_time': xau_data.Date[i] + " " + xau_data.Time[i]})
                    order_profit = - 10 * single_posi * (sub_order['close'] - sub_order['open']) - trading_fee
                    sub_order.update({"profit": order_profit})
                    balance += order_profit
                    account_log.append([balance])
                    order_log.append(sub_order)
                else:
                    account_log.append([balance])

                account_index.append(xau_data.Date[i])

                drawdown_cal = MaximumDrawdown()
                max_drawdown = round(drawdown_cal.calculate(account_log), 4)
                account_log = pd.DataFrame(account_log,
                                           columns=[str(k1) + '_' + str(k2) + '_' + str(period)],
                                           index=account_index)
                save_log = pd.concat((save_log, account_log), axis=1)
                order_log = pd.DataFrame.from_dict(order_log, orient="columns")

                save_all = save_all.append([{"No": n,
                                             'k1': k1,
                                             'k2': k2,
                                             "period": period,
                                             'rate': round((balance - 10000) / 100, 2),
                                             'trading': trading,
                                             'maxdrawdown': max_drawdown}])

                # print("No", n,
                #       "k1:", k1,
                #       " k2:", k2,
                #       " period:", period,
                #       " rating:", round((account - 10000) / 100, 2),
                #       " drawdown:", max_drawdown,
                #       " trading:", trading)  # " var : ", account_log.account.std()

                if n % 10 == 0:
                    # 保存迭代的结果
                    save_all.to_csv("Result/thrust/save_all_" + str(pro_id) + ".csv", index_label=None)
                    save_log.to_csv("Result/thrust/account_log_" + str(pro_id) + ".csv", index_label=None)

                n += 1
                pbar.update(1)

        save_all.to_csv("Result/thrust/save_all_" + str(pro_id) + ".csv", index_label=None)
        save_log.to_csv("Result/thrust/account_log_" + str(pro_id) + ".csv", index_label=None)

        if order_save == 1:
            order_log.to_csv("Result/thrust/order_log_" + str(pro_id) + ".csv", index_label=None)

