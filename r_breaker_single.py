# 螺纹钢商品期货主连
# 使用R_Breaker算法，量化交易回测

# import talib as ta
import pandas as pd
import numpy as np
import time

# from r_breaker import TRADING
# from r_breaker_1minclose import TRADING
from r_breaker_move_2 import TRADING


if __name__ == "__main__":
    trade_algo = TRADING(slippage=0, handling_fee=5)

    time_start = time.time()

    # para = [[0.3, 1.06, 0.06, 0.25, 120]]
    para = [[0.3, 1.06, 0.06, 0.3, 60]]
    # para = [[0.35, 1.01, 0.04, 0.3, 130]]
    trade_algo.main(0, para, len(para), 1)

    print('times used: ', round(time.time() - time_start, 2))

